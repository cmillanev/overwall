<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoclienteMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Contacto Overwall';
    public $asunto;
    public $nombres;
    public $email;
    public $orden;
    public $mensaje;
    public function __construct($asunto, $nombres, $email, $orden, $mensaje)
    {
        $this->asunto = $asunto;
        $this->nombres = $nombres;
        $this->email = $email;
        $this->orden = $orden;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.contacto');
    }
}
