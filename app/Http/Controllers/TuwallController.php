<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\Models\Cmarco;
use App\Models\Frame;
use App\Models\Layout;
use App\Models\Material;
use App\Models\Medida;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class TuwallController extends Controller
{
    public function tuwall(){

        $sliders = Slider::all();
        $medidas = Medida::all();
        $materiales = Material::all();

        return view('front.tuwall.tuwall', compact('sliders', 'medidas', 'materiales'));
    }

    public function cmarco(Request $request){
        $idUser = Auth::User()->id;
        $frame = new Frame();  

        //Valida que venga la imagen
        $request->validate([
            'file_photo' =>  'required',
            'precio' => 'required'
        ]);
    //Convierte la imagen base64 en jpg y guarda
        $image = $request->twall;
        $imageInfo = explode(";base64,", $image);
        $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
        $image = str_replace(' ', '+', $imageInfo[1]);
        $imageName = "post-".time().".".$imgExt;
        Storage::disk('public')->put($imageName, base64_decode($image));
    // Lee la imagen original y la guarda
       $imagenes = $request->file('file_photo')->store('public/cuadros');
       $url = Storage::url($imagenes); 

       $frame->idUser = $idUser;
       $frame->material = $request->acabado;
       $frame->color = $request->color;
       $frame->medidas = $request->medida;
       $frame->url = $url;
       $frame->cantidad = $request->tcantidad;
       $frame->precio = $request->precio;
       $frame->total = $request->total;
       $frame->url_recorte = '/storage/'.$imageName;
       if ($frame->save()) {
        return Redirect::back()->with('actualizar','ok');
    }

    }
   
   
    public function clayout(Request $request){
        $idUser = Auth::User()->id;
        $layout = new Layout();  
       switch ($request->layout) {
           case 'Room':

            $request->validate([
                'file_photo' =>  'required',
                'precio' => 'required'
            ]);
    
            $imagenes = $request->file('file_photo')->store('public/cuadros');
             $url = Storage::url($imagenes); 
    
                $image = $request->layout1_1;;
                $imageInfo = explode(";base64,", $image);
                $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
                $image = str_replace(' ', '+', $imageInfo[1]);
                $imageName = "post-".time().".".$imgExt;
                Storage::disk('public')->put($imageName, base64_decode($image));
    
           $layout->idUser = $idUser;
           $layout->material = $request->materiales;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url_recorte = '/storage/'.$imageName;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
        }
            break;
        case 'Office':


           
            $request->validate([
                'url1' =>  'required',
                'url2' =>  'required',
                'url3' =>  'required',
                'url4' =>  'required',
                'precio' => 'required'
            ]);
    
            $imagenes = $request->file('url1')->store('public/cuadros');
             $url = Storage::url($imagenes); 
            $imagenes2 = $request->file('url2')->store('public/cuadros');
             $url2 = Storage::url($imagenes2); 
            $imagenes3 = $request->file('url3')->store('public/cuadros');
             $url3 = Storage::url($imagenes3); 
            $imagenes4 = $request->file('url4')->store('public/cuadros');
             $url4 = Storage::url($imagenes4); 
    
             $image = $request->layout2_1;
             $imageInfo = explode(";base64,", $image);
             $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
             $image = str_replace(' ', '+', $imageInfo[1]);
             $imageName = "post-".time().rand(10,100).".".$imgExt;
             echo $imageName;
             Storage::disk('public')->put($imageName, base64_decode($image));
             
             $image2 = $request->layout2_2;
             $imageInfo2 = explode(";base64,", $image2);
             $imgExt2 = str_replace('data:image/', '', $imageInfo2[0]);      
             $image2 = str_replace(' ', '+', $imageInfo2[1]);
             $imageName2 = "post-".time().rand(10,100).".".$imgExt2;
             echo $imageName2;
             Storage::disk('public')->put($imageName2, base64_decode($image2));
             
             $image3 = $request->layout2_3;
             $imageInfo3 = explode(";base64,", $image3);
             $imgExt3 = str_replace('data:image/', '', $imageInfo3[0]);      
             $image3 = str_replace(' ', '+', $imageInfo3[1]);
             $imageName3 = "post-".time().rand(10,100).".".$imgExt3;
             echo $imageName3;
             Storage::disk('public')->put($imageName3, base64_decode($image3));
             
             $image4 = $request->layout2_4;
             $imageInfo4 = explode(";base64,", $image4);
             $imgExt4 = str_replace('data:image/', '', $imageInfo4[0]);      
             $image4 = str_replace(' ', '+', $imageInfo4[1]);
             $imageName4 = "post-".time().rand(10,100).".".$imgExt4;
             echo $imageName4;
             Storage::disk('public')->put($imageName4, base64_decode($image4));
          

           $layout->idUser = $idUser;
           $layout->material = $request->material;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url2 = $url2;
           $layout->url3 = $url3;
           $layout->url4 = $url4;
           $layout->url_recorte = '/storage/'.$imageName;
           $layout->url_recorte_2 = '/storage/'.$imageName2;
           $layout->url_recorte_3 = '/storage/'.$imageName3;
           $layout->url_recorte_4 = '/storage/'.$imageName4;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
           }
           break;
        case 'Fun':

          
            $request->validate([
                'url1' =>  'required',
                'url2' =>  'required',
                'url3' =>  'required',
                'url4' =>  'required',
                'url5' =>  'required',
                'url6' =>  'required',
                'precio' => 'required'

            ]);
    
            $imagenes = $request->file('url1')->store('public/cuadros');
            $url = Storage::url($imagenes); 
           $imagenes2 = $request->file('url2')->store('public/cuadros');
            $url2 = Storage::url($imagenes2); 
           $imagenes3 = $request->file('url3')->store('public/cuadros');
            $url3 = Storage::url($imagenes3); 
           $imagenes4 = $request->file('url4')->store('public/cuadros');
            $url4 = Storage::url($imagenes4); 
           $imagenes5 = $request->file('url5')->store('public/cuadros');
            $url5 = Storage::url($imagenes5); 
           $imagenes6 = $request->file('url6')->store('public/cuadros');
            $url6 = Storage::url($imagenes6); 
    
            $image = $request->layout3_1;
            $imageInfo = explode(";base64,", $image);
            $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
            $image = str_replace(' ', '+', $imageInfo[1]);
            $imageName = "post-".time().rand(10,100).".".$imgExt;
            Storage::disk('public')->put($imageName, base64_decode($image));
            
            $image2 = $request->layout3_2;
            $imageInfo2 = explode(";base64,", $image2);
            $imgExt2 = str_replace('data:image/', '', $imageInfo[0]);      
            $image2 = str_replace(' ', '+', $imageInfo2[1]);
            $imageName2 = "post-".time().rand(10,100).".".$imgExt2;
            Storage::disk('public')->put($imageName2, base64_decode($image2));
            
            $image3 = $request->layout3_3;
            $imageInfo3 = explode(";base64,", $image3);
            $imgExt3 = str_replace('data:image/', '', $imageInfo3[0]);      
            $image3 = str_replace(' ', '+', $imageInfo3[1]);
            $imageName3 = "post-".time().rand(10,100).".".$imgExt;
            Storage::disk('public')->put($imageName3, base64_decode($image3));
            
            $image4 = $request->layout3_4;
            $imageInfo4 = explode(";base64,", $image4);
            $imgExt4 = str_replace('data:image/', '', $imageInfo4[0]);      
            $image4 = str_replace(' ', '+', $imageInfo4[1]);
            $imageName4 = "post-".time().rand(10,100).".".$imgExt4;
            Storage::disk('public')->put($imageName4, base64_decode($image4));

            $image5 = $request->layout3_5;
            $imageInfo5 = explode(";base64,", $image5);
            $imgExt5 = str_replace('data:image/', '', $imageInfo5[0]);      
            $image5 = str_replace(' ', '+', $imageInfo5[1]);
            $imageName5 = "post-".time().rand(10,100).".".$imgExt5;
            Storage::disk('public')->put($imageName5, base64_decode($image5));

            $image6 = $request->layout3_6;
            $imageInfo6 = explode(";base64,", $image6);
            $imgExt6 = str_replace('data:image/', '', $imageInfo6[0]);      
            $image6 = str_replace(' ', '+', $imageInfo6[1]);
            $imageName6 = "post-".time().rand(10,100).".".$imgExt6;
            Storage::disk('public')->put($imageName6, base64_decode($image6));
    
           $layout->idUser = $idUser;
           $layout->material = $request->material;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url2 = $url2;
           $layout->url3 = $url3;
           $layout->url4 = $url4;
           $layout->url5 = $url5;
           $layout->url6 = $url6;
           $layout->url_recorte = '/storage/'.$imageName;
           $layout->url_recorte_2 = '/storage/'.$imageName2;
           $layout->url_recorte_3 = '/storage/'.$imageName3;
           $layout->url_recorte_4 = '/storage/'.$imageName4;
           $layout->url_recorte_5 = '/storage/'.$imageName5;
           $layout->url_recorte_6 = '/storage/'.$imageName6;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
           }
           break;
       
        case 'Home':
            $request->validate([
                'url1' =>  'required',
                'url2' =>  'required',
                'url3' =>  'required',
                'url4' =>  'required',
                'precio' => 'required'
            ]);
    
            $imagenes = $request->file('url1')->store('public/cuadros');
             $url = Storage::url($imagenes); 
            $imagenes2 = $request->file('url2')->store('public/cuadros');
             $url2 = Storage::url($imagenes2); 
            $imagenes3 = $request->file('url3')->store('public/cuadros');
             $url3 = Storage::url($imagenes3); 
            $imagenes4 = $request->file('url4')->store('public/cuadros');
             $url4 = Storage::url($imagenes4); 
    
             $image = $request->layout4_1;
             $imageInfo = explode(";base64,", $image);
             $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
             $image = str_replace(' ', '+', $imageInfo[1]);
             $imageName = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName, base64_decode($image));
             
             $image2 = $request->layout4_2;
             $imageInfo2 = explode(";base64,", $image2);
             $imgExt2 = str_replace('data:image/', '', $imageInfo[0]);      
             $image2 = str_replace(' ', '+', $imageInfo2[1]);
             $imageName2 = "post-".time().rand(10,100).".".$imgExt2;
             Storage::disk('public')->put($imageName2, base64_decode($image2));
             
             $image3 = $request->layout4_3;
             $imageInfo3 = explode(";base64,", $image3);
             $imgExt3 = str_replace('data:image/', '', $imageInfo3[0]);      
             $image3 = str_replace(' ', '+', $imageInfo3[1]);
             $imageName3 = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName3, base64_decode($image3));
             
             $image4 = $request->layout4_4;
             $imageInfo4 = explode(";base64,", $image4);
             $imgExt4 = str_replace('data:image/', '', $imageInfo4[0]);      
             $image4 = str_replace(' ', '+', $imageInfo4[1]);
             $imageName4 = "post-".time().rand(10,100).".".$imgExt4;
             Storage::disk('public')->put($imageName4, base64_decode($image4));

           $layout->idUser = $idUser;
           $layout->material = $request->material;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url2 = $url2;
           $layout->url3 = $url3;
           $layout->url4 = $url4;
           $layout->url_recorte = '/storage/'.$imageName;
           $layout->url_recorte_2 = '/storage/'.$imageName2;
           $layout->url_recorte_3 = '/storage/'.$imageName3;
           $layout->url_recorte_4 = '/storage/'.$imageName4;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
           }     
           break;

        case 'Flag':
            $request->validate([
                'url1' =>  'required',
                'url2' =>  'required',
                'url3' =>  'required',
                'precio' => 'required'
            ]);
    
            $imagenes = $request->file('url1')->store('public/cuadros');
             $url = Storage::url($imagenes); 
            $imagenes2 = $request->file('url2')->store('public/cuadros');
             $url2 = Storage::url($imagenes2); 
            $imagenes3 = $request->file('url3')->store('public/cuadros');
             $url3 = Storage::url($imagenes3); 
           
             $image = $request->layout5_1;
             $imageInfo = explode(";base64,", $image);
             $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
             $image = str_replace(' ', '+', $imageInfo[1]);
             $imageName = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName, base64_decode($image));
             
             $image2 = $request->layout5_2;
             $imageInfo2 = explode(";base64,", $image2);
             $imgExt2 = str_replace('data:image/', '', $imageInfo[0]);      
             $image2 = str_replace(' ', '+', $imageInfo2[1]);
             $imageName2 = "post-".time().rand(10,100).".".$imgExt2;
             Storage::disk('public')->put($imageName2, base64_decode($image2));
             
             $image3 = $request->layout5_3;
             $imageInfo3 = explode(";base64,", $image3);
             $imgExt3 = str_replace('data:image/', '', $imageInfo3[0]);      
             $image3 = str_replace(' ', '+', $imageInfo3[1]);
             $imageName3 = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName3, base64_decode($image3));  

           $layout->idUser = $idUser;
           $layout->material = $request->material;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url2 = $url2;
           $layout->url3 = $url3;
           $layout->url_recorte = '/storage/'.$imageName;
           $layout->url_recorte_2 = '/storage/'.$imageName2;
           $layout->url_recorte_3 = '/storage/'.$imageName3;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
           }    
           break;

        case 'Space':
            $request->validate([
                'url1' =>  'required',
                'url2' =>  'required',
                'url3' =>  'required',
                'precio' => 'required'
            ]);
    
            $imagenes = $request->file('url1')->store('public/cuadros');
             $url = Storage::url($imagenes); 
            $imagenes2 = $request->file('url2')->store('public/cuadros');
             $url2 = Storage::url($imagenes2); 
            $imagenes3 = $request->file('url3')->store('public/cuadros');
             $url3 = Storage::url($imagenes3); 
    
             $image = $request->layout6_1;
             $imageInfo = explode(";base64,", $image);
             $imgExt = str_replace('data:image/', '', $imageInfo[0]);      
             $image = str_replace(' ', '+', $imageInfo[1]);
             $imageName = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName, base64_decode($image));
             
             $image2 = $request->layout6_2;
             $imageInfo2 = explode(";base64,", $image2);
             $imgExt2 = str_replace('data:image/', '', $imageInfo[0]);      
             $image2 = str_replace(' ', '+', $imageInfo2[1]);
             $imageName2 = "post-".time().rand(10,100).".".$imgExt2;
             Storage::disk('public')->put($imageName2, base64_decode($image2));
             
             $image3 = $request->layout6_3;
             $imageInfo3 = explode(";base64,", $image3);
             $imgExt3 = str_replace('data:image/', '', $imageInfo3[0]);      
             $image3 = str_replace(' ', '+', $imageInfo3[1]);
             $imageName3 = "post-".time().rand(10,100).".".$imgExt;
             Storage::disk('public')->put($imageName3, base64_decode($image3));  
                
           $layout->idUser = $idUser;
           $layout->material = $request->material;
           $layout->medida = 'medidas';
           $layout->cantidad = $request->cantidad;
           $layout->precio = $request->precio;
           $layout->total = $request->total;
           $layout->layout = $request->layout;
           $layout->url1 = $url;
           $layout->url2 = $url2;
           $layout->url3 = $url3;
           $layout->url_recorte = '/storage/'.$imageName;
           $layout->url_recorte_2 = '/storage/'.$imageName2;
           $layout->url_recorte_3 = '/storage/'.$imageName3;
           if ($layout->save()) {
            return Redirect::back()->with('actualizar','ok');
           }       
           break;           
       }

    }


    public function destroy($id)
    {
      
            $articulo = Frame::findOrFail($id);

            return $articulo->url;

                //Cambia storage por public para eliminar de storage
            $url = str_replace('storage', 'public', $articulo->url);
            //Elimina de storage la imagen
            storage::delete($url);

            $articulo->delete();

            return Redirect::back()->with('message', 'Articulo Eliminado');
        
    }

    public function layouts(Request $request){

       
         //trae cantidad
         $layout = $_POST['cant'];
         $materiales = Material::all();
         switch ($layout) {
             case 'layout1':
                $titulo = "LAYOUT OVERWALL ROOM";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout1', compact('materiales', 'titulo', 'descripcion'));
                 break;
             case 'layout2':
                $titulo = "LAYOUT OVERWALL OFFICE";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout2', compact('materiales', 'titulo', 'descripcion'));
                 break;
             case 'layout3':
                $titulo = "LAYOUT OVERWALL ROOM";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout3', compact('materiales', 'titulo', 'descripcion'));
                 break;
             case 'layout4':
                $titulo = "LAYOUT OVERWALL ROOM";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout4', compact('materiales', 'titulo', 'descripcion'));
                 break;
             case 'layout5':
                $titulo = "LAYOUT OVERWALL ROOM";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout5', compact('materiales', 'titulo', 'descripcion'));
                 break;
             case 'layout6':
                $titulo = "LAYOUT OVERWALL ROOM";
                $descripcion = "1 pánel de 40 x 40 cm";
                
                return view('front.tuwall.layouts.layout6', compact('materiales', 'titulo', 'descripcion'));
                 break;
             
             default:
                 # code...
                 break;
         }

         return view('front.resultados.gprecios');
    }
}
