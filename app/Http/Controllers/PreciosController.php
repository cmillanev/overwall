<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PreciosController extends Controller
{
    //

    public function gprecios(Request $request){

            //Trae material
            $material = $_POST['name'];
            //trae medidas
            $medidas = $_POST['mun'];
            //trae cantidad
            $cant = $_POST['cant'];

            $query = DB::select('SELECT precio FROM precios WHERE material = ?  and medidas = ?', [$material, $medidas]);


            $suma = $query[0]->precio * $cant;
            $precio = $query[0]->precio;
       

            return view('front.resultados.gprecios', compact('suma', 'precio'));
    }

    public function tprecios(Request $request){

        $medidas = $_POST['med'];

        $cant = $_POST['cant'];

        $query = DB::select('SELECT precio FROM precios WHERE modalidad = ?  and medidas = ?', ['picwall', $medidas]);

        $suma = $query[0]->precio * $cant;
        $precio = $query[0]->precio;

        return view('front.tuwall.resultados.tprecios', compact('suma', 'precio'));

    }

    public function lprecios(Request $request){

        $lay = $_POST['lay'];
        $material = $_POST['name'];
        $cant = $_POST['cant'];

        $query = DB::select('SELECT precio FROM precios WHERE modalidad = ?  and material = ?', [$lay, $material]);

        $suma = $query[0]->precio * $cant;
        $precio = $query[0]->precio;

        return view('front.tuwall.resultados.tprecios', compact('suma', 'precio'));

    }
}
