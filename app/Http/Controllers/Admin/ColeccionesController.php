<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coleccione;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ColeccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colecciones = Coleccione::all();
        return view('admin.ecommerce.colecciones.index', compact('colecciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ecommerce.colecciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'file' =>  'required'
        ]);

       $imagenes = $request->file('file')->store('public/colecciones');
       $url = Storage::url($imagenes);
            

        $colecciones = new Coleccione;

        $colecciones->coleccion_nombre = $request->coleccion_nombre;
        $colecciones->coleccion_palabras = $request->coleccion_palabras;
        $colecciones->coleccion_url =  $url;
        $colecciones->coleccion_estatus = $request->coleccion_estatus;

        $colecciones->save();
        
       // return Redirect::back()->with('actualizar','ok');
        return redirect()->route('colecciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coleccion = Coleccione::findOrFail($id);
        return view('admin.ecommerce.colecciones.edit', compact('coleccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request;
            $coleccion = Coleccione::findOrFail($id);
        if($request->hasFile('file')){
      
            //Cambia storage por public para eliminar de storage
            $url = str_replace('storage', 'public', $coleccion->coleccion_url);
            //Elimina de storage la imagen
            Storage::delete($url);
            //Eliminar de la bd
        // $coleccion->delete();
            $imagenes = $request->file('file')->store('public/colecciones');
            $url = Storage::url($imagenes);  
            $coleccion->coleccion_url =  $url;
        }
       
        $coleccion->coleccion_nombre = $request->coleccion_nombre;
        $coleccion->coleccion_palabras = $request->coleccion_palabras;
        $coleccion->coleccion_estatus = $request->coleccion_estatus;
        $coleccion->save();

        return redirect()->route('colecciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($coleccion)
    {       
        $coleccion = Coleccione::findOrFail($coleccion);
         //Cambia storage por public para eliminar de storage
         $url = str_replace('storage', 'public', $coleccion->coleccion_url);
         //Elimina de storage la imagen
         Storage::delete($url);
         //Eliminar de la bd
         $coleccion->delete();
         return redirect()->route('colecciones.index');
    }
}
