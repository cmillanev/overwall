<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        return view('admin.ecommerce.categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        
        return view('admin.ecommerce.categorias.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'file' =>  'required'
        ]);
        $request->validate([
            'file1' =>  'required'
        ]);

       $imagenes = $request->file('file')->store('public/categorias');
       $imagenes2 = $request->file('file1')->store('public/categorias');
       $url = Storage::url($imagenes);
       $url2 = Storage::url($imagenes2);


        $categoria = new Categoria;

        $categoria->categoria_nombre = $request->categoria_nombre;
        $categoria->categoria_palabras = $request->categoria_palabras;
        $categoria->categoria_url =  $url;
        $categoria->categoria_url_fondo =  $url2;
        $categoria->categoria_estatus = $request->categoria_estatus;

        $categoria->save();
        
       // return Redirect::back()->with('actualizar','ok');
        return redirect()->route('categorias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $categoria = Categoria::findOrFail($id);
        return view('admin.ecommerce.categorias.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
        //Cambia storage por public para eliminar de storage
        $url = str_replace('storage', 'public', $categoria->categoria_url);
        //Elimina de storage la imagen
        storage::delete($url);
        //Eliminar de la bd
        $categoria->delete();
        return redirect()->route('categorias.index');
    }
}
