<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Models\Slider;
use App\Models\Tag;
use App\Models\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $tags = DB::table('tags')
            ->join('categorias', 'tags.categoria_id', '=', 'categorias.id')
            
            ->select('tags.*', 'categorias.categoria_nombre')
            ->get();
      
        return view('admin.ecommerce.tags.index', compact('tags'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        return view('admin.ecommerce.tags.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $tag = new Tag();
        $tag->categoria_id = $request->categoria_id;
        $tag->tag_nombre = $request->tag_nombre;
        $tag->tag_palabras = $request->tag_palabras;
        $tag->tag_estatus = $request->tag_estatus;

        $tag->save();
        
       // return Redirect::back()->with('actualizar','ok');
        return redirect()->route('tags.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::all();
        $tags = DB::table('tags')
        ->join('categorias', 'tags.categoria_id', '=', 'categorias.id')
        ->select('tags.*', 'categorias.categoria_nombre')
        ->where('tags.id', '=', $id)
        ->get();

        return view('admin.ecommerce.tags.edit', compact('categorias', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
      $tag = Tag::findOrFail($id);

        $tag->categoria_id = $request->categoria_id;
        $tag->tag_nombre = $request->tag_nombre;
        $tag->tag_palabras = $request->tag_palabras;
        $tag->tag_estatus = $request->tag_estatus;

        $tag->save();

        return redirect()->route('tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);

        $tag->delete();

        return redirect()->route('tags.index');
    }   
}
