<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cgetty;
use App\Models\Cupone;
use App\Models\Frame;
use App\Models\Layout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CuponesController extends Controller
{
    public function cupon(Request $request){
        //validar si el cupón es válido
         $valida = DB::SELECT('SELECT * FROM cupones WHERE cupon = ? and estatus = ?', [$request->cupon, '1']);

        
         

         if ($valida != null){
            $idUser = Auth::user()->id;
            $descuento = '.'.$valida[0]->porcentaje;

            $cgetties = Cgetty::where('idUser', $idUser)
            ->where('pagada', null)
            ->where('cupon', null)
            ->get();

            foreach ($cgetties as $cgetty) {
               $porcentaje = $cgetty->total * $descuento;
               $precio = $cgetty->total - $porcentaje;
    
               DB::table('cgetties')
               ->where('id', $cgetty->id )
               ->update([
                   'total' => $precio,
                   'cupon' => $request->cupon
                   ]);
            }

            $frames = Frame::where('idUser', $idUser)
                        ->where('pagada', null)
                        ->where('cupon', null)
                        ->get();
              foreach ($frames as $frame) {
               $porcentaje = $frame->total * $descuento;
               $precio = $frame->total - $porcentaje;
    
               DB::table('frames')
               ->where('id', $frame->id )
               ->update([
                   'total' => $precio,
                   'cupon' => $request->cupon
                   ]);
            }            
                        
            $layouts = Layout::where('idUser', $idUser)
                        ->where('pagada', null)
                        ->where('cupon', null)
                        ->get();

               foreach ($layouts as $layout) {
               $porcentaje = $layout->total * $descuento;
               $precio = $layout->total - $porcentaje;
    
               DB::table('layouts')
               ->where('id', $layout->id )
               ->update([
                   'total' => $precio,
                   'cupon' => $request->cupon
                   ]);
            }              

            return Redirect::back()->with('cupon','ok'); 
        }else{
            return Redirect::back()->with('cupon','no');  
        }

        
    }

    public function index(){

        return view('admin.ecommerce.cupones.index');
    }
}
