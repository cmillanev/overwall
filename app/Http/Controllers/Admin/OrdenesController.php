<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\PedidoenviadoMail;
use App\Models\Address;
use App\Models\Cgetty;
use App\Models\Denvio;
use App\Models\Destinatario;
use App\Models\Frame;
use App\Models\Layout;
use App\Models\Mexpago;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Traits\HasRoles;

class OrdenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    /*
      return  $ordenes = DB::table('cgetties')
        ->join('frames', 'cgetties.cgetties_transaccion', '=', 'frames.frames_transaccion')
        ->join('layouts', 'cgetties.cgetties_transaccion', '=', 'layouts.layouts_transaccion')
        ->select('cgetties.*', frames.*', 'layouts.*')
        ->get();             
*/
    $ordenes = Mexpago::all(); 
    $idUser = Auth::user()->id;
    $orden = Mexpago::where('user_id', $idUser)->get();           
        return view('admin.ecommerce.ordenes.index', compact('ordenes', 'orden'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        //SEKECCIOONA EL USER_ID DE MEXPAGO
       $mexpago = Mexpago::where('numeroTransaccion', $id) 
                        ->get('user_id');
        //SELECCICONA TODO DE MEXPAGO
       $estatus = Mexpago::where('numeroTransaccion', $id) 
                        ->get();
     

    //busca los datos del cliente
       $cliente = User::findOrFail($mexpago); 
       $dir = Denvio::where('user_id', $mexpago[0]->user_id)->get();  
       $destinatario = Destinatario::where('user_id', $mexpago[0]->user_id)->get();  
       $direccion = $dir[0]->e_calle.', '.$dir[0]->e_numExt.', '.$dir[0]->e_numInt.', '.$dir[0]->e_colonia.', '.$dir[0]->e_ciudad.', '.$dir[0]->e_estado.', '.$dir[0]->e_cp;

       $ordenGetty = Cgetty::where('transaccion', $id)
                        ->where('pagada', 'pagada')
                        ->get();
       $ordenFrame = Frame::where('transaccion', $id)
                        ->where('pagada', 'pagada')
                        ->get();
       $ordenLayout = Layout::where('transaccion', $id)
                        ->where('pagada', 'pagada')
                        ->get();

        return view('admin.ecommerce.ordenes.show', compact('ordenGetty', 'ordenFrame', 'ordenLayout',
                    'direccion', 'cliente', 'destinatario', 'estatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $orden = Mexpago::findOrFail($id);
    
       $orden->estatus_pedido = $request->estatus_pedido;
       $orden->save();
       if ($request->estatus_pedido == 'Enviado') {
        $email = User::where('id', $orden->user_id)
        ->get('email');
        Mail::to($email)->queue(new PedidoenviadoMail());
     }
       return Redirect::back()->with('Actualizar', 'Estatus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
