<?php

namespace App\Http\Controllers\Usuario;

use App\Http\Controllers\Controller;
use App\Models\Cgetty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use function PHPUnit\Framework\returnSelf;

class CgettyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $iduser = Auth::user()->id;
       

        $pedido = new Cgetty();

        $pedido->idUser = $iduser;
        $pedido->material = $request->materiales;
        $pedido->medidas = $request->medidas;
        $pedido->imagen = $request->imagen;
        $pedido->url  = $request->url;
        $pedido->cantidad = $request->cantidad;
        $pedido->precio = $request->precio;
        $pedido->total = $request->total;

            if ($pedido->save()) {
                return Redirect::back()->with('actualizar','ok');
            }

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $articulo = Cgetty::findOrFail($id);

            $articulo->delete();

            return Redirect::back()->with('message', 'Articulo Eliminado');
        
    }
}
