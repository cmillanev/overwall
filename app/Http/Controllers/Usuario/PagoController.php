<?php

namespace App\Http\Controllers\Usuario;

use App\Http\Controllers\Controller;
use App\Mail\ConfirmacioncompraMail;
use App\Models\Mexpago;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PagoController extends Controller
{
    public function pago(Request $request){

      //Recibe la respuesta del pago
        $numTransaccion = $request->numeroTransaccion;   
        $transa = explode('-', $numTransaccion);  
        $idPedido = $transa[0];

        DB::table('cgetties')
            ->where('idUser', $idPedido )
            ->update([
                'pagada' => 'pagada',
                'transaccion' => $numTransaccion
                ]);

        DB::table('frames')
            ->where('idUser', $idPedido )
            ->update([
                'pagada' => 'pagada',
                'transaccion' => $numTransaccion
                ]);

        DB::table('layouts')
            ->where('idUser', $idPedido )
            ->update([
                'pagada' => 'pagada',
                'transaccion' => $numTransaccion
                ]);


           $mexpago = new Mexpago();
           $mexpago->foliomexPago = $request->folioMexPago;
           $mexpago->numeroTransaccion = $request->numeroTransaccion;
           $mexpago->pago = $request->pago;     
           $mexpago->monto = $request->monto;     
           $mexpago->numeroAut = $request->numeroAut; 
           $mexpago->user_id = $idPedido;
           $mexpago->estatus_pedido = 'Nueva';
           $mexpago->save();

           $user = User::findOrFail($idPedido);

           Mail::to($user->email)->queue(new ConfirmacioncompraMail($user->name, $user->email, $numTransaccion));

           return view('front.confirmacion');
    }
}
