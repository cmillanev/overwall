<?php

namespace App\Http\Controllers\Usuario;

use App\Http\Controllers\Controller;
use App\Models\Frame;
use App\Models\Layout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class TuwallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        switch ($request->control) {
            case 'Room':
                $articulo = Layout::findOrFail($id);
                //Cambia storage por public para eliminar de storage
             //Cambia storage por public para eliminar de storage
             $url1 = str_replace('storage', 'public', $articulo->url1);
             $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
             //Elimina de storage la imagen
             Storage::delete($url1);
             Storage::delete($url_recorte);
    
            $articulo->delete();
    
            return Redirect::back()->with('message', 'Articulo Eliminado');
                break;
            case 'Office':
                    $articulo = Layout::findOrFail($id);
                    //Cambia storage por public para eliminar de storage
                    $url1 = str_replace('storage', 'public', $articulo->url1);
                    $url2 = str_replace('storage', 'public', $articulo->url2);
                    $url3 = str_replace('storage', 'public', $articulo->url3);
                    $url4 = str_replace('storage', 'public', $articulo->url4);
                    $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
                    $url_recorte2 = str_replace('storage', 'public', $articulo->url_recorte2);
                    $url_recorte3 = str_replace('storage', 'public', $articulo->url_recorte3);
                    $url_recorte4 = str_replace('storage', 'public', $articulo->url_recorte4);
                    //Elimina de storage la imagen
                    Storage::delete($url1);
                    Storage::delete($url2);
                    Storage::delete($url3);
                    Storage::delete($url4);
                    Storage::delete($url_recorte);
                    Storage::delete($url_recorte2);
                    Storage::delete($url_recorte3);
                    Storage::delete($url_recorte4);
        
                $articulo->delete();
        
                return Redirect::back()->with('message', 'Articulo Eliminado');
                break;  
            case 'Fun':
                    $articulo = Layout::findOrFail($id);
                    //Cambia storage por public para eliminar de storage
                    $url1 = str_replace('storage', 'public', $articulo->url1);
                    $url2 = str_replace('storage', 'public', $articulo->url2);
                    $url3 = str_replace('storage', 'public', $articulo->url3);
                    $url4 = str_replace('storage', 'public', $articulo->url4);
                    $url5 = str_replace('storage', 'public', $articulo->url5);
                    $url6 = str_replace('storage', 'public', $articulo->url6);
                    $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
                    $url_recorte2 = str_replace('storage', 'public', $articulo->url_recorte2);
                    $url_recorte3 = str_replace('storage', 'public', $articulo->url_recorte3);
                    $url_recorte4 = str_replace('storage', 'public', $articulo->url_recorte4);
                    $url_recorte5 = str_replace('storage', 'public', $articulo->url_recorte5);
                    $url_recorte6 = str_replace('storage', 'public', $articulo->url_recorte6);
                    //Elimina de storage la imagen
                    Storage::delete($url1);
                    Storage::delete($url2);
                    Storage::delete($url3);
                    Storage::delete($url4);
                    Storage::delete($url_recorte);
                    Storage::delete($url_recorte2);
                    Storage::delete($url_recorte3);
                    Storage::delete($url_recorte4);
                    Storage::delete($url_recorte5);
                    Storage::delete($url_recorte6);
        
                $articulo->delete();
        
                return Redirect::back()->with('message', 'Articulo Eliminado');
            break;  
            
            case 'Home':
                $articulo = Layout::findOrFail($id);
                //Cambia storage por public para eliminar de storage
                $url1 = str_replace('storage', 'public', $articulo->url1);
                $url2 = str_replace('storage', 'public', $articulo->url2);
                $url3 = str_replace('storage', 'public', $articulo->url3);
                $url4 = str_replace('storage', 'public', $articulo->url4);
                $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
                $url_recorte2 = str_replace('storage', 'public', $articulo->url_recorte2);
                $url_recorte3 = str_replace('storage', 'public', $articulo->url_recorte3);
                $url_recorte4 = str_replace('storage', 'public', $articulo->url_recorte4);
                //Elimina de storage la imagen
                Storage::delete($url1);
                Storage::delete($url2);
                Storage::delete($url3);
                Storage::delete($url4);
                Storage::delete($url_recorte);
                Storage::delete($url_recorte2);
                Storage::delete($url_recorte3);
                Storage::delete($url_recorte4);
    
            $articulo->delete();
    
            return Redirect::back()->with('message', 'Articulo Eliminado');
                break;
            case 'Flag':
                    $articulo = Layout::findOrFail($id);
                    //Cambia storage por public para eliminar de storage
                    $url1 = str_replace('storage', 'public', $articulo->url1);
                    $url2 = str_replace('storage', 'public', $articulo->url2);
                    $url3 = str_replace('storage', 'public', $articulo->url3);
                    $url4 = str_replace('storage', 'public', $articulo->url4);
                    $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
                    $url_recorte2 = str_replace('storage', 'public', $articulo->url_recorte2);
                    $url_recorte3 = str_replace('storage', 'public', $articulo->url_recorte3);
                  
                    //Elimina de storage la imagen
                    Storage::delete($url1);
                    Storage::delete($url2);
                    Storage::delete($url3);
                    Storage::delete($url4);
                    Storage::delete($url_recorte);
                    Storage::delete($url_recorte2);
                    Storage::delete($url_recorte3);
                    
        
                $articulo->delete();
        
                return Redirect::back()->with('message', 'Articulo Eliminado');
                break;    
            case 'Space':
                    $articulo = Layout::findOrFail($id);
                    //Cambia storage por public para eliminar de storage
                    $url1 = str_replace('storage', 'public', $articulo->url1);
                    $url2 = str_replace('storage', 'public', $articulo->url2);
                    $url3 = str_replace('storage', 'public', $articulo->url3);
                    $url4 = str_replace('storage', 'public', $articulo->url4);
                    $url_recorte = str_replace('storage', 'public', $articulo->url_recorte);
                    $url_recorte2 = str_replace('storage', 'public', $articulo->url_recorte2);
                    $url_recorte3 = str_replace('storage', 'public', $articulo->url_recorte3);
                  
                    //Elimina de storage la imagen
                    Storage::delete($url1);
                    Storage::delete($url2);
                    Storage::delete($url3);
                    Storage::delete($url4);
                    Storage::delete($url_recorte);
                    Storage::delete($url_recorte2);
                    Storage::delete($url_recorte3);
        
                $articulo->delete();
        
                return Redirect::back()->with('message', 'Articulo Eliminado');
            break;
           
        }
       
      

        $articulo = Frame::findOrFail($id);

    
            //Cambia storage por public para eliminar de storage
            $url = str_replace('storage', 'public', $articulo->url);
            $url2 = str_replace('storage', 'public', $articulo->url_recorte);
            //Elimina de storage la imagen
            Storage::delete($url);
            Storage::delete($url2);
            
        $articulo->delete();

        return Redirect::back()->with('message', 'Articulo Eliminado');
    
    }
}
