<?php

namespace App\Http\Controllers;


use App\Models\Coleccione;
use App\Models\Material;
use App\Models\Medida;
use App\Models\Slider;
use App\Models\User;
use App\Models\Categoria;
use App\Models\Cgetty;
use App\Models\Frame;
use App\Models\Layout;
use App\Models\Tag;
use Database\Seeders\TagsSeeder;
use GettyImages\Api\GettyImages_Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $sliders = Slider::all();
      $colecciones = Coleccione::all();
      
        return view('front.index', compact('sliders', 'colecciones'));
    }

    public function busqueda(Request $request){

      
     
            # Replace these values with your key and secret
            $apiKey = "k6ppkpmxpg8h2pmnk466v3sx";
            $apiSecret = "xNWVMidw9fNhyuVcoayU";
            
            $phrase = $request->frase;
            $pagina = $request->page;
            $fileTypes = ['jpg'];
            
            // Example of built in search images endpoint
            $client = GettyImages_Client::getClientWithClientCredentials("$apiKey", "$apiSecret");
            $fields = array("referral_destinations", "title", "preview", "orientation", "max_dimensions", "display_set", "file_types");
            $response = $client->SearchImages()->withPhrase("$phrase")->withFileTypes($fileTypes)->withGraphicalStyles(['photography'])->withFields($fields)->withPageSize(30)
            ->withSortOrder('most_popular')->withPage($pagina)->withAcceptLanguage('es')->execute();
            $datos = json_decode($response,true);
    
            $data = array($datos);
            $count = count($datos['images']);

            if ($count <= 29) {
              $count = count($datos['images']);
            }else{
              $count = 30;
            }

            $eti = DB::select('SELECT id FROM categorias where categoria_palabras = ?', [$phrase]);

          if ($eti != null) {
            
            $etiquetas = DB::select('SELECT * FROM tags WHERE categoria_id = ?', [$eti[0]->id]  );
          }else{
             $etiquetas = null;
          }
          


           
           
           // return $data = array($datos['images'][0]['display_sizes'][0]['uri']);

            $categorias = Categoria::all();

            //selecciona el id de la categoría

            function getBrowser() {

              $user_agent = $_SERVER['HTTP_USER_AGENT'];
              $browser = "N/A";
              
              $browsers = array(
              '/msie/i' => 'Internet explorer',
              '/firefox/i' => 'Firefox',
              '/safari/i' => 'Safari',
              '/chrome/i' => 'Chrome',
              '/edge/i' => 'Edge',
              '/opera/i' => 'Opera',
              '/mobile/i' => 'Mobile browser'
              );
              
              foreach ($browsers as $regex => $value) {
              if (preg_match($regex, $user_agent)) { $browser = $value; }
              }
              
              return $browser;
              }
              
              $navegador = getBrowser();

              if($navegador == 'Mobile browser'){

                    // Example of built in search images endpoint
            $client = GettyImages_Client::getClientWithClientCredentials("$apiKey", "$apiSecret");
            $fields = array("referral_destinations", "title", "preview", "orientation", "max_dimensions", "display_set", "file_types");
            $response = $client->SearchImages()->withPhrase("$phrase")->withFileTypes($fileTypes)->withGraphicalStyles(['photography'])->withFields($fields)->withPageSize(10)
            ->withSortOrder('most_popular')->withPage($pagina)->withAcceptLanguage('es')->execute();
            $datos = json_decode($response,true);
    
            $data = array($datos);
            $count = count($datos['images']);

            if ($count <= 9) {
              $count = count($datos['images']);
            }else{
              $count = 10;
            }

            $eti = DB::select('SELECT id FROM categorias where categoria_palabras = ?', [$phrase]);

          if ($eti != null) {
            
            $etiquetas = DB::select('SELECT * FROM tags WHERE categoria_id = ?', [$eti[0]->id]  );
          }else{
             $etiquetas = null;
          }


                return view('front.resultadoMobil', compact('data','categorias','phrase', 'count', 'etiquetas' ));
              }

      
              return view('front.resultado', compact('data','categorias','phrase', 'count', 'etiquetas' ));
           
           
      
    }

    public function vistaprevia(Request $request){
      
         # Replace these values with your key and secret
         $apiKey = "k6ppkpmxpg8h2pmnk466v3sx";
         $apiSecret = "xNWVMidw9fNhyuVcoayU";
         
         $phrase = $request->id;
         
         
         // Example of built in search images endpoint
         $client = GettyImages_Client::getClientWithClientCredentials("$apiKey", "$apiSecret");
         $fields = array("referral_destinations", "title", "preview", "orientation", "max_dimensions");
         $response = $client->SearchImagesCreative()->withPhrase("$phrase")->withFields($fields)->withPageSize(20)->withAcceptLanguage('es')->execute();
         $datos = json_decode($response,true);
 
         $data = array($datos);
         // $data = array($datos['images'][0]['display_sizes'][0]['uri']);  

         $medidas = Medida::all();
         $materiales = Material::all();

         $medida = '100 X 80';
         $material = 'Canvas';
         $total = 2035;
         
         return view('front.preview', compact('data', 'medidas', 'materiales', 'material', 'medida', 'total'));
    }

    public function carrito(){

  //  return  $phone = User::find(21)->destinatario->d_nombre;

      if (Auth::user() == false) {
        $cgetties = null;
        $frames = null;
        $sumaf = 0;
        return view('front.carrito', compact('cgetties', 'frames', 'sumaf'));
      }

      $sumaf = 0;

      $idUser = Auth::user()->id;
      $usuario = User::findOrFail($idUser);
      //$usuario->address->calle; 
      $hora = date("H:i:s");
      $hoy = date("Y-m-d"); 
      $transaccion = $idUser . '-' . $hora;

      $cgetties = Cgetty::where('idUser', $idUser)
                        ->where('pagada', null)
                        ->get();

      $frames = Frame::where('idUser', $idUser)
                        ->where('pagada', null)
                        ->get();
                        
      $layouts = Layout::where('idUser', $idUser)
                        ->where('pagada', null)
                        ->get();
      //Se crea el array para mexpago
      $articulos = ["articulos" =>[
       
  
    ]];
    //Agregando de getty los productos
    foreach ($cgetties as $cgetty) {

      array_push($articulos['articulos'], ["descripcion"=>"$cgetty->imagen", "monto"=>"$cgetty->total"]);

    }
    //Agregando de frame los productos
    foreach ($frames as $frame) {

      array_push($articulos['articulos'], ["descripcion"=>"Cuadro personalizado", "monto"=>"$frame->total"]);

    }
    //Agregando de layouts los productos
    foreach ($layouts as $layout) {

      array_push($articulos['articulos'], ["descripcion"=>"Cuadro personalizado", "monto"=>"$layout->total"]);

    }

    
    //Datos del usuario
    $datos = [
      
      "correo" => "$usuario->email",
      "celular" => "$usuario->telefono"
    ];

     $productos = json_encode($articulos);
     $nombre = json_encode($datos);

     // $productos =  "articulos" : [{"descripcion" : "Descripcion Articulo #1", "monto" : "50.00"}, { "descripcion" : " Descripcion Articulo #2", "monto" : "50.00" }, {"descripcion" : " Descripcion Articulo #3", "monto" : "100.00"}] }
   
      return view('front.carrito', compact('cgetties', 'frames', 'layouts', 'sumaf',
                  'hoy', 'productos', 'nombre', 'idUser','hora','transaccion'));

    }

    public function contacto(){
      
      return view('front.contacto');
    }


    public function carga(){
      return view('front.carga');
    }

    public function confirmacion(){
      return view('front.confirmacion');
    }

   
}
