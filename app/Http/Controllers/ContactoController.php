<?php

namespace App\Http\Controllers;

use App\Mail\ContactoclienteMail;
use App\Mail\NuevoclienteMail;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use PharIo\Manifest\Email;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Mail::to('encuadrante@gmail.com')->queue(new NuevoclienteMail());
        return view('front.contacto');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
      
       
        $contacto = new Contact;
        $contacto->contacto_motivo = $request->contacto_motivo;
        $contacto->contacto_nombre = $request->contacto_nombre;
        $contacto->contacto_apellido = $request->contacto_apellido;
        $contacto->contacto_correo = $request->contacto_correo;
        $contacto->contacto_orden = $request->contacto_orden;
        $contacto->contacto_mensaje = $request->contacto_mensaje;
        //$contacto->motivo = $request->motivo;
        $contacto->save();

        $asunto = $request->contacto_motivo;
        $nombres = $request->contacto_nombre . ' ' . $request->apellido;
        $email = $request->contacto_correo;
        $orden = $request->contacto_orden;
        $mensaje = $request->contacto_mensaje;

        Mail::to('overwall_mx@icloud.com')->queue(new ContactoclienteMail($asunto, $nombres, $email, $orden, $mensaje));
       // Mail::to('encuadrante@gmail.com')->queue(new ContactoclienteMail($asunto, $nombres, $email, $orden, $mensaje));
         return Redirect::back()->with('actualizar','ok');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
