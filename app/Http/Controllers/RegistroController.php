<?php

namespace App\Http\Controllers;

use App\Mail\NuevoclienteMail;
use App\Models\Cgetty;
use App\Models\Dcontacto;
use App\Models\Denvio;
use App\Models\Destinatario;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use RegistersUsers;
  

    protected $redirectTo = '/carrito';

    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {

        
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_calle' => 'required',
            'c_colonia' => 'required',
            'c_numExt' => 'required',
         
            'c_mun' => 'required',
            'c_ciudad' => 'required',
            'c_estado' => 'required',
            'c_pais' => 'required',
            'c_cp' => 'required',
            'e_calle' => 'required',
            'e_colonia' => 'required',
            'e_numExt' => 'required',
         
            'e_mun' => 'required',
            'e_ciudad' => 'required',
            'e_estado' => 'required',
            'e_pais' => 'required',
            'e_cp' => 'required|max:5',
            'd_nombre' => 'required'
        ]);


       
       


        $usuario = new User();

        $usuario->name = $request->name;
        $usuario->email = $request->email;
        
        $usuario->password = Hash::make($request->password);
        

        if($usuario->save()){
            //asignar rol
            $usuario->assignRole('cliente');

        }
        $idUser = DB::select('SELECT MAX(id) AS idUser FROM users');

        $contacto = new Dcontacto();
        $contacto->user_id = $idUser[0]->idUser;
        $contacto->c_calle = $request->c_calle;
        $contacto->c_colonia = $request->c_colonia;
        $contacto->c_numExt = $request->c_numExt;
        $contacto->c_numInt = $request->c_numInt;
        $contacto->c_mun = $request->c_mun;
        $contacto->c_ciudad = $request->c_ciudad;
        $contacto->c_estado = $request->c_estado;
        $contacto->c_pais = $request->c_pais;
        $contacto->c_cp = $request->c_cp;
        $contacto->save();



        $envio = new Denvio();
        $envio->user_id = $idUser[0]->idUser;
        $envio->e_calle = $request->e_calle;
        $envio->e_colonia = $request->e_colonia;
        $envio->e_numExt = $request->e_numExt;
        $envio->e_numInt = $request->e_numInt;
        $envio->e_mun = $request->e_mun;
        $envio->e_ciudad = $request->e_ciudad;
        $envio->e_estado = $request->e_estado;
        $envio->e_pais = $request->e_pais;
        $envio->e_cp = $request->e_cp;
        $envio->save();

        $destinatario = new Destinatario();
        $destinatario->user_id = $idUser[0]->idUser;
        $destinatario->d_nombre = $request->d_nombre;
        $destinatario->save();

        //Guardar localstorage si hay algo 

        

        Mail::to($request->email)->queue(new NuevoclienteMail());



        if ($request->material_cuadros != null) {
                 
            $material = explode(",",$request->material_cuadros);
            $medidas = explode(",",$request->medidas_cuadros);
            $cantidad = explode(",",$request->cantidad_cuadros);
            $precio = explode(",",$request->precio_cuadros);
            $total = explode(",",$request->total_cuadros);
            $url = explode(",",$request->url_cuadros);
            
           
            $count = count($material);

            for ($i=0; $i< $count ; $i++) { 

              $pedido = new Cgetty();

        $pedido->idUser = $idUser[0]->idUser;
        $pedido->material = $material[$i];
        $pedido->medidas = $medidas[$i];
        $pedido->imagen = 'Imagen Getty';
        $pedido->url  = $url[$i];
        $pedido->cantidad = $cantidad[$i];
        $pedido->precio = $precio[$i];
        $pedido->total = $total[$i];
        $pedido->save();
            
        
    }

   
         
 }

    
 $user =  Auth::loginUsingId($idUser[0]->idUser, true);
   return Redirect::back()->with('actualizar','ok'); 

       
      
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   
}
