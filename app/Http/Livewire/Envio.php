<?php

namespace App\Http\Livewire;

use App\Models\Denvio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Envio extends Component
{
    public $e_calle, $e_colonia, $e_numExt, $e_numInt, $e_mun, $e_ciudad, $e_estado,
            $e_pais, $e_cp;
    public $successMsg;        
    public function render()
    {
        $idUser = Auth::user()->id;
        $envio = Denvio::where('user_id', $idUser)->get();
        $this->e_calle = $envio[0]->e_calle;
        $this->e_colonia = $envio[0]->e_colonia;
        $this->e_numExt = $envio[0]->e_numExt;
        $this->e_numInt = $envio[0]->e_numInt;
        $this->e_mun = $envio[0]->e_mun;
        $this->e_ciudad = $envio[0]->e_ciudad;
        $this->e_estado = $envio[0]->e_estado;
        $this->e_pais = $envio[0]->e_pais;
        $this->e_cp = $envio[0]->e_cp;
        return view('livewire.envio', compact('envio'));
    }

    public function submitForm()
    {
        $idUser = Auth::user()->id;
        Denvio::where('user_id', $idUser)
                    ->update([
                        'e_calle' => $this->e_calle,
                        'e_colonia' => $this->e_colonia,
                        'e_numExt' => $this->e_numExt,
                        'e_numInt' => $this->e_numInt,
                        'e_mun' => $this->e_mun,
                        'e_ciudad' => $this->e_ciudad,
                        'e_estado' => $this->e_estado,
                        'e_pais' => $this->e_pais,
                        'e_cp' => $this->e_cp
                        ]);

                    $this->successMsg = 'Datos de envio actualizados.';
                    Session::flash('message', 'store');
                    
    }
}
