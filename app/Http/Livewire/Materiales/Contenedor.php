<?php

namespace App\Http\Livewire\Materiales;

use App\Models\Material;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Contenedor extends Component
{
    public $nombre, $descripcion, $material_id;
    public $view = 'create';
    public $successMsg;

    protected $rules = [
        'nombre' => 'required',
        'descripcion' => 'required'
    ];
    protected $messages = [
        'nombre.required' => 'El nombre es requerido.',
        'descripcion.required' => 'La descripción es requerida'
    ];

    public function render()
    {
        $this->materiales = Material::get();
        return view('livewire.materiales.contenedor', [
            'materiales' => Material::orderBy('id', 'desc')->paginate(8)
        ]);
    }
    public function store(){
        $validatedData = $this->validate();
        Material::create([
            'material_nombre' => $this->nombre,
            'material_descripcion' => $this->descripcion,
        ]);

        $this->nombre = '';
        $this->descripcion = '';
        $this->successMsg = 'Registro exitoso.';
                    Session::flash('message', 'store');
    

    }
    public function edit($id){

        $material  = Material::find($id);
        $this->material_id = $material->id;
        $this->nombre =  $material->material_nombre;
        $this->descripcion =  $material->material_descripcion;
        $this->view = 'edit';
      }
      public function update(){
        $material = Material::find($this->material_id);
        $material->update([
         'material_nombre' => $this->nombre,
         'material_descripcion' => $this->descripcion
        ]);
         //Setea el metodo 
        $this->default();
     }
     public function destroy($id){
         Material::destroy($id);
     }
     public function default(){
 
         $this->nombre = '';
         $this->descripcion = '';
         $this->view = 'create';
         
     }
}
