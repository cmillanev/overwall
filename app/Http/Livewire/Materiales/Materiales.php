<?php

namespace App\Http\Livewire\Materiales;

use App\Models\Material;
use Livewire\Component;

class Materiales extends Component
{
    public function render()
    {
        $this->materiales = Material::get();
        return view('livewire.materiales.contenedor', [
            'materiales' => Material::orderBy('id', 'desc')->paginate(8)
        ]);
    }
}
