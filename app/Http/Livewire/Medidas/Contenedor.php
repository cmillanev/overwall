<?php

namespace App\Http\Livewire\Medidas;

use App\Models\Medida;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Contenedor extends Component
{

    public $medidas_x, $medidas_y, $medida_id;
    public $view = 'create';
    public $successMsg;

    protected $rules = [
        'medidas_x' => 'required',
        'medidas_y' => 'required'
    ];
    protected $messages = [
        'medidas_x.required' => 'El campo medidas x es requerido.',
        'medidas_y.required' => 'El campo medidas y es requerido.'
    ];
    public function render()
    {
        $this->medidas = Medida::get();
        return view('livewire.medidas.contenedor', [
            'medidas' => Medida::orderBy('id', 'desc')->paginate(8)
            ]);
    }

    public function store(){
     
        $validatedData = $this->validate();
        Medida::create([
            'medidas_x' => $this->medidas_x,
            'medidas_y' => $this->medidas_y,
        ]);

        $this->medidas_x = '';
        $this->medidas_y = '';
        $this->successMsg = 'Registro exitoso.';
                    Session::flash('message', 'store');
    

    }
    public function edit($id){

        $medida  = Medida::find($id);
        $this->medida_id = $medida->id;
        $this->medidas_x =  $medida->medidas_x;
        $this->medidas_y =  $medida->medidas_y;
        $this->view = 'edit';
      }
      public function update(){
        $medida = Medida::find($this->medida_id);
        $medida->update([
         'medidas_x' => $this->medidas_x,
         'medidas_y' => $this->medidas_y
        ]);
         //Setea el metodo 
        $this->default();
     }
     public function destroy($id){
         Medida::destroy($id);
     }
     public function default(){
 
         $this->nombre = '';
         $this->descripcion = '';
         $this->view = 'create';
         
     }
}
