<?php

namespace App\Http\Livewire\Contacto;

use App\Models\Contact;
use Livewire\Component;

class Contacto extends Component
{
    public function render()
    {
        $this->contacto = Contact::get();
        return view('livewire.contacto.contacto', [
            'contactos' => Contact::orderBy('id', 'desc')->paginate(8)
        ]);
    }
    public function destroy($id){
        Contact::destroy($id);
    }
}
