<?php

namespace App\Http\Livewire\Cupones;

use App\Models\Cupon;
use App\Models\Cupone;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Contenedor extends Component
{
    public $cupon, $porcentaje, $estatus, $fecha;
    public $view = 'create';
    public $successMsg, $cupon_id;

    protected $rules = [
        'cupon' => 'required',
        'porcentaje' => 'required'
    ];
    protected $messages = [
        'cupon.required' => 'El cupón es requerido.',
        'porcentaje.required' => 'EL porcentaje es requerido'
    ];

    public function render()
    {
        
        return view('livewire.cupones.contenedor', [
            'cupones' => Cupone::orderBy('id', 'desc')->paginate(8)
        ]);
    }

    public function store(){
        $validatedData = $this->validate();
        Cupone::create([
            'cupon' => $this->cupon,
            'porcentaje' => $this->porcentaje,
            'estatus' =>$this->estatus,
            'fecha' =>$this->fecha
        ]);

        $this->cupon = '';
        $this->porcentaje = '';
        $this->successMsg = 'Registro exitoso.';
                    Session::flash('message', 'store');
    
    }
    public function edit($id){

        $cupon  = Cupone::find($id);
        $this->cupon_id = $cupon->id;
        $this->cupon =  $cupon->cupon;
        $this->porcentaje =  $cupon->porcentaje;
        $this->estatus =  $cupon->estatus;
        $this->fecha =  $cupon->fecha;
        $this->view = 'edit';
      }
      public function update(){
        $material = Cupone::find($this->cupon_id);
        $material->update([
            'cupon' => $this->cupon,
            'porcentaje' => $this->porcentaje,
            'estatus' =>$this->estatus,
            'fecha' =>$this->fecha
        ]);
         //Setea el metodo 
        $this->default();
     }
     public function destroy($id){
         Cupone::destroy($id);
     }
     public function default(){
 
         $this->cupon = '';
         $this->porcentaje = '';
         $this->estatus = '';
         $this->fecha = '';
         $this->view = 'create';
         
     }
}
