<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Credenciales extends Component
{
    public $name, $email, $password;
    public $successMsg;  

    public function submitForm()
    {
        $idUser = Auth::user()->id;
        $usuario = User::findOrFail($idUser);

        $usuario->name = $this->name;
        $usuario->email = $this->email;
        if ($this->password != null) {
            $usuario->password = Hash::make($this->password);
        }
        $usuario->save();                 

                    $this->successMsg = 'Datos personales actualizados.';
                    Session::flash('message', 'store');
                    
    }

    public function render()
    {
        $idUser = Auth::user()->id;
        $user = User::findOrFail($idUser);
        $this->name = $user->name;
        $this->email = $user->email;
        $this->password = $user->password;
        return view('livewire.credenciales', compact('user'));
    }
}
