<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Mexpago;
use Livewire\Component;

class Contenedor extends Component
{
    public $nuevas, $produccion, $enviadas, $entregadas;

    public function render()
    {
        $nueva = Mexpago::where('estatus_pedido', 'Nuevas')->get();
        $this->nuevas = count($nueva);
        $produccion = Mexpago::where('estatus_pedido', 'Producción')->get();
        $this->produccion = count($produccion);
        $enviadas = Mexpago::where('estatus_pedido', 'Enviado')->get();
        $this->enviadas = count($enviadas);
        $entregado = Mexpago::where('estatus_pedido', 'Entregado')->get();
        $this->entregadas = count($entregado);

        if(!isset($nuevas)){
            $this->nuevas = 0;
        }
        if(!isset($produccion)){
            $this->produccion = 0;
        }
        if(!isset($enviadas)){
            $this->enviadas = 0;
        }
        if(!isset($entregado)){
            $this->entregas = 0;
        }
        return view('livewire.dashboard.contenedor');
    }
}
