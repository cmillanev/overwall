<?php

namespace App\Http\Livewire\Precios;

use App\Models\Precio;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithPagination;

class Contenedor extends Component
{
    use WithPagination;
    public $modalidad, $material, $medidas, $precio, $precio_id;
    public $view = 'create';
    public $successMsg;
    protected $paginationTheme = 'bootstrap';

    protected $rules = [
        'modalidad' => 'required',
        'medidas' => 'required',
        'precio' => 'required|numeric'
    ];
    protected $messages = [
        'modalidad.required' => 'La modalidad es requerida.',
        'medidas.required' => 'Las medidas son requeridas',
        'precio.required' => 'Ingrese el precio',
        'name.required' => 'Ingrese el nombre',
        'telefono.required' => 'Ingrese el teléfono',
        'telefono.digits' => 'El teléfono debe de tener 10 dígitos numéricos'
    ];

    public function render()
    {
       
        $this->precios = Precio::get();
        return view('livewire.precios.contenedor', [
            'precios' => Precio::orderBy('id', 'desc')->paginate(8)
        ]);
    }
    public function store(){
        $validatedData = $this->validate();
        Precio::create([
            'modalidad' => $this->modalidad,
            'material' => $this->material,
            'medidas' => $this->medidas,
            'precio' => $this->precio
        ]);

        $this->modalidad = '';
        $this->material = '';
        $this->medidas = '';
        $this->precio = '';
        $this->successMsg = 'Registro exitoso.';
                    Session::flash('message', 'store');
    

    }
    public function edit($id){

        $precio  = Precio::find($id);
        $this->precio_id = $precio->id;
        $this->modalidad =  $precio->modalidad;
        $this->material =  $precio->material;
        $this->medidas =  $precio->medidas;
        $this->precio =  $precio->precio;
        $this->view = 'edit';
      }
      public function update(){
        $usuario = Precio::find($this->precio_id);
        $usuario->update([
         'modalidad' => $this->modalidad,
         'material' => $this->material,
         'medidas' => $this->medidas,
         'precio' => $this->precio
        ]);
         //Setea el metodo 
        $this->default();
     }
     public function destroy($id){
         Precio::destroy($id);
     }
     public function default(){
 
         $this->modalidad = '';
         $this->material = '';
         $this->medidas = '';
         $this->precio = '';
         $this->view = 'create';
         
     }
}
