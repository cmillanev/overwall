<?php

namespace App\Http\Livewire;
use App\Models\Destinatario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Ndestintario extends Component
{
    public $nombre;

    public $successMsg = '';

    public function render()
    {
        $idUser = Auth::user()->id;

        $destinatario = DB::select('SELECT * FROM destinatarios WHERE user_id = ?', [$idUser]);
        $this->nombre = $destinatario[0]->d_nombre;
        return view('livewire.ndestintario', compact('destinatario'));
    }

    public function submitForm()
    {
        $idUser = Auth::user()->id;
        Destinatario::where('user_id', $idUser)
                    ->update(['d_nombre' => $this->nombre]);

                    $this->successMsg = 'Nombre de destinatario actualizado.';
                    Session::flash('message', 'store');
                    
    }
}
