<?php

namespace App\Http\Livewire\Incompletas;

use App\Models\Cgetty;
use App\Models\Mexpago;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Contenedor extends Component
{
    public function render()
    {
        
        $cgetties = Cgetty::where('pagada', null);
        $idUser = Auth::user()->id;
        $orden = Mexpago::where('user_id', $idUser)->get();  
        return view('livewire.incompletas.contenedor', compact());
    }
}
