<?php

namespace App\Http\Livewire\Clientes;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
class Contenedor extends Component
{
    use WithPagination;
    public $modalidad, $material, $medidas, $precio, $precio_id;
    public $view = 'create';
    public $successMsg;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
      

       // $this->clientes = User::get();
        return view('livewire.clientes.contenedor', [
            'clientes' => User::role('cliente')->orderBy('id', 'desc')->paginate(8)
        ]);
    }
}
