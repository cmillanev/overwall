<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;



    // Relación
    public function user() {
        return $this->hasOne('App\User', 'id'); // Le indicamos que se va relacionar con el atributo id
    }

}
