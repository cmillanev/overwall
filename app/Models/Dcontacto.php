<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dcontacto extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'c_calle',
        'c_colonia',
        'c_numExt',
        'c_numInt',
        'c_mun',
        'c_ciudad',
        'c_estado',
        'c_pais'
    ];



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
