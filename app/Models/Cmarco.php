<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cmarco extends Model
{
    use HasFactory;

    protected  $fillable = [
        'iduser',
        'descripcion',
        'medidas',
        'material',
        'url'
    ];
}
