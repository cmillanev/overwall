<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUser',
        'material',
        'medidas',
        'url',
        'cantidad',
        'precio',
        'total',
        'color',
        'frames_transaccion'
    ];
}
