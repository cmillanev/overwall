<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUser',
        'medida',
        'material',
        'cantidad',
        'precio',
        'total',
        'layour',
        'url1',
        'url2',
        'url3',
        'url4',
        'url5',
        'url6',
        'layouts_transaccion'
    ];
}
