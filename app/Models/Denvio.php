<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Denvio extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'e_calle',
        'e_colonia',
        'e_numExt',
        'e_numInt',
        'e_mun',
        'e_ciudad',
        'e_estado',
        'e_pais'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
