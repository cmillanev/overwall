<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $fillable = [
        'categoria_nombre',
        'categoria_palabras',
        'categoria_url',
        'categoria_estatus'
    ];


       // Relación
       public function tags() {
        return $this->hasMany('App\Models\Tag'); // Le indicamos que se va relacionar con el atributo id
    }



       

 
}
