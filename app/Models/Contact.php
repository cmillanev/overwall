<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'contacto_nombre',
        'contacto_apellido',
        'contacto_correo',
        'contacto_orden',
        'contacto_mensaje'
    ];
}
