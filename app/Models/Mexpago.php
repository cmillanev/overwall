<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mexpago extends Model
{
    use HasFactory;

    protected $table = 'mexpago';

    protected $fillable = [
        'filioMexPago',
        'numeroTransaccion',
        'pago',
        'monto',
        'numeroAut',
        'estatus_pedido'
    ];
}
