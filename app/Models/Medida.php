<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    use HasFactory;

    protected $fillable = [
        'medidas_nombre',
        'medidas_x',
        'medidas_y',
        'medidas_unidad',
        'medidas_status'
    ];
}
