<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cgetty extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUser',
        'material',
        'medidas',
        'imagen',
        'url',
        'cantidad',
        'precio',
        'pagada',
        'cgetties_transaccion'
    ];
}
