<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = [
        'categoria_id',
        'tag_nombre',
        'tag_palabras',
        'tag_estatus'
    ];

    public function categoria()
    {
        return $this->hasOne('App\Models\Categoria', 'id');
    }


}
