<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => '',
    'title_prefix' => '',
    'title_postfix' => '| Overall',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => '<b>Over</b>Wall',
    'logo_img' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => true,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => true,
    'usermenu_desc' => false,
    'usermenu_profile_url' => true,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#71-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => true,
    'layout_fixed_navbar' => true,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#721-authentication-views-classes
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#722-admin-panel-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-danger elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#73-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#74-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'admin',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#92-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#8-menu-configuration
    |
    */

    'menu' => [
        [
            'text' => ' ir al sitio',
            'url' => 'https://overwall.com.mx',
            'icon' => 'fas fa-fw fa-globe',
            'topnav' => true,
        ],
        
        [
            'text' => 'Dashboard',
            'route'  => 'home',
            'icon' => 'fas fa-fw fa-home',
            'can' => 'view_products',
         
          
        ],
        [
            'text' => 'Ecommerce',
            'url' => '#',
            'icon' => 'fas fa-fw fa-shopping-cart',
            'can' => 'view_products',
            'submenu' => [
             /*   [
                    'text' => 'Reportes',
                    'url' => 'reportes',
                    'icon' => 'fas fa-fw fa-chart-bar',
                    'can' => 'edit_products',
                ], */
                [
                    'text' => 'Ordenes',
                    'url' => 'admin/ordenes',
                    'icon' => 'fas fa-fw fa-receipt',
                    'can' => 'view_products',
                ],
            /*     [
                    'text' => 'Ordenes en carrito',
                    'url' => 'admin/incompletas',
                    'icon' => 'fas fa-fw fa-cart-arrow-down',
                    'can' => 'edit_products',
                ],
               [
                    'text' => 'Productos',
                    'url' => '#',
                    'icon' => 'fas fa-fw fa-camera',
                    'can' => 'edit_products',
                ], */
                [
                    'text' => 'Precios',
                    'url' => 'admin/precios',
                    'icon' => 'fas fa-fw fa-dollar-sign',
                    'can' => 'edit_products',
                ],
                [
                    'text' => 'Categorías',
                    'url' => 'admin/categorias',
                    'icon' => 'fas fa-fw fa-archive',
                    'can' => 'edit_products',
                ],
                [
                    'text' => 'Tags',
                    'url' => 'admin/tags',
                    'icon' => 'fas fa-fw fa-archive',
                    'can' => 'edit_products',
                ],
             /*   [
                    'text' => 'Artistas',
                    'url' => '#',
                    'can' => 'edit_products',
                ], */
                [
                    'text' => 'Colecciones',
                    'url' => 'admin/colecciones',
                    'icon' => 'fas fa-fw fa-images',
                    'can' => 'edit_products',
                ],
         /*       [
                    'text' => 'Envios',
                    'url' => '#',
                    'can' => 'edit_products',
                ], */
                [
                    'text' => 'Cupones',
                    'url' => 'admin/cuponera',
                    'icon' => 'fas fa-fw fa-ticket',
                    'can' => 'edit_products',
                ],
             /*     [
                    'text' => 'Clientes',
                    'url' => 'admin/clientes',
                    'icon' => 'fas fa-fw fa-portrait',
                    'can' => 'edit_products',
                ],
              [
                    'text' => 'Configuración',
                    'url' => '#',
                    'can' => 'edit_products',
                ], */
                [
                    'text' => 'Sliders',
                    'url' => 'admin/sliders',
                    'icon' => 'fas fa-fw fa-sliders-h',
                    'can' => 'edit_products',
                ],
                [
                    'text' => 'Materiales',
                    'url' => 'admin/materiales',
                    'icon' => 'fas fa-fw fa-archive',
                    'can' => 'edit_products',
                ],
                [
                    'text' => 'Medidas',
                    'url' => 'admin/medidas',
                    'icon' => 'fas fa-fw fa-archive',
                    'can' => 'edit_products',
                ],
            ],
        ],
        [
            'text' => 'Contacto',
            'url'  => 'admin/contact',
            'icon' => 'far fa-fw fa-envelope',
            'can' => 'edit_products',
          
        ],
     /*   [
            'text' => 'Pagos',
            'icon' => 'fas fa-fw fa-credit-card',
            'can' => 'edit_products',
            'submenu' => [
                [
                    'text' => 'Transacciones',
                    'url' => '#',
                ],
                [
                    'text' => 'Métodos de pago',
                    'url' => '#',
                ],
            ],
        ], */
    /*    [
            'text' => 'Galería',
            'url' => 'galeria',
            'icon' => 'fas fa-fw fa-images',
            'can' => 'edit_products',
        ],
        [
            'text' => 'Getty Images',
            'url' => '#',
            'icon' => 'fas fa-fw fa-camera',
            'can' => 'edit_products',
            'submenu' => [
                [
                    'text' => 'Conexión',
                    'url' => 'admin/getty',  
                    'icon' => 'fas fa-fw fa-plug',                 
                ],
                
           ],
        ],     */  
        [
            'text' => 'Administración',
            'url' => '#',
            'icon' => 'fas fa-fw fa-user-shield',
            'can' => 'edit_products',
            'submenu' => [
                [
                    'text' => 'Usuarios',
                    'url' => 'admin/usuarios', 
                    'icon' => 'fas fa-fw fa-users',                  
                ],
              /*  [
                    'text' => 'Información del sistema',
                    'url' => '#',                   
                ], */
                [
                    'text' => 'Edición de Emails',
                    'url' => 'maileclipse',
                    'icon' => 'fas fa-fw fa-envelope-open-text',
                ],
             /*   [
                    'text' => 'Logs',
                    'url' => '#',                   
                ],
                [
                    'text' => 'Backups',
                    'url' => '#',                   
                ],  */
            ],
        ],
        
       
        
       
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#83-custom-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#91-plugins
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'vendor/sweetalert2/sweetalert2.all.min.js',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#93-livewire
    */

    'livewire' => false,
];
