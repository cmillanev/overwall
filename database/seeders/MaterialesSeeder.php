<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert([
            'material_nombre' => 'Canvas',
            'material_descripcion' => 'Descripción',
            'material_precio' => 100.00,
            'material_status' => 1,
            'material_image' => 'muestra.jpg',
        ]);
        DB::table('materials')->insert([
            'material_nombre' => 'Aluminio',
            'material_descripcion' => 'Descripción',
            'material_precio' => 100.00,
            'material_status' => 1,
            'material_image' => 'muestra.jpg',
        ]);
        DB::table('materials')->insert([
            'material_nombre' => 'Acrilico',
            'material_descripcion' => 'Descripción',
            'material_precio' => 100.00,
            'material_status' => 1,
            'material_image' => 'muestra.jpg',
        ]);
        DB::table('materials')->insert([
            'material_nombre' => 'Trovicel Laminado',
            'material_descripcion' => 'Descripción',
            'material_precio' => 100.00,
            'material_status' => 1,
            'material_image' => 'muestra.jpg',
        ]);
        DB::table('materials')->insert([
            'material_nombre' => 'Poster',
            'material_descripcion' => 'Descripción',
            'material_precio' => 100.00,
            'material_status' => 1,
            'material_image' => 'muestra.jpg',
        ]);
    }
}
