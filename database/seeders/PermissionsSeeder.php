<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $permissions_array = [];
        array_push($permissions_array, Permission::create(['name' => 'create_products']));
        array_push($permissions_array, Permission::create(['name' => 'edit_products']));
        array_push($permissions_array, Permission::create(['name' => 'delete_products']));
       //Se cea un permiso a aparte para el cliente, 
        $viewCustomerPermission = Permission::create(['name' => 'view_products']);
       //Mete el permiso al array 
        array_push($permissions_array, $viewCustomerPermission);
        //Se crea el rol admin, y se le asigna el array
        $superAdminRole = Role::create(['name' => 'admin']);
        $superAdminRole->syncPermissions($permissions_array);
        $operativoRole = Role::create(['name' => 'operativo']);
        $operativoRole->syncPermissions($permissions_array);
        //Se crea el cliente y se le asigna el permiso
        $viewCustomerRole = Role::create(['name' => 'cliente']);
        $viewCustomerRole->syncPermissions($viewCustomerPermission);

        //Crea usuario visitante
        $visitanteRole = Role::create(['name' => 'visitante']);

        $userSuperAdmin = User::create([
            'name' => 'Admin',
            'email' => 'admin@overwall.com',
            'password' => Hash::make('password'),
        ]);

        $userSuperAdmin->assignRole('admin');

        $userOperativo = User::create([
            'name' => 'Operativo',
            'email' => 'operativo@overwall.com',
            'password' => Hash::make('password'),
        ]);

        $userOperativo->assignRole('operativo');

        $userCustomer = User::create([
            'name' => 'Cliente',
            'email' => 'cliente@overwall.com',
            'password' => Hash::make('password'),
        ]);

        $userCustomer->assignRole('cliente');

        $userVisitante = User::create([
            'name' => 'Visitante',
            'email' => 'visitante@overwall.com',
            'password' => Hash::make('password'),
        ]);

        $userVisitante->assignRole('visitante');    
    }
}