<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrecioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Canvas',
            'medidas' => '100 X 80',
            'precio' => 2035
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Canvas',
            'medidas' => '90 X 60',
            'precio' => 1630
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Canvas',
            'medidas' => '60 X 40',
            'precio' => 905
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Canvas',
            'medidas' => '30 X 45',
            'precio' => 690
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Acrílico',
            'medidas' => '100 X 80',
            'precio' => 5950
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Acrílico',
            'medidas' => '90 X 60',
            'precio' => 4780
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Acrílico',
            'medidas' => '60 X 40',
            'precio' => 2520
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Acrílico',
            'medidas' => '30 X 45',
            'precio' => 2035
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Aluminio',
            'medidas' => '100 X 80',
            'precio' => 6650
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Aluminio',
            'medidas' => '90 X 60',
            'precio' => 5690
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Aluminio',
            'medidas' => '60 X 40',
            'precio' => 3210
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Aluminio',
            'medidas' => '30 X 45',
            'precio' => 2680
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Trovicel',
            'medidas' => '100 X 80',
            'precio' => 4320
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Trovicel',
            'medidas' => '90 X 60',
            'precio' => 3808
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Trovicel',
            'medidas' => '60 X 40',
            'precio' => 3289
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Trovicel',
            'medidas' => '30 X 45',
            'precio' => 3104
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Póster',
            'medidas' => '100 X 80',
            'precio' => 1300
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Póster',
            'medidas' => '90 X 60',
            'precio' => 1210
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Póster',
            'medidas' => '60 X 40',
            'precio' => 1050
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'getty',
            'material' => 'Póster',
            'medidas' => '30 X 45',
            'precio' => 950
        ]); 
        
        DB::table('precios')->insert([
            'modalidad' => 'room',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 3628
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'room',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 4723
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'room',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 3952
        ]); 
        
        DB::table('precios')->insert([
            'modalidad' => 'office',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 3637
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'office',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 4723
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'office',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 3952
        ]); 
      
        DB::table('precios')->insert([
            'modalidad' => 'fun',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 3706
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'fun',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 4585
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'fun',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 3892
        ]); 
        
        DB::table('precios')->insert([
            'modalidad' => 'home',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 4219
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'home',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 6100
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'home',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 4912
        ]); 

        DB::table('precios')->insert([
            'modalidad' => 'flag',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 4204
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'flag',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 6208
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'flag',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 4969
        ]);

        DB::table('precios')->insert([
            'modalidad' => 'space',
            'material' => 'Trovicel',
            'medidas' => '',
            'precio' => 3991
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'space',
            'material' => 'Acrílico',
            'medidas' => '',
            'precio' => 5443
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'space',
            'material' => 'Foamboard',
            'medidas' => '',
            'precio' => 4677
        ]);

        DB::table('precios')->insert([
            'modalidad' => 'picwall',
            'material' => '',
            'medidas' => '15 X 20',
            'precio' => 3991
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'picwall',
            'material' => '',
            'medidas' => '20 X 25',
            'precio' => 5443
        ]); 
        DB::table('precios')->insert([
            'modalidad' => 'picwall',
            'material' => '',
            'medidas' => '28 X 35',
            'precio' => 4677
        ]); 
    }
}
