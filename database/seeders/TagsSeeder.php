<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'tag_nombre' => 'Marinos',
            'tag_palabras' => 'Marinos',
            'tag_estatus' => 1
        ]); 
        DB::table('tags')->insert([
            'tag_nombre' => 'Atardecer sobre mar',
            'tag_palabras' => 'Atardecer sobre mar',
            'tag_estatus' => 1
        ]); 
        DB::table('tags')->insert([
            'tag_nombre' => 'Atardecer bosque',
            'tag_palabras' => 'atardecer bosque',
            'tag_estatus' => 1
        ]); 
    }
}
