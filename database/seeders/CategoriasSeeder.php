<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Abstracto',
            'categoria_palabras' => 'Abstracto',
            'categoria_url' => '/storage/categorias/abstracto.png',
            'categoria_url_fondo' => '/storage/categorias/abstracto_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Animales',
            'categoria_palabras' => 'Animales',
            'categoria_url' => '/storage/categorias/animales.png',
            'categoria_url_fondo' => '/storage/categorias/animales_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Arquitectura',
            'categoria_palabras' => 'arquitectura',
            'categoria_url' => '/storage/categorias/arquitectura.png',
            'categoria_url_fondo' => '/storage/categorias/arquitectura_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Arte',
            'categoria_palabras' => 'arte',
            'categoria_url' => '/storage/categorias/arte.png',
            'categoria_url_fondo' => '/storage/categorias/arte_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Autos',
            'categoria_palabras' => 'autos',
            'categoria_url' => '/storage/categorias/autos.png',
            'categoria_url_fondo' => '/storage/categorias/autos_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Botanica',
            'categoria_palabras' => 'botanica',
            'categoria_url' => '/storage/categorias/botanica.png',
            'categoria_url_fondo' => '/storage/categorias/botanica_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Chic',
            'categoria_palabras' => 'chic',
            'categoria_url' => '/storage/categorias/chic.png',
            'categoria_url_fondo' => '/storage/categorias/chic_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Ciencia',
            'categoria_palabras' => 'ciencia',
            'categoria_url' => '/storage/categorias/ciencia.png',
            'categoria_url_fondo' => '/storage/categorias/ciencia_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Ciudades',
            'categoria_palabras' => 'ciudades',
            'categoria_url' => '/storage/categorias/ciudades.png',
            'categoria_url_fondo' => '/storage/categorias/ciudades_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Cocina',
            'categoria_palabras' => 'cocina',
            'categoria_url' => '/storage/categorias/cocina.png',
            'categoria_url_fondo' => '/storage/categorias/cocina_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Comida',
            'categoria_palabras' => 'comida',
            'categoria_url' => '/storage/categorias/comida.png',
            'categoria_url_fondo' => '/storage/categorias/comida_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Deporte',
            'categoria_palabras' => 'deporte',
            'categoria_url' => '/storage/categorias/deporte.png',
            'categoria_url_fondo' => '/storage/categorias/deporte_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Detalles',
            'categoria_palabras' => 'detalles',
            'categoria_url' => '/storage/categorias/detalles.png',
            'categoria_url_fondo' => '/storage/categorias/detalles_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Fauna',
            'categoria_palabras' => 'fauna',
            'categoria_url' => '/storage/categorias/fauna.png',
            'categoria_url_fondo' => '/storage/categorias/fauna_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Laminas',
            'categoria_palabras' => 'laminas',
            'categoria_url' => '/storage/categorias/laminas.png',
            'categoria_url_fondo' => '/storage/categorias/laminas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Mapas',
            'categoria_palabras' => 'mapas',
            'categoria_url' => '/storage/categorias/mapas.png',
            'categoria_url_fondo' => '/storage/categorias/mapas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Mascotas',
            'categoria_palabras' => 'mascotas',
            'categoria_url' => '/storage/categorias/mascotas.png',
            'categoria_url_fondo' => '/storage/categorias/mascotas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Moda',
            'categoria_palabras' => 'moda',
            'categoria_url' => '/storage/categorias/moda.png',
            'categoria_url_fondo' => '/storage/categorias/moda_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Moderno',
            'categoria_palabras' => 'moderno',
            'categoria_url' => '/storage/categorias/moderno.png',
            'categoria_url_fondo' => '/storage/categorias/moderno_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Naturaleza',
            'categoria_palabras' => 'naturaleza',
            'categoria_url' => '/storage/categorias/naturaleza.png',
            'categoria_url_fondo' => '/storage/categorias/naturaleza_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Paisaje',
            'categoria_palabras' => 'paisaje',
            'categoria_url' => '/storage/categorias/paisaje.png',
            'categoria_url_fondo' => '/storage/categorias/paisaje_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Planetas',
            'categoria_palabras' => 'planetas',
            'categoria_url' => '/storage/categorias/planetas.png',
            'categoria_url_fondo' => '/storage/categorias/planetas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Plumas',
            'categoria_palabras' => 'plumas',
            'categoria_url' => '/storage/categorias/plumas.png',
            'categoria_url_fondo' => '/storage/categorias/plumas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Retrato',
            'categoria_palabras' => 'retrato',
            'categoria_url' => '/storage/categorias/retrato.png',
            'categoria_url_fondo' => '/storage/categorias/retrato_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Romantico',
            'categoria_palabras' => 'romantico',
            'categoria_url' => '/storage/categorias/romantico.png',
            'categoria_url_fondo' => '/storage/categorias/romantico_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Serpiente',
            'categoria_palabras' => 'serpiente',
            'categoria_url' => '/storage/categorias/serpiente.png',
            'categoria_url_fondo' => '/storage/categorias/serpiente_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Texturas',
            'categoria_palabras' => 'texturas',
            'categoria_url' => '/storage/categorias/texturas.png',
            'categoria_url_fondo' => '/storage/categorias/texturas_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Vino',
            'categoria_palabras' => 'vino',
            'categoria_url' => '/storage/categorias/vino.png',
            'categoria_url_fondo' => '/storage/categorias/vino_fondo.png',
            'categoria_estatus' => 1
        ]);
        DB::table('categorias')->insert([
            'categoria_nombre' => 'Vintage',
            'categoria_palabras' => 'vintage',
            'categoria_url' => '/storage/categorias/vintage.png',
            'categoria_url_fondo' => '/storage/categorias/vintage_fondo.png',
            'categoria_estatus' => 1
        ]);
    }
}
