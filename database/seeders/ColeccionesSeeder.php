<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColeccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Abstracto',
            'coleccion_palabras' => 'Abstracto',
            'coleccion_url' => '/storage/colecciones/abstracto.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Arquitectura',
            'coleccion_palabras' => 'Arquitectura',
            'coleccion_url' => '/storage/colecciones/arquitectura.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Autos',
            'coleccion_palabras' => 'Autos',
            'coleccion_url' => '/storage/colecciones/autos.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Detalles',
            'coleccion_palabras' => 'Detalles',
            'coleccion_url' => '/storage/colecciones/detalles.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Fauna',
            'coleccion_palabras' => 'Fauna',
            'coleccion_url' => '/storage/colecciones/fauna.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Moda',
            'coleccion_palabras' => 'Moda',
            'coleccion_url' => '/storage/colecciones/moda.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Paisaje',
            'coleccion_palabras' => 'Paisaje',
            'coleccion_url' => '/storage/colecciones/paisaje.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Plantas',
            'coleccion_palabras' => 'Plantas',
            'coleccion_url' => '/storage/colecciones/plantas.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Selección Overwall',
            'coleccion_palabras' => 'Paisaje',
            'coleccion_url' => '/storage/colecciones/seleccion overeall.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Tendencias',
            'coleccion_palabras' => 'tendencias',
            'coleccion_url' => '/storage/colecciones/tendencias.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Texturas',
            'coleccion_palabras' => 'texturas',
            'coleccion_url' => '/storage/colecciones/texturas.png',
            'coleccion_estatus' => 1
        ]);
        DB::table('colecciones')->insert([
            'coleccion_nombre' => 'Vintage',
            'coleccion_palabras' => 'vintage',
            'coleccion_url' => '/storage/colecciones/texturas.png',
            'coleccion_estatus' => 1
        ]);
    }
}
