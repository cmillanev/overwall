<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedidasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medidas')->insert([
            'medidas_nombre' => '',
            'medidas_precio' => 50.00,
            'medidas_x' => '110',
            'medidas_y' => '80',
            'medidas_unidad' => 'cm',
            'medidas_status' => 1
        ]);
        DB::table('medidas')->insert([
            'medidas_nombre' => '',
            'medidas_precio' => 50.00,
            'medidas_x' => '90',
            'medidas_y' => '60',
            'medidas_unidad' => 'cm',
            'medidas_status' => 1
        ]);
        DB::table('medidas')->insert([
            'medidas_nombre' => '',
            'medidas_precio' => 50.00,
            'medidas_x' => '60',
            'medidas_y' => '40',
            'medidas_unidad' => 'cm',
            'medidas_status' => 1
        ]);
        DB::table('medidas')->insert([
            'medidas_nombre' => '',
            'medidas_precio' => 50.00,
            'medidas_x' => '30',
            'medidas_y' => '45',
            'medidas_unidad' => 'cm',
            'medidas_status' => 1
        ]);
    }
}
