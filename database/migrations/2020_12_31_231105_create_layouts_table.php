<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layouts', function (Blueprint $table) {
            $table->id();
            $table->string('idUser');
            $table->string('medida');
            $table->string('material');
            $table->integer('cantidad');
            $table->float('precio', 8, 2);
            $table->float('total', 8, 2);
            $table->string('cupon')->nullable();
            $table->string('layout');
            $table->longText('url1')->nullable();
            $table->longText('url2')->nullable();
            $table->longText('url3')->nullable();
            $table->longText('url4')->nullable();
            $table->longText('url5')->nullable();
            $table->longText('url6')->nullable();
            $table->string('pagada')->nullable();
            $table->string('transaccion')->nullable();
            $table->string('url_recorte')->nullable();
            $table->string('url_recorte_2')->nullable();
            $table->string('url_recorte_3')->nullable();
            $table->string('url_recorte_4')->nullable();
            $table->string('url_recorte_5')->nullable();
            $table->string('url_recorte_6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layouts');
    }
}
