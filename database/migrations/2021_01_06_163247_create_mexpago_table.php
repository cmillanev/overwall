<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMexpagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mexpago', function (Blueprint $table) {
            $table->id();
            $table->string('folioMexPago');
            $table->string('numeroTransaccion');
            $table->string('pago');
            $table->string('monto');
            $table->string('numeroAut');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mexpago');
    }
}
