<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denvios', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('e_calle');
            $table->string('e_colonia');
            $table->string('e_numExt');
            $table->string('e_numInt');
            $table->string('e_mun');
            $table->string('e_ciudad');
            $table->string('e_estado');
            $table->string('e_pais');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denvios');
    }
}
