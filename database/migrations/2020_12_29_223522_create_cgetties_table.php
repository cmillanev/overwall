<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCgettiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cgetties', function (Blueprint $table) {
            $table->id();
            $table->string('idUser');
            $table->string('material');
            $table->string('medidas');
            $table->string('imagen');
            $table->longText('url');
            $table->integer('cantidad');
            $table->float('precio', 8, 2);
            $table->float('total', 8, 2);
            $table->string('cupon')->nullable();
            $table->string('pagada')->nullable();
            $table->string('transaccion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cgetties');
    }
}
