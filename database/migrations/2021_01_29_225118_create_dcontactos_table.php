<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDcontactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dcontactos', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('c_calle');
            $table->string('c_colonia');
            $table->string('c_numExt');
            $table->string('c_numInt');
            $table->string('c_mun');
            $table->string('c_ciudad');
            $table->string('c_estado');
            $table->string('c_pais');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dcontactos');
    }
}
