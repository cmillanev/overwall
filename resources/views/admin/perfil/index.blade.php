@extends('adminlte::page')

@section('plugins.Sweetalert2', true)
@section('css')
@livewireStyles
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop    


@section('title', 'Dashboard')

@section('content_header')
    <h1>Datos de tu cuenta</h1>
@stop

@section('content')
@role('operativo')
    Soy un operativo!
@endrole
@role('visitante')
    Soy un visitante...
@endrole
@role('admin')
   
   En contrucción
    

@endrole
@role('cliente')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
              <livewire:credenciales />
              </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <livewire:envio />
              </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <livewire:ndestintario />
              </div>
        </div>
    </div>
@endrole

@stop

@section('js')
@livewireScripts
@if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Datos actualizados!',
        '',
        'success'
        )
</script>
<script>
  window.addEventListener('swal:modal', event => {
    Swal.fire(
        '¡Datos actualizados!',
        '',
        'success'
        )
  });
  </script>
@endif
@stop