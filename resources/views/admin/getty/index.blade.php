@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <meta name="_token" content="{{ csrf_token() }}">
  
    <style>
       /*

All grid code is placed in a 'supports' rule (feature query) at the bottom of the CSS (Line 77). 
        
The 'supports' rule will only run if your browser supports CSS grid.

Flexbox is used as a fallback so that browsers which don't support grid will still recieve an identical layout.

*/

@import url(https://fonts.googleapis.com/css?family=Montserrat:500);

:root {
	/* Base font size */
	font-size: 10px;
}

*,
*::before,
*::after {
	box-sizing: border-box;
}

body {
	min-height: 100vh;
	background-color: #fafafa;
}

.container {
	max-width: 100rem;
	margin: 0 auto;
	padding: 0 2rem 2rem;
}

.heading {
	font-family: "Montserrat", Arial, sans-serif;
	font-size: 4rem;
	font-weight: 500;
	line-height: 1.5;
	text-align: center;
	padding: 3.5rem 0;
	color: #1a1a1a;
}

.heading span {
	display: block;
}

.gallery {
	display: flex;
	flex-wrap: wrap;
	/* Compensate for excess margin on outer gallery flex items */
	margin: -1rem -1rem;
}

.gallery-item {
	/* Minimum width of 24rem and grow to fit available space */
	flex: 1 0 24rem;
	/* Margin value should be half of grid-gap value as margins on flex items don't collapse */
	margin: 1rem;
	box-shadow: 0.3rem 0.4rem 0.4rem rgba(0, 0, 0, 0.4);
	overflow: hidden;
}

.gallery-image {
	display: block;
	width: 100%;
	height: 100%;
	object-fit: cover;
	transition: transform 400ms ease-out;
}

.gallery-image:hover {
	transform: scale(1.15);
}

/*

The following rule will only run if your browser supports CSS grid.

Remove or comment-out the code block below to see how the browser will fall-back to flexbox styling. 

*/

@supports (display: grid) {
	.gallery {
		display: grid;
		grid-template-columns: repeat(auto-fit, minmax(24rem, 1fr));
		grid-gap: 2rem;
	}

	.gallery,
	.gallery-item {
		margin: 0;
	}
}

    </style>

@stop

@section('plugins.Sweetalert2', false)
    

@section('title', 'Dashboard')

@section('content_header')
    <h1>Getty Images</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <input class="form-control btn-submit" type="text" name="idSucursal" placeholder="Buscar">           
    </div>
</div>

<div id="f315-div">


    <div class="row">
     @foreach ($data as $dat)
     @for ($i = 0; $i < 20; $i++)

     

      <div class="gallery">

		<div class="gallery-item">
			<img class="gallery-image" src="{{asset($dat['images'][$i]['display_sizes'][0]['uri'])}}" alt="person writing in a notebook beside by an iPad, laptop, printed photos, spectacles, and a cup of coffee on a saucer">
		</div>

	</div>

       
   
    
           @endfor 
     @endforeach
   
    </div>
</div>


@stop



@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>


<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  
      $(".btn-submit").keyup(function(e){
       
          e.preventDefault();
          if (e.keyCode == 13) {

  
          var name = $("input[name=idSucursal]").val();
          var mun = "";
         
  
          console.log(name);
  
          $.ajax({
             type:'POST',
             url:"{{ route('search.post') }}",
             data:{name:name, mun:mun, _token: '{{csrf_token()}}'},
             success: function (response) {
                  $('#f315-div').html(response);
              },
          });
          }
      });
  
    </script>
  

<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'fitImagesInViewport': true
    });
</script>

    
@stop