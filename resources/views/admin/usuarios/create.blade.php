@extends('adminlte::page')

@section('css')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('plugins.Datatables', true)

@section('title', 'Atributos')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Crear usuario</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
          <li class="breadcrumb-item"><a href="{{url('admin/usuarios')}}">Usuarios</a></li>
          <li class="breadcrumb-item active">Crear</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            Nuevo usuario
          </div>
          <div class="card-body">
          <form action="{{ route('usuarios.store') }}" method="post">
            @csrf
          <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" name="name" required class="form-control">
          </div>
          <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" required class="form-control">
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" name="password" required class="form-control">
          </div>
          <div class="form-group">
            <label for="rol">Rol</label>
            <select class="form-control"  name="rol" id="">
             @foreach ($roles as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
             @endforeach
            </select>
          </div>
        
          </div>
          <div class="card-footer">
           
              <input class="btn btn-success" type="submit" value="Enviar">
        
          </div>
        </form>
        </div>
      </div>
    </div>
</div>

@endsection
@section('js')

@stop