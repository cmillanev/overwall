@extends('adminlte::page')

@section('css')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('plugins.Sweetalert2', false)

@section('title', 'Crear Categoría')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Categorías</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item">Categorías</li>
            <li class="breadcrumb-item active">Crear</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')

<div class="justify-content-end" >

    
   
 </div>
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">
            
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Creando nueva categoría</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                 <form role="form" action="{{ route('categorias.store') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="titulo">Título de la categoría</label>
                      <input type="text" class="form-control" id="titulo" name="categoria_nombre" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="palabras">Frase de búsqueda en getty</label>
                      <input type="text" class="form-control" id="palabras" name="categoria_palabras" placeholder="palabras">
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="form-group">
                          <label for="exampleFormControlFile1">Seleccione icono</label>
                          <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file">
                        </div>
                    </div>
                    <div class="input-group">
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Seleccione icono con fondo</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file1">
                      </div>
                  </div>
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" name="categoria_estatus" id="exampleCheck1" value="1">
                      <label class="form-check-label" for="exampleCheck1">Activa</label>
                    </div>
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
           

        </div>
    </div>
</div>
@endsection
@section('js')
   @if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Excelente!',
        'Los cambios se han realizado',
        'success'
        )
</script>
    
@endif

@stop