<form id="formulario-borrar" action="{{ route('categorias.destroy', $categoria )}}" method="POST" style="display:inline-block;">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
</form>