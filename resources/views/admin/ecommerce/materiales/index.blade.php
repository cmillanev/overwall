@extends('adminlte::page')

@section('css')
@livewireStyles
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('plugins.Datatables', true)

@section('title', 'Atributos')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Materiales (Getty)</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item active">Materiales</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')
<div class="container">
   
  @livewire('materiales.contenedor')

</div>
@endsection
@section('js')
@livewireScripts
<script>
    $(function () {
   
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
@stop