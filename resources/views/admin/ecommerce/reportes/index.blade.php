@extends('adminlte::page')

@section('plugins.Sweetalert2', true)

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<div class="row">
    <div class="col-lg-3  col-sm-6">
        <div class="dashboard-stat2 bordered">
<div class="display">
    <div class="number">
        <h3 class="font-green-sharp">
            <span data-counter="counterup" data-value="0">0</span>
        </h3>
        <small>Today revenue</small>
    </div>
    <div class="icon">
        <i class="far fa-money-bill-alt"></i>
    </div>
</div>
</div>
    </div>
    <div class="col-lg-3  col-sm-6">
        <div class="dashboard-stat2 bordered">
<div class="display">
    <div class="number">
        <h3 class="font-blue-sharp">
            <span data-counter="counterup" data-value="0">0</span>
        </h3>
        <small>Today orders</small>
    </div>
    <div class="icon">
        <i class="icon-basket"></i>
    </div>
</div>
</div>      
  </div>
    <div class="col-lg-3  col-sm-6">
        <div class="dashboard-stat2 bordered">
<div class="display">
    <div class="number">
        <h3 class="font-red-haze">
            <span data-counter="counterup" data-value="0">0</span>
        </h3>
        <small>Total products</small>
    </div>
    <div class="icon">
        <i class="fab fa-product-hunt"></i>
    </div>
</div>

</div>       
 </div>
    <div class="col-lg-3  col-sm-6">
        <div class="dashboard-stat2 bordered">
<div class="display">
    <div class="number">
        <h3 class="font-purple-soft">
            <span data-counter="counterup" data-value="0">0</span>
        </h3>
        <small>Total customers</small>
    </div>
    <div class="icon">
        <i class="icon-user"></i>
    </div>
</div>
</div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop