@extends('adminlte::page')

@section('css')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)

@section('title', 'Etiquetas')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Etiquetas</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item active">Etiquetas</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')

<div class="justify-content-end" >


    <a href=" {{ url('admin/tags/create') }} " class="btn btn-success">Agregar Etiqueta</a>
 </div>
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">
            

            <!-- Default box -->
            <div class="box"> 
                <div class="box-header with-border">

                  

                </div>
                <div class="box-body">

                  <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Descripción</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                        @foreach ($tags as $tag)
                        <tr>
                            <td>{{$tag->id}}</td>
                            <td>{{$tag->tag_nombre }}</td>
                            <td>{{$tag->categoria_nombre }}</td>
                            <td>{{$tag->tag_palabras}}</td>
                            @if ($tag->tag_estatus == 1)
                            <td>Activo</td>
                            @else 
                            <td>Inactivo</td>
                            @endif
                            <td>
                              <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                              <form id="formulario-borrar" action="{{ route('tags.destroy', $tag->id )}}" method="POST" style="display:inline-block;">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                            </form>
                            
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(function () {
   
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })



    $('#formulario-borrar').submit(function(e){
      e.preventDefault();
      Swal.fire({
      title: '¿Seguro que quieres eliminar la categoría?',
      text: "No se podrá recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: 'Cancelar!'
    }).then((result) => {
      if (result.value) {
      
        this.submit();
        }
      })
    });
        
  </script>
@stop