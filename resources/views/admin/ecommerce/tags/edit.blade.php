@extends('adminlte::page')

@section('css')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('plugins.Sweetalert2', false)

@section('title', 'Crear Categoría')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Etiquetas</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item">Etiquetas</li>
            <li class="breadcrumb-item active">Crear</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')

<div class="justify-content-end" >

    
   
 </div>
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12 ">
            
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Editar etiqueta</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                 <form role="form" action="{{ route('tags.update', $tags[0]->id) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                    <div class="form-group">
                        <label for="rol">Categoría superior</label>
                        <select class="form-control"  name="categoria_id" id="">
                        <option value="{{ $tags[0]->categoria_id }}">{{ $tags[0]->categoria_nombre }}</option>
                         @foreach ($categorias as $categoria)
                        <option value="{{$categoria->id}}">{{$categoria->categoria_nombre}}</option>
                         @endforeach
                        </select>
                      </div>
                    <div class="form-group">
                      <label for="titulo">Título de la etiqueta</label>
                      <input type="text" class="form-control" id="titulo" name="tag_nombre" value="{{ $tags[0]->tag_nombre }}">
                    </div>
                    <div class="form-group">
                      <label for="palabras">Palabras</label>
                      <input type="text" class="form-control" id="palabras" name="tag_palabras" value="{{ $tags[0]->tag_palabras }}">
                    </div>
                    <div class="form-group">
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" name="tag_estatus" id="exampleCheck1" value="1">
                      <label class="form-check-label" for="exampleCheck1">Activa</label>
                    </div>
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
           

        </div>
    </div>
</div>
@endsection
@section('js')
   @if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Excelente!',
        'Los cambios se han realizado',
        'success'
        )
</script>
    
@endif

@stop