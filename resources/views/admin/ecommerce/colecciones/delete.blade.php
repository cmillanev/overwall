<form id="formulario-borrar" action="{{ route('colecciones.destroy', $coleccion )}}" method="POST" style="display:inline-block;">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
</form>