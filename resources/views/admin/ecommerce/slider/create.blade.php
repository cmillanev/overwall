@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" />
@stop



@section('title', 'Sliders')

@section('content_header')
    <h1>Subir imagenes</h1>
@stop


@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
               
                  <form action="{{route('sliders.store')}}" class="dropzone" id="my-awesome-dropzone" method="POST" >
                    @csrf
                 
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"  crossorigin="anonymous"></script>
<script>
    // "myAwesomeDropzone" is the camelized version of the HTML element's ID
Dropzone.options.myAwesomeDropzone = {
  headers:{
      'X-CSRF-TOKEN' : "{{csrf_token()}}"
  },
};
</script>
@stop