@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" />
@stop

@section('plugins.Sweetalert2', true)

@section('title', 'Sliders')

@section('content_header')
    <h1>Sliders</h1>
@stop

@section('content')

<div class="container">
   <div class="row">
       <div class="col-3">
          <a href=" {{ route('sliders.create') }} " class="btn btn-success">Agregar</a>
       </div>
   </div>
    <div class="row">
        @foreach ($sliders as $slider)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset($slider->url)}}" alt="" class="img-fluid">
                    <div class="card-footer">
                         <a href="{{route('sliders.edit', $slider)}}" class="btn btn-primary">Editar</a>
                        <form action="{{route('sliders.destroy', $slider)}}" class="d-inline" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger" >Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>



@stop


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"  crossorigin="anonymous"></script>
<script>
    // "myAwesomeDropzone" is the camelized version of the HTML element's ID
Dropzone.options.myAwesomeDropzone = {
  headers:{
      'X-CSRF-TOKEN' : "{{csrf_token()}}"
  },
};
</script>
@stop