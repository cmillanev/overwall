@extends('adminlte::page')

@section('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
@endsection

@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)

@section('title', 'Colecciones')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Ordenes de compra</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item active">Ordenes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')

@role('admin')
<div class="justify-content-end" >


   
</div>
<div class="container-fluid spark-screen">
   <div class="row">
       <div class="col-md-12 ">
           

           <!-- Default box -->
           <div class="box"> 
               <div class="box-header with-border">

                 

               </div>
               <div class="box-body">

                 <table id="example1" class="table table-striped table-bordered" style="width:100%">
                       <thead class="thead-success">
                           <th>Fecha</th>
                           <th>Id</th>
                           <th>Folio</th>
                           <th>Transaccion</th>
                           <th>Estatus</th>
                           <th>Ver</th>
                          
                           
                       </thead>
                       <tbody>
                       @foreach ($ordenes as $orden)
                       <tr>
                        <td>{{$orden->created_at}}</td>
                           <td>{{$orden->id}}</td>
                           <td>{{$orden->folioMexPago}}</td>
                           <td>{{$orden->numeroTransaccion}}</td>
                           <td>{{$orden->estatus_pedido}}</td>
                           <td>
                               <a href="{{ route('ordenes.show', $orden->numeroTransaccion) }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                           </td>
                           
                       </tr>
                       @endforeach
                       </tbody>
                   </table>
               </div>
               <!-- /.box-body -->
           </div>
           <!-- /.box -->

       </div>
   </div>
</div>
@endrole
@role('cliente')
<div class="justify-content-end" >


   
</div>
<div class="container-fluid spark-screen">
   <div class="row">
       <div class="col-md-12 ">
           

           <!-- Default box -->
           <div class="box"> 
               <div class="box-header with-border">

                 

               </div>
               <div class="box-body">

                 <table id="example1" class="table table-striped table-bordered" style="width:100%">
                       <thead class="thead-success">
                           <th>Fecha Pedido</th>
                           <th>Estatus</th>
                           <th>Folio Pago</th>
                           <th>Transacción</th>
                           <th>Pago</th>
                           <th>Monto</th>
                          
                           
                       </thead>
                       <tbody>
                       @foreach ($orden as $orde)
                       <tr>
                          <td>{{$orde->created_at}}
                          
                          </td>
                           <td>Preparando</td>
                           <td>{{$orde->folioMexPago}}</td>
                           <td>{{$orde->numeroTransaccion}}</td>
                           <td>{{$orde->pago}}</td>
                           <td>
                           ${{number_format($orde->monto), 2}}.00
                           </td>
                           
                       </tr>
                       @endforeach
                       </tbody>
                   </table>
               </div>
               <!-- /.box-body -->
           </div>
           <!-- /.box -->

       </div>
   </div>
</div>
@endrole
@endsection
@section('js')
<script>
    $(function () {
   
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })

        
  </script>
@stop