@extends('adminlte::page')

@section('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
<style>
    .card-header{
        background-color: #123F6F;
        color: aliceblue;
    }

table {
   width: 100%;
}

 .img_grade{
    width: 200px;
    height: auto;
    display: block;
    margin-left: auto;
 
}
 .img_chico{
  width: 110px;
  display: block;
  margin-right: 125px;
   
}
</style>
@endsection

@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)

@section('title', 'Orden de compra')

@section('content_header')

  
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
   
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item active">Ordenes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section('content')
 
<div class="card">
    <div class="card-header">
        Detalles del pedido
    </div>
    <form action="{{route('ordenes.update', $estatus[0]->id)}}" method="post">
        @csrf
        @method('PUT')
    <div class="card-body">
        <div class="row">
        <div class="form-group col-md-5">
            <label for="">Nombre del cliente:</label>
           <input class="form-control" type="text" value="{{ $cliente[0]->name }}"> 
        </div>
        <div class="form-group col-md-3">
            <label for="">Destinatario:</label>
           <input class="form-control" type="text" value="{{ $destinatario[0]->d_nombre }}"> 
        </div>
        <div class="form-group col-md-4">
            <label for="">Email:</label>
           <input class="form-control" type="text" value="{{ $cliente[0]->email }}"> 
        </div>
    </div>
       
         
        <div class="form-group">
            <label for="">Dirección de envío:</label>
           <input class="form-control" type="text" value="{{ $direccion}}"> 
        </div>
        <div class="row">
            
           <div class="form-group col-md-3">
               <label for="">Estatus</label>
               <select class="form-control" name="estatus_pedido" id="">
               <option value="{{$estatus[0]->estatus_pedido}}">{{$estatus[0]->estatus_pedido}}</option>    
               <option value="Nuevo">Nuevo</option>    
               <option value="Producción">Producción</option>    
               <option value="Enviado">Enviado</option>
               <option value="Entregado">Entregado</option>
               </select>   
           </div>    
           
           <div class="form-group col-md-9">
               <label for="guia_envio">No. de guía</label>
            <input class="form-control" type="text" name="guia_envio" value=""> 
           </div>
             
  
       </div>  
    </div>
    <div class="card-footer">
        <div class="form-group col-md-3">
        <input type="submit" class="form-control btn btn-primary" value="Actualizar">
        </div>
    </div>
    </form>
</div>

<div class="card">
    <div class="card-body">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
            @if (isset($ordenGetty[0]->id))    
               <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Getty Images</a>  
             @endif   
             @if (isset($ordenLayout[0]->id))    
             <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Layouts</a>
             @endif
             @if (isset($ordenFrame[0]->id))  
             <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Tu wall</a>          
             @endif
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            @if (isset($ordenGetty[0]->id))  
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
               @foreach ($ordenGetty as $getty)
                <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $getty->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Medidas:</label>
                                <input class="form-control" type="text" value="{{ $getty->medidas }} cm">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $getty->cantidad }}">
                            </div>
                            <div class="form-group">
                                <label for="">Id Imagen Getty:</label>
                                <input class="form-control" type="text" value="{{ $getty->imagen }}">
                            </div>
                            <div class="form-group">
                                <label for="">Url Imagen Getty:</label>
                                <input class="form-control" type="text" value="{{ $getty->url }}">
                            </div>
                        </form>
                    </div>
                    <div class="col-6">
                        <div class="card margen " style="width: 28rem; " id="card">
                            <img src="{{$getty->url}}" alt="" class="img-fluid">
                          
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
            </div>
            @endif
            @if (isset($ordenLayout[0]->id))  
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                @foreach ($ordenLayout as $layout)
                @if ($layout->layout == 'Room')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2">
                                                <img class="img_grande" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                             
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                @if ($layout->layout == 'Office')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-3">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Original 2:</label>
                                <a download="o_space_2.jpg" href="{{$layout->url2}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Original 3:</label>
                                <a download="o_space_3.jpg" href="{{$layout->url3}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Original 4:</label>
                                <a download="o_space_4.jpg" href="{{$layout->url4}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-3">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Recorte 2:</label>
                                <a download="r_space_2.jpeg" href="{{$layout->url_recorte_2}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Recorte 3:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_3}}">Descarga</a>
                            </div>
                            <div class="form-group col-3">
                                <label for="">Recorte 4:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_4}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_2}}" alt="">  
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_3}}" alt="">
                                            </td>   
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_4}}" alt="">
                                            </td>   
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                @if ($layout->layout == 'Fun')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 2:</label>
                                <a download="o_space_2.jpg" href="{{$layout->url2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 3:</label>
                                <a download="o_space_3.jpg" href="{{$layout->url3}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 2:</label>
                                <a download="r_space_2.jpeg" href="{{$layout->url_recorte_2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 3:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_3}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2">
                                                <img class="img_grande" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_2}}" alt="">  
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_3}}" alt="">
                                            </td>   
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                @if ($layout->layout == 'Home')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 2:</label>
                                <a download="o_space_2.jpg" href="{{$layout->url2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 3:</label>
                                <a download="o_space_3.jpg" href="{{$layout->url3}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 2:</label>
                                <a download="r_space_2.jpeg" href="{{$layout->url_recorte_2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 3:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_3}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2">
                                                <img class="img_grande" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_2}}" alt="">  
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_3}}" alt="">
                                            </td>   
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                @if ($layout->layout == 'Flag')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 2:</label>
                                <a download="o_space_2.jpg" href="{{$layout->url2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 3:</label>
                                <a download="o_space_3.jpg" href="{{$layout->url3}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 2:</label>
                                <a download="r_space_2.jpeg" href="{{$layout->url_recorte_2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 3:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_3}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2">
                                                <img class="img_grande" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_2}}" alt="">  
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_3}}" alt="">
                                            </td>   
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                @if ($layout->layout == 'Space')
                   <div class="row">
                    <div class="col-6">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tipo Layout:</label>
                                <input class="form-control" type="text" value="{{ $layout->layout }}">
                            </div>
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $layout->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $layout->cantidad }}">
                            </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Original 1:</label>
                                <a download="o_space_1.jpg" href="{{$layout->url1}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 2:</label>
                                <a download="o_space_2.jpg" href="{{$layout->url2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Original 3:</label>
                                <a download="o_space_3.jpg" href="{{$layout->url3}}">Descarga</a>
                            </div>
                        </div>
                            <div class="row">
                            <div class="form-group col-4">
                                <label for="">Recorte 1:</label>
                                <a download="r_space_1.jpeg" href="{{$layout->url_recorte}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 2:</label>
                                <a download="r_space_2.jpeg" href="{{$layout->url_recorte_2}}">Descarga</a>
                            </div>
                            <div class="form-group col-4">
                                <label for="">Recorte 3:</label>
                                <a download="r_space_3.jpeg" href="{{$layout->url_recorte_3}}">Descarga</a>
                            </div>
                        </div>
                        
                           
                        </form>
                    </div>
                    <div class="col-6">
                        
                        <div class="card margen ">
                            <div class="card-body">

                                <table ">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2">
                                                <img class="img_grande" src="{{$layout->url_recorte}}" alt="">
                                            </td>
                                          
                                            <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_2}}" alt="">  
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                             <td>
                                                <img class="img_chico" src="{{$layout->url_recorte_3}}" alt="">
                                            </td>   
                                        </tr>
                                    </tbody>
                                </table>
                         
                            </div>
                        </div>
                    </div>
                </div>    
                @endif
                
                @endforeach
              
            </div>
            @endif
            @if (isset($ordenFrame[0]->id))  
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                @foreach ($ordenFrame as $orden)
                    
                
                <div class="row">
                    <div class="col-4">
                        <form action="">
                            <div class="form-group">
                                <label for="">Material:</label>
                                <input class="form-control" type="text" value="{{ $orden->material }}">
                            </div>
                            <div class="form-group">
                                <label for="">Color:</label>
                                <input class="form-control" type="text" value="{{ $orden->color }}">
                            </div>
                            <div class="form-group">
                                <label for="">Medidas:</label>
                                <input class="form-control" type="text" value="{{ $orden->medidas }}">
                            </div>
                            <div class="form-group">
                                <label for="">Cantidad:</label>
                                <input class="form-control" type="text" value="{{ $orden->cantidad }}">
                            </div>
                            <div class="form-group">
                                <label for="">Imagen Original:</label>
                                <a download="{{$orden->transaccion}}tuwall-original.jpg" href="{{$orden->url}}">Descarga</a>
                            </div>
                            <div class="form-group">
                                <label for="">Imagen corte:</label>
                                <a download="{{$orden->transaccion}}tuwall-corte.jpg" href="{{$orden->url_recorte}}">Descarga</a>
                            </div>
                        </form>
                    </div>
                    <div class="col-8">
                        <div class="card margen " style="width: 30%; " id="card">
                            <img src="{{$orden->url_recorte}}" alt="" class="img-fluid">
                          
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach
            </div>
            @endif
          </div>
    </div>
</div>

   
@endsection
@section('js')

@if (session('Actualizar') == 'Estatus')

<script>
   Swal.fire(
        '¡Se actualizó el estatus! ',
        '',
        'success'
        )

  
</script>
    
@endif

@stop