<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Narrative Welcome Email</title>
  <style type="text/css">

  /* Take care of image borders and formatting */

  @font-face{
  font-family: 'menu';
  src: url('fonts/DIN Alternate Bold.otf');
}
@font-face{
  font-family: 'gotham-bold';
  src: url('fonts/Gotham-Bold.ttf');
}

@font-face{ 
  font-family: 'gotham-medium';
  src: url('fonts/Gotham-Medium.otf');
}
@font-face{
  font-family: 'din-pro-medium';
  src: url('fonts/DINPro Medium.otf');
}
@font-face{
  font-family: 'din-pro-regular';
  src: url('fonts/DINPro Regular.otf');
}
@font-face{
  font-family: 'din-pro-light';
  src: url('tipografias_overwall/DINPro-Light.otf');
}

  img {
    max-width: 100%;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
  }

  a {
    border: 0;
    outline: none;
  }

  a img {
    border: none;
  }

  /* General styling */

  td, h1, h2, h3  {
    font-family: Helvetica, Arial, sans-serif;
    font-weight: 400;
  }

  td {
    font-size: 13px;
    line-height: 19px;
    text-align: left;
  }

  body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #37302d;
    background: #ffffff;
  }

  table {
    border-collapse: collapse !important;
  }


  h1, h2, h3, h4 {
    padding: 0;
    margin: 0;
    color: #444444;
    font-weight: 400;
    line-height: 110%;
  }

  h1 {
    font-size: 35px;
  }

  h2 {
    font-size: 30px;
  }

  h3 {
    font-size: 24px;
  }



  .important-font {
    color: #21BEB4;
    font-weight: bold;
  }

  .hide {
    display: none !important;
  }

  .force-full-width {
    width: 100% !important;
  }

  .redes{
    width: 10px;
  }

  </style>

  <style type="text/css" media="screen">
      @media screen {
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400);

        /* Thanks Outlook 2013! */
        td, h1, h2, h3 {
          font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif !important;
        }
      }
  </style>

  <style type="text/css" media="only screen and (max-width: 600px)">
    /* Mobile styles */
    @media only screen and (max-width: 600px) {

      table[class="w320"] {
        width: 320px !important;
      }

      table[class="w300"] {
        width: 300px !important;
      }

      table[class="w290"] {
        width: 290px !important;
      }

      td[class="w320"] {
        width: 320px !important;
      }

      td[class~="mobile-padding"] {
        padding-left: 14px !important;
        padding-right: 14px !important;
      }

      td[class*="mobile-padding-left"] {
        padding-left: 14px !important;
      }

      td[class*="mobile-padding-right"] {
        padding-right: 14px !important;
      }

      td[class*="mobile-padding-left-only"] {
        padding-left: 14px !important;
        padding-right: 0 !important;
      }

      td[class*="mobile-padding-right-only"] {
        padding-right: 14px !important;
        padding-left: 0 !important;
      }

      td[class*="mobile-block"] {
        display: block !important;
        width: 100% !important;
        text-align: left !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        padding-bottom: 15px !important;
      }

      td[class*="mobile-no-padding-bottom"] {
        padding-bottom: 0 !important;
      }

      td[class~="mobile-center"] {
        text-align: center !important;
      }

      table[class*="mobile-center-block"] {
        float: none !important;
        margin: 0 auto !important;
      }

      *[class*="mobile-hide"] {
        display: none !important;
        width: 0 !important;
        height: 0 !important;
        line-height: 0 !important;
        font-size: 0 !important;
      }

      td[class*="mobile-border"] {
        border: 0 !important;
      }
    }

    h4{
      text-align: center;
      color:#123f6f;
      font-family: 'din-pro-regular';
    }
    p{
      color: #123f6f
    }

    .redes{
      margin-left: 2%;
      width: 35px;
     
    }
    #red{
      margin-left: 10%;
      
     
    }
    #cintillo{
      color: #E3E3E2;
      font-size: 14pt;
      font-family: 'din-pro-regular';
    }
    #parrafo{
      font-size: 14pt;
      font-family: 'din-pro-regular';
    }
    #pureti{
      margin-left: 25%;
    }
  </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<table width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="background: #ffffff;" width="100%"><center>
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<th style="width: 100%;"><img id="imgbienvenida" src="https://overwall.com.mx/emails/bienvenida/header_bienvenida.png" alt="" /></th>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td style="border-bottom: 1px solid #e7e7e7;"><center>
<table class="w320" style="width: 0px;" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mobile-hide" style="padding-top: 20px; padding-bottom: 0px; vertical-align: bottom; width: 600px;">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 20px 20px 0px; width: 6.66667%;">&nbsp;</td>
<td class="mobile-padding" style="padding: 20px 20px 0px; width: 93.3333%;" align="left"><br class="mobile-hide" />
<h3><strong>&iexcl;Tu registro ha sido exitoso con Overwall!</strong></h3>
<br />
<p id="parrafo">Con este correo y contrase&ntilde;a asignada podr&aacute;s entrar a la p&aacute;gina para hacer pedidos, consutar el estado de tu orden y estar en contacto con nosotros<br /><br />Recuerda que todos los productos Overwall purificar&aacute;n el aire que respiras, gracias a su asombrosa tecnolog&iacute;a Pureti. Llena tu casa con arte, tus mejores recuerdos y aire fresco.</p>
<img id="pureti" src="https://overwall.com.mx/emails/bienvenida/circulos_bienvenida.png" alt="" width="220" height="197" /><br /><br /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td style="background-color: #ffffff; border-bottom: 1px solid #ffffff;" valign="top"><center>
<table class="w320" style="height: 100%;" border="0" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="mobile-padding" style="padding: 20px;" valign="top"><br />
<p id="cintillo">Si no te registraste a Overwall con este correo, haz <strong>Click Aqu&iacute;</strong></p>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
<tr>
<td style="background-color: #123f6f; height: 120px;"><center>
<table class="w320" style="height: 100%; color: #123f6f;" border="0" width="600" cellspacing="0" cellpadding="0" bgcolor="#1f1f1f">
<tbody>
<tr>
<td class="mobile-padding" style="font-size: 12px; padding: 20px; background-color: #123f6f; color: #ffffff; text-align: left;" align="right" valign="middle"><a style="color: #ffffff;" href="https://overwall.com.mx">NUESTRA P&Aacute;GINA</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a style="color: #ffffff;" href="https://overwall.com.mx/soporte">SOPORTE</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a style="color: #ffffff;" href="https://overwall.com.mx/politicas">POL&Iacute;TICAS</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a style="color: #ffffff;" href="https://www.facebook.com/overwallmx"><img id="red" class="redes" src="https://overwall.com.mx/emails/confirmacion/facebook.png" alt="" /></a><a style="color: #ffffff;" href="https://www.instagram.com/overwallmx/"><img class="redes" src="https://overwall.com.mx/emails/confirmacion/instagram.png" alt="" /></a><a style="color: #ffffff;" href="https://twitter.com/OverwallM"><img class="redes" src="https://overwall.com.mx/emails/confirmacion/twitter.png" alt="" /></a><a style="color: #ffffff;" href="https://www.youtube.com/channel/UCGEo_Uqb-UAF6N28ypyrFuA"><img class="redes" src="https://overwall.com.mx/emails/confirmacion/youtube.png" alt="" /></a></td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>