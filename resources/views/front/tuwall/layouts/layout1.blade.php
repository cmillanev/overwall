<div class="mostrarlay1">
    <div class="row subes">
      <div class="col-md-12 order-md-2">
        <h2 class="featurette-heading ">SUBE TUS FOTOS</h2>
      </div>
     
    </div>

    <div class="row ">
      <div class="col-md-1"></div>
      <div class="col-md-6 order-md-1">
       
        <div class="row">
          <div class="card" style="background: #E3E3E2" >
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
              @csrf
            <div class="file-upload" id="drop-area">
              <!--place upload image/icon first !-->
              <img id="avatarimage" src="{{asset('front/carga.png')}}" />
              <!--place input file last !-->
              <input type="file" name="evidencia[]" id="input" accept="image/*" name="url1"  />
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h5 class="subtitulos">IMPORTANTE:</h5><br>
            <p class="parrafos">Cuida que tus fotos estén en alta resolución y con buen calidad. Recomendamos un minimo de 600 x 600 px y 300 dpi.</p>
            <img src="{{asset('front/iconos pago 1.png')}}" alt="" style="height: 50px;">
          </div>
          <div class="col-md-6">
            <a href=""><h5 class="subtitulos2">GUÍA DE <br> RECOMENDACIONES</h5></a>
          </div>
        </div>
      </div>
      <div class="col-md-5 order-md-2">
        
        <p style="color: #123f6f;" id="letramarco">{{$titulo}}</p>
        <p> {{$descripcion}}</p>

        <div class="form-group col-md-5">
         
            <label for="exampleFormControlSelect1">Impreso sobre</label>
              <select  name="materiales"  id="materialesl"  class="form-control btn-lay" required>
                <option value="">Seleccione</option>
             @foreach ($materiales as $material)
                <option value="{{ $material->material_nombre }}">{{ $material->material_nombre }}</option>
             @endforeach
         </select>  
       
       </div>
      <div class="form-group col-md-5">
        <label for="">Cantidad</label>
        <input type="number" class="form-control btn-lay" name="cantidad" value="1">
    </div>


   <p class="precio"> MX $<span id="f314-div">0</span></p>

    <br> @if (Auth::check())
    <input type="hidden" name="layout" value="1">
    <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 
   </form>
        @else
          <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
      Registrarse
    </button>
    <button type="button" class="btn btn-primary" >
      Inicia sesión
    </button>

    @endif

    </div>
      </div>
    </div>