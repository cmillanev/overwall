@extends('layouts.app2') @section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.css">
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">

<style>

<style>


.row{
  margin-left: 30px;
}

.iconospago{
  height: 40px;
}


      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .opciones1{
        text-align:center;
       
      }
      .opciones1 img{
       margin-top: 8%; 
       margin-bottom: 8%; 
       width: 45%;
       cursor:pointer;
      }
      .opciones{
        height: 135px;
        cursor:pointer;
      }
      .opciones2{
        height: 135px;
        padding-left:40px;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor:pointer;
      }
      .hidden {
    display: none;
}
/* The container */
.contenedor {
  display: block;
  position: relative;
 
  cursor: pointer;

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.contenedor input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}



      #layouts{
        background-color: #3582d5;
      
      
      }
      .subes{
        padding-bottom: 56px;
        padding-top: 59px;
        text-align: center;
        color: #123f6f;
        font-size: 44pt;
        font-family: 'gotham-bold';
      }

      .card{
        width: 100%;
        padding-left: 20px;
        padding-right: 20px;
     
       
      }
     
   

#marcos{
      margin: 10px;
      margin-bottom: 40px;
      
      }  

      #submit{
        background-color: #123f6f;
        color: white;
        width: 210px;
        height: 50px;
        border-radius:8px;
        font-family: 'gotham-bold';
        font-size: 10pt;
      }

      label, .lazul{
        color: #123f6f;
        font-size: 13pt;
        font-family: 'din-pro-light';
      }
        input[type]:focus{
          border-color: #123f6f;
          color:#123f6f;
          font-size: 18pt;
        font-family: 'din-pro-light';
        } 

    .precio {
      color: #123f6f;
      font-size: 27px;
      font-family: 'din-pro-light';
    }    

    .subtitulos{
      margin-top:30px;
      color: #123f6f;
      text-align: center;
      font-family: 'din-pro-medium';
      font-size: 15pt;
    }  

    .subtitulos2{
      margin-top:30px;
      color: #123f6f;
      text-align: center;
      font-family: 'din-pro-regular';
      font-size: 15pt;
    }  

    .parrafos{
      color: #123f6f;
      margin-left: 30px;
      font-size: 8pt;
    } 

    label.cabinet{
  display: block;
  cursor: pointer;
}

label.cabinet input.file{
  display: none;
}

#upload-demo{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoA{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoB{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoC{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoD{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoE{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoF{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoG{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoH{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoI{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoJ{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoK{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoL{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoM{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoN{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoO{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoP{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoQ{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoR{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoS{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoT{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoU{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
#upload-demoV{
  width: 250px;
  height: 250px;
  padding-bottom:25px;
}
figure figcaption {
   
    bottom: 0;
    color: #fff;
    width: 100%;
    padding-left: 9px;
    padding-bottom: 5px;
    text-shadow: 0 0 10px #000;
}

#medidas_m{
  color: #123f6f;
  font-family: 'din-pro-regular';
  font-size: 15pt;
}
#nombre_m{
  color: #123f6f;
  font-family: 'din-pro-regular';
  font-size: 15pt;
}

#item-img-output-t1{
  margin-top: 25px;

  border: solid 25px #252727;
  height: auto;
  width: 300;
}

#item-img-output{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-layout2-1{
  margin-top: 25px;
  
}
#item-img-output-l2-2{
  margin-top: 25px;
  
}
#item-img-output-l2-3{
  
  
}
#item-img-output-l2-4{
  
  
}
#item-img-output-layout3-1{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l3-2{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l3-3{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l3-4{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l3-5{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l3-6{
  margin-top: 25px;
  padding: 10px;
}

/*Layout4 */
#item-img-output-layout4-1{
 
 margin-top: 20%;
 height: 200px;
 width: auto;


}
#item-img-output-l4-2{
 margin-top: 20%;
 height: 100;
 width: auto;

}
#item-img-output-l4-3{
 margin-bottom: 20%;
 height: 100px;
 width: auto;
}
#item-img-output-l4-4{

 margin-top: 20%;
 height: 200px;
width: auto;
}
/*layout 5 */
#item-img-output-layout5-1{
  margin-top: 20%;
  height: 250px;
  width: auto;
}
#item-img-output-l5-2{
  margin-top: 20%;
  height: 250px;
  width: auto;
}
#item-img-output-l5-3{
  margin-top: 20%;
  height: 250px;
  width: auto;
}

#item-img-output-layout6-1{
  margin-top: 25px;
  padding: 10px;
}
#item-img-output-l6-2{
  margin-top: 25px;
 
}

.descripcion{
    margin-top: 30%;
    margin-bottom: 60%;
    color: #123f6f;
    font-family: 'din-pro-regular';

  }

  .img-thumbnail1 {
    border: 0px ;
  
    width: 100%;
   
}

.letramarco{
  color: #123f6f;
  font-family: 'din-pro-regular';
  font-size: 18pt; 
}
.descri{
  color: #123f6f;
  font-family: 'din-pro-regular';
  font-size: 14pt; 
}
input[type="radio"] {
  cursor:pointer;
}
.office{
 padding-left: 5%; 
}

.office img{
  width: 50%;

}


.table td, .table th {
      
      padding: 3px;
      vertical-align: top;
      border-top: 1px  transparent;
  
  }

 
  .office figure{
    width: 180%
    
  }
  .home{
  padding-left: 5%; 
}
.home img{
  width: 50%;

}
.home figure{
    width: 180%
    
  }
  .home table{
  margin-left:15px;
}

@media only screen and (max-width: 800px) {

  .iconospago{
    margin-left: 20px;
    margin-bottom:10px;
    height: 20px;
}

  .opciones1{
        display: flex;
        justify-content: center;
        align-items: center;
        height: 30%;
       
       
      }
  .opciones2{
    margin-top: 10px;
    height: auto;
    width: 150%;
    cursor:pointer;
      }
  .hidden {
    display: none;
}    
    
      .opciones{
        height: 30px;
      }
      .opcionesa{
        height: 25px;
        cursor:pointer;
      }
      .opciones1 img{
       width: 80%;
       cursor:pointer;
      }
      
      .subes{
        padding-bottom: 1px;
        padding-top: 20px;
        text-align: center;
        color: #123f6f;
       
        font-family: 'gotham-bold';
      }
      .subes h2{
        font-size: 40%;
      }

      .parrafos{
      color: #123f6f;
      margin-left: 30px;
      font-size: 8pt;
    } 
    #item-img-output-layout4-1{
  
  margin-top: 20%;
  height: 140px;
 width: auto;

}
#item-img-output-l4-2{

margin-top: 20%;
height: 65px;
width: auto;


}
#item-img-output-l4-3{

margin-top: 20%;
height: 65px;
width: auto;


}


#item-img-output-l4-4{

  margin-top: 20%;
  height: 140px;
 width: auto;


}


  /*layout 5 */
#item-img-output-layout5-1{
  margin-top: 20%;
  height: 140px;
   width: auto;
  
 
}
#item-img-output-l5-2{
  margin-top: 20%;
  height: 140px;
   width: auto;
}
#item-img-output-l5-3{
  margin-top: 20%;
  height: 140px;
   width: auto;
}


}

    </style>

   
   



@section('content')
      
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
  </ul>
</div><br />
@endif



<main>

  <section class="">

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 1.png'}}" alt="First slide">
                
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 2.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 4.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 5.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 6.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 7.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 8.png'}}" alt="First slide">
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</section>


  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container-fluid">

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-6 opciones1">
        <img src="front/imglayout.png" alt="" class="img-fluid" id="mostrar1">
      </div><!-- /.col-lg-4 -->
      
      <div class="col-6 opciones1">
        <img src="front/marcos.png" alt="" class="img-fluid" id="mostrar2">
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->

   
  {{--Empieza el layout --}}
  <div class="target1">
    <div class="row subes">
      <div class="col-md-12">
        <h2 class="featurette-heading">SELECCIONA TU LAYOUT</h2>
        
      </div>
    </div>
    <div class="row " id="layouts">
    
      <div class="col text-center" >
        <img src="{{ asset('front/layout 1.png') }}" alt="" class="opciones btn-tuwall" id="layout1" >
        <img src="{{ asset('front/select/layout 1a.png') }}" alt="" class="opciones btn-tuwall" id="layout1a" width="auto" height="135px" >
  
    </div>
    <!-- /.col-md-4 -->
    <div class=" col text-center">
        <img src="{{ asset('front/layout 2.png') }}" alt="" class="opciones" id="layout2"  >
        <img src="{{ asset('front/select/layout 2a.png') }}" alt="" class="opciones" id="layout2a" width="auto" height="135px"  >
    </div>
    <!-- /.col-md-4 -->
    <div class="col text-center">
        <img src="{{ asset('front/layout 3.png') }}" alt="" class="opciones" id="layout3" >
        <img src="{{ asset('front/select/layout 3a.png') }}" alt="" class="opciones" id="layout3a" >
     
    </div>
    <!-- /.col-md-4 -->
    <div class="col text-center">
        <img src="{{ asset('front/layout 4.png') }}" alt="" class="opciones" id="layout4" >
        <img src="{{ asset('front/select/layout 4a.png') }}" alt="" class="opciones" id="layout4a" >
     
    </div>
    <!-- /.col-md-4 -->
    <div class="col text-center">
        <img src="{{ asset('front/bandera52.png') }}" alt="" class="opciones" id="layout5">
        <img src="{{ asset('front/select/layout 5a.png') }}" alt="" class="opciones" id="layout5a">
     
    </div>
    <!-- /.col-md-4 -->
    <div class="col text-center">
        <img src="{{ asset('front/layout 6.png') }}" alt="" class="opciones" id="layout6">
        <img src="{{ asset('front/select/layout 6a.png') }}" alt="" class="opciones" id="layout6a">
     
    </div>
    <!-- /.col-lg-4 -->
    </div>

  </div>
  {{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 1--++++++++++++++++++++++++++++++++++--}}
  <div class="mostrarlay1">
    <div class="row subes">
      <div class="col-md-12 order-md-2">
        <h2 class="featurette-heading ">SUBE TUS FOTOS</h2>
      </div>
     
    </div>
    <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
              <div class="card margen " style="background: #E3E3E2">
                      <div class="row justify-content-center">
                
                      <div class="col-6 ">
                      <label class="cabinet center-block">
                        <figure>
                          <img src=""  class="gambar img-responsive img-thumbnail1" id="item-img-output" />
                    
                        </figure>
                        <input type="file" required class="item-img file center-block" id="item-img" name="file_photo"/>
                        <input type="hidden" name="layout1_1" id="layout1-1">
                      </label>
                     
                    </div>
                 
                  </div>
                  
              </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="subtitulos">IMPORTANTE:</h5><br>
                      <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                      <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                    </div>
                  
                  </div>
              
       </div>
      
       <div class="col-md-3">
       
       <div class="form-group">
        <p class="letramarco">LAYOUT OVERWALL ROOM </p>
        <p class="descri"> 1 panel de 70 x 70 cm.</p>
  
       </div>
       
         <div class="form-group">
          <label for="exampleFormControlSelect1">Impreso sobre</label>
                <select  name="materiales"  id="txt_campo_1"  class="form-control btn-lay btn-mat1" required>
                    <option value="">Seleccione</option>
                    <option value="Trovicel">Trovicel</option>
                    <option value="Acrílico">Acrílico</option>
                    <option value="Foamboard">Foamboard</option>
                 </select>  
        </div>   
         
     
    
        <div class="form-group ">
          <label for="">Cantidad</label>
          <input type="number" min="1" class="form-control btn-lay"  name="cantidad" value="1">
          <input type="hidden" name="layout" value="Room" id="layout_l1">
        </div>
          
         
        
       
     
        <p class="precio"> MX $<span id="f314-div">0.00</span></p>
  
   <br> @if (Auth::check())
   
   <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 
  
       @else
    <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
           Inicia sesión
           </button>
  
   @endif
          
        
       
        
        </form>
     
      
      </div>
      <div class="col-md-3">
        <br>
        <div class="descripcion">
            <p id="descripcion1"></p>
        </div>
       
      </div>
    </div>
  </div>
  
    
  
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 2--++++++++++++++++++++++++++++++++++--}}
<div class="mostrarlay2">
  <div class="row subes">
    <div class="col-md-12 order-md-2">
      <h2 class="featurette-heading ">SUBE TUS FOTOS</h2>
    </div>
   
  </div>
  <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row contenedor">
      <div class="col-md-1"></div>
      <div class="col-md-5">
                  <div class="card margen " style="background: #E3E3E2">
                      <div class="row office">
                        <table>
                          <tr>
                            <td>
                              <label class="cabinet center-block">
                                <figure>
                                  <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-layout2-1" />
                            
                                </figure>
                                <input type="file" required class="item-img-l2-1 file form-control" name="url1"/>
                                <input type="hidden" name="layout2_1" id="layout2-1">
                              </label>
                            </td>
                            <td>
                              <label class="cabinet center-block">
                                <figure>
                                  <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l2-2" />
                            
                                </figure>
                                <input type="file" required class="item-img-l2-2 file form-control" name="url2"/>
                                <input type="hidden" name="layout2_2" id="layout2-2">
                                
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label class="cabinet center-block">
                                <figure>
                                  <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l2-3" />
                            
                                </figure>
                                <input type="file" required class="item-img-l2-3 file center-block" name="url3"/>
                                <input type="hidden" name="layout2_3" id="layout2-3">
                              </label>
                            </td>
                            <td>
                              <label class="cabinet center-block">
                                <figure>
                                  <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l2-4" />
                            
                                </figure>
                                <input type="file" required class="item-img-l2-4 file center-block" name="url4"/>
                                <input type="hidden" name="layout2_4" id="layout2-4">
                              </label>
                            </td>
                          </tr>
                        </table>
                      </div>
                   
               
       
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <h5 class="subtitulos">IMPORTANTE:</h5><br>
                    <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                    <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                  </div>
                
                </div>
      </div>
     <div class="col-md-3">
     
     <div class="form-group">
      <p class="letramarco">LAYOUT OVERWALL OFFICE </p>
      <p class="descri"> 4 paneles de 34 x 34 cm.</p>

     </div>
     
       <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre</label>
        <select  name="material"  id="material_l2"  class="form-control btn-lay2 btn-mat2" required>
          <option value="" selected>Seleccione</option>
          <option value="Trovicel">Trovicel</option>
          <option value="Acrílico">Acrílico</option>
          <option value="Foamboard">Foamboard</option>
      </select> 
      </div>   
       
   
  
      <div class="form-group ">
        <label for="">Cantidad</label>
        <input type="number" min="1" class="form-control btn-lay2" id="cantidad_l2" name="cantidad" value="1">
        <input type="hidden" name="layout" value="Office" id="layout_l2">
      </div>
        
       
    
     
   
 <p class="precio"> MX $<span id="l2-div"></span></p>

 <br> @if (Auth::check())
 
 <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 

     @else
  <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
         Inicia sesión
         </button>

 @endif
        
      
     
      
      </form>
   
    
    </div>
    <div class="col-md-3">
      <br>
      <div class="descripcion">
        <p id="descripcion2"></p>
    </div>
     
    </div>
  </div>
 
  </div>

{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 3--++++++++++++++++++++++++++++++++++--}}
<div class="mostrarlay3">
  <div class="row subes">
    <div class="col-md-12 order-md-2">
      <h2 class="featurette-heading ">SUBE TUS FOTOS</h2>
    </div>
   
  </div>
  <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row contenedor">
      <div class="col-md-6">
                  <div class="card margen "  style="background: #E3E3E2;">

                    <div class="row">
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-layout3-1" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-1 file center-block" name="url1"/>
                          <input type="hidden" name="layout3_1" id="layout3-1">
                        </label>
                      </div>
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l3-2" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-2 file center-block" name="url2"/>
                          <input type="hidden" name="layout3_2" id="layout3-2">
                          
                        </label>
                       
                      </div>
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l3-3" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-3 file center-block" name="url3"/>
                          <input type="hidden" name="layout3_3" id="layout3-3">
                          
                        </label>
                       
                      </div>
                    </div>
                    <div class="row">
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l3-4" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-4 file center-block" name="url4"/>
                          <input type="hidden" name="layout3_4" id="layout3-4">
                        </label>
                      </div>
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l3-5" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-5 file center-block" name="url5"/>
                          <input type="hidden" name="layout3_5" id="layout3-5">
                          
                        </label>
                       
                      </div>
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l3-6" />
                      
                          </figure>
                          <input type="file" required class="item-img-l3-6 file center-block" name="url6"/>
                          <input type="hidden" name="layout3_6" id="layout3-6">
                          
                        </label>
                       
                      </div>
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="subtitulos">IMPORTANTE:</h5><br>
                      <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                      <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                    </div>
                  
                  </div>
       
     </div>
     <div class="col-md-3">
     
     <div class="form-group">
      <p class="letramarco">LAYOUT OVERWALL FUN </p>
      <p class="descri"> 6 paneles de 25 x 25 cm.</p>

     </div>
     
       <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre</label>
            <select  name="material"  id="material_l3"  class="form-control btn-lay3 btn-mat3" required>
              <option value="" selected>Seleccione</option>
              <option value="Trovicel">Trovicel</option>
              <option value="Acrílico">Acrílico</option>
              <option value="Foamboard">Foamboard</option>
          </select>  
      </div>   
       
   
  
      <div class="form-group ">
        <label for="">Cantidad</label>
        <input type="number" min="1" class="form-control btn-lay3" id="cantidad_l3" name="cantidad" value="1">
        <input type="hidden" name="layout" value="Fun" class="btn-lay3" id="layout_l3">
      </div>
  
  
   <p class="precio"> MX $<span id="l3-div"></span></p>

 <br> @if (Auth::check())
 
 <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 

     @else
  <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
         Inicia sesión
         </button>

 @endif
        
      </form>
   
    
    </div>
    <div class="col-md-3">
      <br>
      <div class="descripcion">
        <p id="descripcion3"></p>
    </div>
     
    </div>
  </div>
</div>
  
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 4--++++++++++++++++++++++++++++++++++--}}
<div class="mostrarlay4">
  <div class="row subes">
    <div class="col-md-12 order-md-2">
      <h2 class="featurette-heading">SUBE TUS FOTOS</h2>
    </div>
   
  </div>
  <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row contenedor">
      <div class="col-md-6">
                  <div class="card margen " style="background: #E3E3E2">

                    <div class="row">
             
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar2 img-responsive img-thumbnail1 image-fluid" id="item-img-output-layout4-1" />
                      
                          </figure>
                          <input type="file" required class="item-img-l4-1 file center-block" name="url1"/>
                          <input type="hidden" name="layout4_1" id="layout4-1">
                        </label>
                      </div>
                      <div class=" col-4">
                       
                          <div class="col-12">
                             <label class="cabinet center-block">
                            <figure>
                              <img src="" class="gambar3 img-responsive img-thumbnail1" id="item-img-output-l4-2" />
                        
                            </figure>
                            <input type="file" required class="item-img-l4-2 file center-block" name="url2"/>
                            <input type="hidden" name="layout4_2" id="layout4-2">
                            
                          </label>
                          </div>
                          <div class="col-12">
                            <label class="cabinet center-block">
                           <figure>
                             <img src="" class="gambar3 img-responsive img-thumbnail1" id="item-img-output-l4-3" />
                       
                           </figure>
                           <input type="file" required class="item-img-l4-3 file center-block" name="url3"/>
                           <input type="hidden" name="layout4_3" id="layout4-3">
                           
                         </label>
                         </div>
                         
                        
                      </div>
                    
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar2 img-responsive img-thumbnail1" id="item-img-output-l4-4" />
                      
                          </figure>
                          <input type="file" required class="item-img-l4-4 file center-block" name="url4"/>
                          <input type="hidden" name="layout4_4" id="layout4-4">
                        </label>
                      </div>
                     
                   
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="subtitulos">IMPORTANTE:</h5><br>
                      <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                      <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                    </div>
                  
                  </div>
       
     </div>
     <div class="col-md-3">
     
     <div class="form-group">
      <p class="letramarco">LAYOUT OVERWALL HOME </p>
      <p class="descri"> 2 paneles de 40 x 70 cm. y 2 paneles de 34 x 34 cm.</p>

     </div>
     
       <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre</label>
        <select  name="material"  id="material_l4"  class="form-control btn-lay4 btn-mat4" required>
          <option value="" selected>Seleccione</option>
          <option value="Trovicel">Trovicel</option>
          <option value="Acrílico">Acrílico</option>
          <option value="Foamboard">Foamboard</option>
      </select>   
      </div>   
       
   
  
      <div class="form-group ">
        <label for="">Cantidad</label>
        <input type="number" min="1" class="form-control btn-lay4" name="cantidad" value="1" id="cantidad_l4">
        <input type="hidden" name="layout" value="Home" class="btn-lay4" id="layout_l4">
    </div>
  
  
   <p class="precio"> MX $<span id="l4-div">0</span></p>
  

 <br> @if (Auth::check())
 
 <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 

     @else
  <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
         Inicia sesión
         </button>

 @endif
        
      </form>
   
    
    </div>
    <div class="col-md-3">
      <br>
      <div class="descripcion">
        <p id="descripcion4"></p>
    </div>
     
    </div>
  </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 5--++++++++++++++++++++++++++++++++++--}}
<div class="mostrarlay5">
  <div class="row subes">
    <div class="col-md-12 order-md-2">
      <h2 class="featurette-heading">SUBE TUS FOTOS</h2>
    </div>
   
  </div>
  <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row contenedor">
      <div class="col-md-6">
                  <div class="card margen " style="background: #E3E3E2">

                    <div class="row justify-content-beetween">
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar2 img-responsive img-thumbnail1" id="item-img-output-layout5-1" />
                      
                          </figure>
                          <input type="file" required class="item-img-l5-1 file center-block" name="url1"/>
                          <input type="hidden" name="layout5_1" id="layout5-1">
                        </label>
                      </div>
                      
                      <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar2 img-responsive img-thumbnail1" id="item-img-output-l5-2" />
                      
                          </figure>
                          <input type="file" required class="item-img-l5-2 file center-block" name="url2"/>
                          <input type="hidden" name="layout5_2" id="layout5-2">
                          
                        </label>
                      </div>
                        <div class=" col-4">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar2 img-responsive img-thumbnail1" id="item-img-output-l5-3" />
                      
                          </figure>
                          <input type="file" required class="item-img-l5-3 file center-block" name="url3"/>
                          <input type="hidden" name="layout5_3" id="layout5-3">
                          
                        </label>
                       
                      </div>
                   
                     
                   
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="subtitulos">IMPORTANTE:</h5><br>
                      <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                      <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                    </div>
                  
                  </div>
       
     </div>
     <div class="col-md-3">
     
     <div class="form-group">
      <p class="letramarco">LAYOUT OVERWALL FLAG </p>
      <p class="descri"> 3 paneles de 70 x 40 cm.</p>

     </div>
     
       <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre</label>
            <select  name="material"  id="material_l5"  class="form-control btn-lay5 btn-mat5" required>
              <option value="" selected>Seleccione</option>
              <option value="Trovicel">Trovicel</option>
              <option value="Acrílico">Acrílico</option>
              <option value="Foamboard">Foamboard</option>
       </select>    
      </div>   
       
   
  
      <div class="form-group ">
        <label for="">Cantidad</label>
        <input type="number" min="1" class="form-control btn-lay5" name="cantidad" id="cantidad_l5" value="1">
        <input type="hidden" name="layout" value="Flag" class="btn-lay5" id="layout_l5">
    </div>
  
  
   <p class="precio"> MX $<span id="l5-div">0</span></p>
  

 <br> @if (Auth::check())
 
 <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 

     @else
  <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
         Inicia sesión
         </button>

 @endif
        
      </form>
   
    
    </div>
    <div class="col-md-3">
      <br>
      <div class="descripcion">
        <p id="descripcion5"></p>
    </div>
     
    </div>
  </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 6--++++++++++++++++++++++++++++++++++--}}
<div class="mostrarlay6">
  <div class="row subes">
    <div class="col-md-12 order-md-2">
      <h2 class="featurette-heading">SUBE TUS FOTOS</h2>
    </div>
   
  </div>
  <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row contenedor">
      <div class="col-md-6">
                  <div class="card margen " style="background: #E3E3E2">

                    <div class="row">
             
                      <div class=" col-8">
                        <label class="cabinet center-block">
                          <figure>
                            <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-layout6-1" />
                      
                          </figure>
                          <input type="file" required class="item-img-l6-1 file center-block" name="url1"/>
                          <input type="hidden" name="layout6_1" id="layout6-1">
                        </label>
                      </div>
                  
                    
                 
                      <div class=" col-4">
                        <div class="row">
                          <div class="col-12">
                             <label class="cabinet center-block">
                            <figure>
                              <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l6-2" />
                        
                            </figure>
                            <input type="file" required class="item-img-l6-2 file center-block" name="url2"/>
                            <input type="hidden" name="layout6_2" id="layout6-2">
                            
                          </label>
                          </div>
                          <div class="col-12">
                            <label class="cabinet center-block">
                           <figure>
                             <img src="" class="gambar img-responsive img-thumbnail1" id="item-img-output-l6-3" />
                       
                           </figure>
                           <input type="file" required class="item-img-l6-3 file center-block" name="url3"/>
                           <input type="hidden" name="layout6_3" id="layout6-3">
                           
                         </label>
                         </div>
                         
                        </div>
                    
                   
                      </div>
                    
                 
                     
                   
                    </div>
                   
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5 class="subtitulos">IMPORTANTE:</h5><br>
                      <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
                      <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
                    </div>
                  
                  </div>
       
     </div>
     <div class="col-md-3">
     
     <div class="form-group">
      <p class="letramarco">LAYOUT OVERWALL SPACE </p>
      <p class="descri"> 1 panel de 70 x 60 cm y 2 de 34 x 34 cm.</p>

     </div>
     
       <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre</label>
        <select  name="material"  id="material_l6"  class="form-control btn-lay6 btn-mat6" required>
       <option value="" selected>Seleccione</option>
          <option value="Trovicel">Trovicel</option>
          <option value="Acrílico">Acrílico</option>
          <option value="Foamboard">Foamboard</option>
   </select>  
      </div>   
       
   
  
      <div class="form-group ">
        <label for="">Cantidad</label>
      <input type="number" min="1" class="form-control btn-lay6" name="cantidad" value="1" id="cantidad_l6">
      <input type="hidden" name="layout" value="Space" class="btn-lay6" id="layout_l6">
  </div>


 <p class="precio"> MX $<span id="l6-div">0</span></p>

 <br> @if (Auth::check())
 
 <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 

     @else
  <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
         Inicia sesión
         </button>

 @endif
        
      </form>
   
    
    </div>
    <div class="col-md-3">
      <br>
      <div class="descripcion">
        <p id="descripcion6"></p>
    </div>
     
    </div>
  </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++TU WALLLLLLLLLLL--++++++++++++++++++++++++++++++++++--}}
 {{--Empieza el tu wall --}}
 <div class="target2">
  <div class="row featurette subes">
    <div class="col-md-12">
      <h2 class="featurette-heading">PIC WALL</h2>
      
    </div>
  </div>
  <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
    @csrf
  <div class="row " id="layouts">
    <div class="col  text-center">
      <label class="container">
        <img src="{{ asset('front/picwall1.png') }}" alt="" class="opciones2" id="tuwall1">
        <img src="{{ asset('front/select/picwall1a.png') }}" alt="" class="opciones2" id="tuwall1a">
        <input type="radio" id="medida1" name="medida" value="15 X 20" class="btn-tuwall hidden ">
       
      </label>
    </div>   
    <div class="col  text-center">
      <label class="container">
        <img src="{{ asset('front/picwall2.png') }}" alt="" class="opciones2" id="tuwall2">
        <img src="{{ asset('front/select/picwall2a.png') }}" alt="" class="opciones2" id="tuwall2a">
        <input type="radio" id="medida2" name="medida" value="20 X 25" class="btn-tuwall hidden ">
       
      </label>
    </div>   
    <div class="col  text-center">
      <label class="container">
        <img src="{{ asset('front/picwall3.png') }}" alt="" class="opciones2" id="tuwall3">
        <img src="{{ asset('front/select/picwall3.png') }}" alt="" class="opciones2" id="tuwall3a">
        <input type="radio" id="medida3" name="medida" value="28 X 35" class="btn-tuwall hidden ">
       
      </label>
    </div>   
   

 
  <!-- /.col-lg-4 -->
  </div>

  <div class="row">

  </div>


    <div class="row subes">
      <div class="col-md-12 order-md-2">
        <h2 class="featurette-heading">SUBE TUS FOTOS</h2>
      </div>
     
    </div>

    <div class="row" id="marcos">
      <div class="col-md-12">
        <img src="{{ asset('front/coloresmarcos.png') }}" alt=""  class=" img-fluid">
      </div>
    </div>
  
    <div class="row ">
      <div class="col-md-1"></div>
      <div class="col-md-6 order-md-1">
        <div class="row">
          <div class="card text-center" style="background: #E3E3E2" >
         
            <label class="cabinet center-block">
              <figure>
                <img src="" class="gambar2 img-responsive " id="item-img-output-t1" />
          
              </figure>
              <input type="file" required class="item-imgt1 file center-block" name="file_photo"/>
              <input type="hidden" name="twall" id="twall">
            </label>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h5 class="subtitulos">IMPORTANTE:</h5><br>
            <p class="parrafos">Siempre toma en cuenta que la resolución que tiene tu fotografia, esta relacionada directamente con la calidad de tu camara, sea de telefono celular o cámara digital, es decir, una camara que toma imágenes de 10 megapixeles, supera a una de 5 megapixeles, y una de 20 megapixeles, supera a una de 10 megapixeles, este es el elemento que define si tu imagen es apta para imprimirla o no. Si la imagen que es tomada por tu camara es de 4000 x 5000 pixeles, es perfecta para imprimirse.</p>
            <img src="{{asset('front/iconos_pago.jpeg')}}" alt="" class="iconospago">
          </div>
        
        </div>
      </div>
      
      <div class="col-md-5 order-md-2">
        <p class="letramarco"><span id="nombre_m"></span></p>
        <p><span id="medidas_m"></span></p>

        <div class="form-group col-md-5">
          <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
            <select name="acabado" id="txt_campo_1"  class="form-control lazul">
            <option value="0">Seleccione</option>

            <option value="Mate" selected>Mate</option>
            <option value="Brillante" selected>Brillante</option>

            </select>
       </div>

       <div class="form-group col-md-5">
        <label for="exampleFormControlSelect1">Color Marco</label>
          <select name="color" id="txt_campo_2" class="form-control lazul">
          <option value="0">Seleccione</option>
          <option value="Negro" selected>Negro</option>
          <option value="Chocolate">Chocolate</option>
          <option value="Caoba">Caoba</option>
          <option value="Nogal">Nogal</option>

          </select>
      </div>
      <div class="form-group col-md-5">
        <label for="">Cantidad</label>
        <input type="hidden" name="control" value="tuwall">
        <input type="number" min="1" class="form-control btn-tuwall" name="tcantidad" value="1">
    </div>
 

   <p class="precio"> MX $<span id="fw-div">0</span><p>

    <br> @if (Auth::check())

    <button class="form-control " type="submit" id="submit" >AGREGAR AL CARRITO</button> 
    
        @else
           <!-- Button trigger modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
            Inicia sesión
            </button>
    @endif

    </div>
  </form>

</div>

  </div><!-- /.container -->
  
  <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demo" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotateright">Derecha</button>
<button class="btn btn-default" id="rotateleft">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtn" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="cropImagePopA" tabindex="-1" role="dialog" aria-labelledby="myModalLabelA" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoA" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightA">Derecha</button>
<button class="btn btn-default" id="rotateleftA">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnA" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopB" tabindex="-1" role="dialog" aria-labelledby="myModalLabelB" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoB" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightB">Derecha</button>
<button class="btn btn-default" id="rotateleftB">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnB" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopC" tabindex="-1" role="dialog" aria-labelledby="myModalLabelC" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoC" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightC">Derecha</button>
<button class="btn btn-default" id="rotateleftC">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnC" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopD" tabindex="-1" role="dialog" aria-labelledby="myModalLabelD" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoD" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightD">Derecha</button>
<button class="btn btn-default" id="rotateleftD">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnD" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopF" tabindex="-1" role="dialog" aria-labelledby="myModalLabelF" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoF" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightF">Derecha</button>
<button class="btn btn-default" id="rotateleftF">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnF" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopG" tabindex="-1" role="dialog" aria-labelledby="myModalLabelG" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoG" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightG">Derecha</button>
<button class="btn btn-default" id="rotateleftG">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnG" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopH" tabindex="-1" role="dialog" aria-labelledby="myModalLabelH" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoH" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightH">Derecha</button>
<button class="btn btn-default" id="rotateleftH">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnH" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopI" tabindex="-1" role="dialog" aria-labelledby="myModalLabelI" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoI" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightI">Derecha</button>
<button class="btn btn-default" id="rotateleftI">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnI" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopJ" tabindex="-1" role="dialog" aria-labelledby="myModalLabelJ" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoJ" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightJ">Derecha</button>
<button class="btn btn-default" id="rotateleftJ">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnJ" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopK" tabindex="-1" role="dialog" aria-labelledby="myModalLabelK" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoK" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightK">Derecha</button>
<button class="btn btn-default" id="rotateleftK">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnK" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopL" tabindex="-1" role="dialog" aria-labelledby="myModalLabelL" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoL" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightL">Derecha</button>
<button class="btn btn-default" id="rotateleftL">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnL" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopM" tabindex="-1" role="dialog" aria-labelledby="myModalLabelM" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoM" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightM">Derecha</button>
<button class="btn btn-default" id="rotateleftM">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnM" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopN" tabindex="-1" role="dialog" aria-labelledby="myModalLabelN" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoN" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightN">Derecha</button>
<button class="btn btn-default" id="rotateleftN">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnN" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopO" tabindex="-1" role="dialog" aria-labelledby="myModalLabelO" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoO" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightO">Derecha</button>
<button class="btn btn-default" id="rotateleftO">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnO" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopP" tabindex="-1" role="dialog" aria-labelledby="myModalLabelP" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoP" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightP">Derecha</button>
<button class="btn btn-default" id="rotateleftP">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnP" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabelQ" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoQ" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightQ">Derecha</button>
<button class="btn btn-default" id="rotateleftQ">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnQ" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopR" tabindex="-1" role="dialog" aria-labelledby="myModalLabelR" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoR" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightR">Derecha</button>
<button class="btn btn-default" id="rotateleftR">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnR" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopS" tabindex="-1" role="dialog" aria-labelledby="myModalLabelS" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoS" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightS">Derecha</button>
<button class="btn btn-default" id="rotateleftS">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnS" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopT" tabindex="-1" role="dialog" aria-labelledby="myModalLabelT" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoT" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightT">Derecha</button>
<button class="btn btn-default" id="rotateleftT">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnT" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopU" tabindex="-1" role="dialog" aria-labelledby="myModalLabelU" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
          <div id="upload-demoU" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightU">Derecha</button>
<button class="btn btn-default" id="rotateleftU">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnU" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="cropImagePopV" tabindex="-1" role="dialog" aria-labelledby="myModalLabelV" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">
      </h4>
    </div>
    <div class="modal-body">
         
          <div id="upload-demoV" class="center-block"></div>
    </div>
     <div class="modal-footer">
<button class="btn btn-default" id="rotaterightV">Derecha</button>
<button class="btn btn-default" id="rotateleftV">Izquierda</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<button type="button" id="cropImageBtnV" class="btn btn-primary">Guardar</button>
</div>
      </div>
    </div>
  </div>


@endsection
    
@section('js')

<script>
  $(document).ready(function(){
    $('.target1').hide();
    $('.target2').hide();
    $('.mostrarlay1').hide();
    $('.mostrarlay2').hide();
    $('.mostrarlay3').hide();
    $('.mostrarlay4').hide();
    $('.mostrarlay5').hide();
    $('.mostrarlay6').hide();
  
    $("#mostrar1").click(function(){
      $('.target1').show();
      $('.target2').hide();
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();
      $('#layout1a').hide();
      $('#layout2a').hide();
      $('#layout3a').hide();
      $('#layout4a').hide();
      $('#layout5a').hide();
      $('#layout6a').hide();
      
      $('#layout1').show().focus();
      $('#layout2').show();
      $('#layout3').show();
      $('#layout4').show();
      $('#layout5').show();
      $('#layout6').show();
      
     });
    $("#mostrar2").click(function(){
      $('.target2').show();
      $('.target1').hide();
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();

      $('#tuwall1a').hide();
      $('#tuwall2a').hide();
      $('#tuwall3a').hide();
      $('#tuwall1').show();
      $('#tuwall2').show();
      $('#tuwall3').show();

    
    });
  
    $("#layout1").click(function(){
      $('.mostrarlay1').show(500);
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();
      $('#medida1').blur();

      $('#layout1a').show();
      $('#layout2').show();
      $('#layout3').show();
      $('#layout4').show();
      $('#layout5').show();
      $('#layout6').show();
      $('#layout1').hide();
      $('#layout2a').hide();
      $('#layout3a').hide();
      $('#layout4a').hide();
      $('#layout5a').hide();
      $('#layout6a').hide();



      $('#focus').focus();
   
     });
  
    $("#layout2").click(function(){
      $('.mostrarlay2').show(500);
      $('.mostrarlay1').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();

      $('#layout1').show();
      $('#layout2a').show();
      $('#layout3').show();
      $('#layout4').show();
      $('#layout5').show();
      $('#layout6').show();

      $('#layout1a').hide();
      $('#layout2').hide();
      $('#layout3a').hide();
      $('#layout4a').hide();
      $('#layout5a').hide();
      $('#layout6a').hide();
     });
     $("#layout3").click(function(){
      $('.mostrarlay3').show(500);
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();

      $('#layout3a').show();
      $('#layout1').show();
      $('#layout2').show();
      $('#layout4').show();
      $('#layout5').show();
      $('#layout6').show();
      $('#layout1a').hide();
      $('#layout2a').hide();
      $('#layout3').hide();
      $('#layout4a').hide();
      $('#layout5a').hide();
      $('#layout6a').hide();
      
     });
     $("#layout4").click(function(){
      $('.mostrarlay4').show(500);
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay6').hide();

      $('#layout4a').show();
      $('#layout1').show();
      $('#layout2').show();
      $('#layout3').show();
      $('#layout5').show();
      $('#layout6').show();
      $('#layout2a').hide();
      $('#layout3a').hide();
      $('#layout4').hide();
      $('#layout1a').hide();
      $('#layout5a').hide();
      $('#layout6a').hide();      
      
     });
     $("#layout5").click(function(){
      $('.mostrarlay5').show(500);
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay4').hide();
      $('.mostrarlay6').hide();

      $('#layout5a').show();
      $('#layout1').show();
      $('#layout2').show();
      $('#layout3').show();
      $('#layout4').show();
      $('#layout6').show();
      $('#layout2a').hide();
      $('#layout3a').hide();
      $('#layout4a').hide();
      $('#layout1a').hide();
      $('#layout5').hide();
      $('#layout6a').hide();  
      
     });
     $("#layout6").click(function(){
      $('.mostrarlay6').show(500);
      $('.mostrarlay1').hide();
      $('.mostrarlay2').hide();
      $('.mostrarlay3').hide();
      $('.mostrarlay5').hide();
      $('.mostrarlay4').hide();

      $('#layout6a').show();
      $('#layout1').show();
      $('#layout2').show();
      $('#layout3').show();
      $('#layout4').show();
      $('#layout5').show();
      $('#layout2a').hide();
      $('#layout3a').hide();
      $('#layout4a').hide();
      $('#layout1a').hide();
      $('#layout5a').hide();
      $('#layout6').hide(); 
      
     });


     $("#tuwall1").click(function(){

      $('#tuwall1a').show();
      $('#tuwall2').show();
      $('#tuwall3').show();

      $('#tuwall1').hide();
      $('#tuwall2a').hide();
      $('#tuwall3a').hide();

     });
     $("#tuwall2").click(function(){

      $('#tuwall2a').show();
      $('#tuwall1').show();
      $('#tuwall3').show();

      $('#tuwall1a').hide();
      $('#tuwall2').hide();
      $('#tuwall3a').hide();

     });
     $("#tuwall3").click(function(){

      $('#tuwall3a').show();
      $('#tuwall1').show();
      $('#tuwall2').show();

      $('#tuwall1a').hide();
      $('#tuwall2a').hide();
      $('#tuwall3').hide();

     });

 
  

  });

 

  
</script> 

<script src="{{asset('assets/js/jquery-1.12.4.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
   

    <script>
      
      // Start upload preview image
  $(".gambar").attr("src", "{{asset('front/fondo_c.png')}}");
  $(".gambar2").attr("src", "{{asset('front/fondo_r 2.png')}}");
  $(".gambar3").attr("src", "{{asset('front/fondo_c1.png')}}");
             
  var $uploadCrop,
              tempFilename,
              rawImg,
              imageId;
              function readFile(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $('#cropImagePop').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCrop = $('#upload-demo').croppie({
                viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePop').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCrop.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img').on('change', function () { 
                    


                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtn').data('id', imageId); readFile(this); 
                        $('#cropImageBtn').on('click', function (ev) {
                  $uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output').attr('src', resp);
                    console.log($uploadCrop);
                    document.getElementById('layout1-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePop').modal('hide');
                  });
                  });

                     
                

                 });
  

                 //lAYOUT 2 1
            
                 var $uploadCropA,
              tempFilename,
              rawImg,
              imageId;
              function readFileA(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoA').addClass('ready');
                    $('#cropImagePopA').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropA = $('#upload-demoA').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopA').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropA.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l2-1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnA').data('id', imageId); readFileA(this); 
                        $('#cropImageBtnA').on('click', function (ev) {
                  $uploadCropA.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-layout2-1').attr('src', resp);
                    console.log($uploadCropA);
                    document.getElementById('layout2-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopA').modal('hide');
                  });
                  });

                     
                

                 });

      document.getElementById('rotaterightA').addEventListener('click', function() {
      $uploadCropA.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftA').addEventListener('click', function() {
      $uploadCropA.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 2 2
            
                 var $uploadCropB,
              tempFilename,
              rawImg,
              imageId;
              function readFileB(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoB').addClass('ready');
                    $('#cropImagePopB').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropB = $('#upload-demoB').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopB').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropB.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l2-2').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnB').data('id', imageId); readFileB(this); 
                        $('#cropImageBtnB').on('click', function (ev) {
                  $uploadCropB.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l2-2').attr('src', resp);
                   
                    document.getElementById('layout2-2').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopB').modal('hide');
                  });
                  });

                 });

      document.getElementById('rotaterightB').addEventListener('click', function() {
      $uploadCropB.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftB').addEventListener('click', function() {
      $uploadCropB.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 2 3
            
                 var $uploadCropC,
              tempFilename,
              rawImg,
              imageId;
              function readFileC(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoC').addClass('ready');
                    $('#cropImagePopC').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropC = $('#upload-demoC').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopC').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropC.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l2-3').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnC').data('id', imageId); readFileC(this); 
                        $('#cropImageBtnC').on('click', function (ev) {
                  $uploadCropC.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l2-3').attr('src', resp);
                   
                    document.getElementById('layout2-3').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopC').modal('hide');
                  });
                  });

                 });

                 document.getElementById('rotaterightC').addEventListener('click', function() {
      $uploadCropC.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftC').addEventListener('click', function() {
      $uploadCropC.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 2 4
            
                 var $uploadCropD,
              tempFilename,
              rawImg,
              imageId;
              function readFileD(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoD').addClass('ready');
                    $('#cropImagePopD').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropD = $('#upload-demoD').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopD').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropD.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l2-4').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnD').data('id', imageId); readFileD(this); 
                        $('#cropImageBtnD').on('click', function (ev) {
                  $uploadCropD.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l2-4').attr('src', resp);
                   
                    document.getElementById('layout2-4').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopD').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightD').addEventListener('click', function() {
      $uploadCropD.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftD').addEventListener('click', function() {
      $uploadCropD.croppie('rotate', parseInt(90));
  });

                 //lAYOUT 3 1
            
                 var $uploadCropF,
              tempFilename,
              rawImg,
              imageId;
              function readFileF(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoF').addClass('ready');
                    $('#cropImagePopF').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropF = $('#upload-demoF').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopF').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropF.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnF').data('id', imageId); readFileF(this); 
                        $('#cropImageBtnF').on('click', function (ev) {
                  $uploadCropF.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-layout3-1').attr('src', resp);
                   
                    document.getElementById('layout3-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopF').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightF').addEventListener('click', function() {
      $uploadCropF.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftF').addEventListener('click', function() {
      $uploadCropF.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 3 2
            
                 var $uploadCropG,
              tempFilename,
              rawImg,
              imageId;
              function readFileG(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoG').addClass('ready');
                    $('#cropImagePopG').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropG = $('#upload-demoG').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopG').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropG.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-2').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnG').data('id', imageId); readFileG(this); 
                        $('#cropImageBtnG').on('click', function (ev) {
                  $uploadCropG.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l3-2').attr('src', resp);
                   
                    document.getElementById('layout3-2').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopG').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightG').addEventListener('click', function() {
      $uploadCropG.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftG').addEventListener('click', function() {
      $uploadCropG.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 3 3
            
                 var $uploadCropH,
              tempFilename,
              rawImg,
              imageId;
              function readFileH(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoH').addClass('ready');
                    $('#cropImagePopH').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropH = $('#upload-demoH').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopH').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropH.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-3').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnH').data('id', imageId); readFileH(this); 
                        $('#cropImageBtnH').on('click', function (ev) {
                  $uploadCropH.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l3-3').attr('src', resp);
                   
                    document.getElementById('layout3-3').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopH').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightH').addEventListener('click', function() {
      $uploadCropH.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftH').addEventListener('click', function() {
      $uploadCropH.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 3 4
            
                 var $uploadCropI,
              tempFilename,
              rawImg,
              imageId;
              function readFileI(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoI').addClass('ready');
                    $('#cropImagePopI').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropI = $('#upload-demoI').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopI').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropI.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-4').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnI').data('id', imageId); readFileI(this); 
                        $('#cropImageBtnI').on('click', function (ev) {
                  $uploadCropI.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l3-4').attr('src', resp);
                   
                    document.getElementById('layout3-4').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopI').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightI').addEventListener('click', function() {
      $uploadCropI.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftI').addEventListener('click', function() {
      $uploadCropI.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 3 5
            
                 var $uploadCropJ,
              tempFilename,
              rawImg,
              imageId;
              function readFileJ(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoJ').addClass('ready');
                    $('#cropImagePopJ').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropJ = $('#upload-demoJ').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopJ').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropJ.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-5').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnJ').data('id', imageId); readFileJ(this); 
                        $('#cropImageBtnJ').on('click', function (ev) {
                  $uploadCropJ.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l3-5').attr('src', resp);
                   
                    document.getElementById('layout3-5').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopJ').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightJ').addEventListener('click', function() {
      $uploadCropJ.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftJ').addEventListener('click', function() {
      $uploadCropJ.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 3 6
            
                 var $uploadCropK,
              tempFilename,
              rawImg,
              imageId;
              function readFileK(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoK').addClass('ready');
                    $('#cropImagePopK').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropK = $('#upload-demoK').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopK').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropK.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l3-6').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnK').data('id', imageId); readFileK(this); 
                        $('#cropImageBtnK').on('click', function (ev) {
                  $uploadCropK.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l3-6').attr('src', resp);
                   
                    document.getElementById('layout3-6').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopK').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightK').addEventListener('click', function() {
      $uploadCropK.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftK').addEventListener('click', function() {
      $uploadCropK.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 4 1
            
                 var $uploadCropL,
                  tempFilename,
                  rawImg,
                  imageId;
              function readFileL(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoL').addClass('ready');
                    $('#cropImagePopL').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropL = $('#upload-demoL').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopL').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropL.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l4-1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnL').data('id', imageId); readFileL(this); 
                        $('#cropImageBtnL').on('click', function (ev) {
                  $uploadCropL.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 450}
                  }).then(function (resp) {
                    $('#item-img-output-layout4-1').attr('src', resp);
                   
                    document.getElementById('layout4-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopL').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightL').addEventListener('click', function() {
      $uploadCropL.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftL').addEventListener('click', function() {
      $uploadCropL.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 4 2
            
                 var $uploadCropM,
              tempFilename,
              rawImg,
              imageId;
              function readFileM(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoM').addClass('ready');
                    $('#cropImagePopM').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropM = $('#upload-demoM').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopM').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropM.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l4-2').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnM').data('id', imageId); readFileM(this); 
                        $('#cropImageBtnM').on('click', function (ev) {
                  $uploadCropM.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 200, height: 200}
                  }).then(function (resp) {
                    $('#item-img-output-l4-2').attr('src', resp);
                   
                    document.getElementById('layout4-2').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopM').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightM').addEventListener('click', function() {
      $uploadCropM.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftM').addEventListener('click', function() {
      $uploadCropM.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 4 3
            
                var $uploadCropN,
              tempFilename,
              rawImg,
              imageId;
              function readFileN(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoN').addClass('ready');
                    $('#cropImagePopN').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropN = $('#upload-demoN').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopN').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropN.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l4-3').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnN').data('id', imageId); readFileN(this); 
                        $('#cropImageBtnN').on('click', function (ev) {
                  $uploadCropN.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l4-3').attr('src', resp);
                   
                    document.getElementById('layout4-3').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopN').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightN').addEventListener('click', function() {
      $uploadCropN.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftN').addEventListener('click', function() {
      $uploadCropN.croppie('rotate', parseInt(90));
  });

                     //lAYOUT 4 4
            
               var $uploadCropO,
              tempFilename,
              rawImg,
              imageId;
              function readFileO(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoO').addClass('ready');
                    $('#cropImagePopO').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropO = $('#upload-demoO').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopO').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropO.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l4-4').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnO').data('id', imageId); readFileO(this); 
                        $('#cropImageBtnO').on('click', function (ev) {
                  $uploadCropO.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 450}
                  }).then(function (resp) {
                    $('#item-img-output-l4-4').attr('src', resp);
                   
                    document.getElementById('layout4-4').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopO').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightO').addEventListener('click', function() {
      $uploadCropO.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftO').addEventListener('click', function() {
      $uploadCropO.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 5 1
            
                 var $uploadCropP,
              tempFilename,
              rawImg,
              imageId;
              function readFileP(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoP').addClass('ready');
                    $('#cropImagePopP').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropP = $('#upload-demoP').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopP').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropP.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l5-1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnP').data('id', imageId); readFileP(this); 
                        $('#cropImageBtnP').on('click', function (ev) {
                  $uploadCropP.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 450}
                  }).then(function (resp) {
                    $('#item-img-output-layout5-1').attr('src', resp);
                   
                    document.getElementById('layout5-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopP').modal('hide');
                  });
                  });

                 });
        
                 document.getElementById('rotaterightP').addEventListener('click', function() {
      $uploadCropP.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftP').addEventListener('click', function() {
      $uploadCropP.croppie('rotate', parseInt(90));
  });
             
                 //lAYOUT 5 2
            
                 var $uploadCropQ,
              tempFilename,
              rawImg,
              imageId;
              function readFileQ(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoQ').addClass('ready');
                    $('#cropImagePopQ').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropQ = $('#upload-demoQ').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopQ').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropQ.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l5-2').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnQ').data('id', imageId); readFileQ(this); 
                        $('#cropImageBtnQ').on('click', function (ev) {
                  $uploadCropQ.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 450}
                  }).then(function (resp) {
                    $('#item-img-output-l5-2').attr('src', resp);
                   
                    document.getElementById('layout5-2').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopQ').modal('hide');
                  });
                  });

                 });
        
                 document.getElementById('rotaterightQ').addEventListener('click', function() {
      $uploadCropQ.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftQ').addEventListener('click', function() {
      $uploadCropQ.croppie('rotate', parseInt(90));
  });
             
                 //lAYOUT 5 3
            
                 var $uploadCropR,
              tempFilename,
              rawImg,
              imageId;
              function readFileR(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoR').addClass('ready');
                    $('#cropImagePopR').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropR = $('#upload-demoR').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopR').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropR.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l5-3').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnR').data('id', imageId); readFileR(this); 
                        $('#cropImageBtnR').on('click', function (ev) {
                  $uploadCropR.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 450}
                  }).then(function (resp) {
                    $('#item-img-output-l5-3').attr('src', resp);
                   
                    document.getElementById('layout5-3').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopR').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightR').addEventListener('click', function() {
      $uploadCropR.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftR').addEventListener('click', function() {
      $uploadCropR.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 6 1
            
                 var $uploadCropS,
              tempFilename,
              rawImg,
              imageId;
              function readFileS(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoS').addClass('ready');
                    $('#cropImagePopS').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropS = $('#upload-demoS').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopS').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropS.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l6-1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnS').data('id', imageId); readFileS(this); 
                        $('#cropImageBtnS').on('click', function (ev) {
                  $uploadCropS.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 400, height: 400}
                  }).then(function (resp) {
                    $('#item-img-output-layout6-1').attr('src', resp);
                   
                    document.getElementById('layout6-1').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopS').modal('hide');
                  });
                  });

                 });
                 document.getElementById('rotaterightS').addEventListener('click', function() {
      $uploadCropS.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftS').addEventListener('click', function() {
      $uploadCropS.croppie('rotate', parseInt(90));
  });
        
                 //lAYOUT 6 2
            
                 var $uploadCropT,
              tempFilename,
              rawImg,
              imageId;
              function readFileT(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoT').addClass('ready');
                    $('#cropImagePopT').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropT = $('#upload-demoT').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopT').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropT.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l6-2').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnT').data('id', imageId); readFileT(this); 
                        $('#cropImageBtnT').on('click', function (ev) {
                  $uploadCropT.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l6-2').attr('src', resp);
                   
                    document.getElementById('layout6-2').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopT').modal('hide');
                  });
                  });

                 });
      document.getElementById('rotaterightT').addEventListener('click', function() {
      $uploadCropT.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftT').addEventListener('click', function() {
      $uploadCropT.croppie('rotate', parseInt(90));
  });
                 //lAYOUT 6 3
            
                 var $uploadCropU,
              tempFilename,
              rawImg,
              imageId;
              function readFileU(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoU').addClass('ready');
                    $('#cropImagePopU').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropU = $('#upload-demoU').croppie({
                 viewport: { width: 100, height: 100 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              $('#cropImagePopU').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropU.croppie('bind', {
                      url: rawImg
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-img-l6-3').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnU').data('id', imageId); readFileU(this); 
                        $('#cropImageBtnU').on('click', function (ev) {
                  $uploadCropU.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 300, height: 300}
                  }).then(function (resp) {
                    $('#item-img-output-l6-3').attr('src', resp);
                   
                    document.getElementById('layout6-3').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopU').modal('hide');
                  });
                  });

                 });

                 document.getElementById('rotaterightU').addEventListener('click', function() {
      $uploadCropU.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftU').addEventListener('click', function() {
      $uploadCropU.croppie('rotate', parseInt(90));
  });
                 //lTU WALLLLLLLL
            
                 var $uploadCropV,
              tempFilename,
              rawImg,
              imageId;
              function readFileV(input) {
                if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                    $('#upload-demoV').addClass('ready');
                    $('#cropImagePopV').modal('show');
                          rawImg = e.target.result;
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                    else {
                      swal("Sorry - you're browser doesn't support the FileReader API");
                  }
              }
  
              $uploadCropV = $('#upload-demoV').croppie({
                viewport: { width: 100, height: 150 },
                boundary: { width: 200, height: 200 },
                showZoomer: true,
                enableOrientation: true
              });
              
              $('#cropImagePopV').on('shown.bs.modal', function(){
               // alert('Shown pop');
              
                $uploadCropV.croppie('bind', {
                      url: rawImg 
                    }).then(function(){
                      console.log('jQuery bind complete');
                    });
              });
  
              $('.item-imgt1').on('change', function () { 
                  imageId = $(this).data('id'); 
                  tempFilename = $(this).val();
                  $('#cancelCropBtnV').data('id', imageId); readFileV(this); 
                        $('#cropImageBtnV').on('click', function (ev) {
                  $uploadCropV.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 240, height: 350}
                  }).then(function (resp) {
                    $('#item-img-output-t1').attr('src', resp);
                   
                    document.getElementById('twall').value = resp;
                   //$("layout1-1").val(resp);
                   
                    $('#cropImagePopV').modal('hide');
                  });
                  });

                 });
        
    document.getElementById('rotaterightV').addEventListener('click', function() {
      $uploadCropV.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleftV').addEventListener('click', function() {
      $uploadCropV.croppie('rotate', parseInt(90));
  });
        
                     
 //FIN TUWALL 

    document.getElementById('rotateright').addEventListener('click', function() {
      $uploadCrop.croppie('rotate', parseInt(-90));
  });
      document.getElementById('rotateleft').addEventListener('click', function() {
      $uploadCrop.croppie('rotate', parseInt(90));
  });
          // End upload preview image
    </script>


<script>
  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      
     
        $(".btn-tuwall").change(function(e){
     
            e.preventDefault();
            
            var med =  $('input:radio[name=medida]:checked').val();
           
           console.log(med);
            var cant = $("input[name=tcantidad]").val();
           

           
          
            if (med == '20 X 25') {
              document.getElementById("medidas_m").innerHTML = '20 X 25 cm.';
              document.getElementById("nombre_m").innerHTML = 'CUADRO MEDIANO';
            }
            if (med == '15 X 20') {
              document.getElementById("medidas_m").innerHTML = '15 X 20 cm.';
              document.getElementById("nombre_m").innerHTML = 'CUADRO CHICO';
            }
            if (med == '28 X 35') {
              document.getElementById("medidas_m").innerHTML = '28 X 35 cm.';
              document.getElementById("nombre_m").innerHTML = 'CUADRO GRANDE';
            }

           $.ajax({
               type:'POST',
               url:"{{ route('tprecios.post') }}",
               data:{med:med, cant:cant, _token: '{{csrf_token()}}'},
               success: function (response) {
                    $('#fw-div').html(response);
                },
            });
    
        });
</script>
<script>
    
  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".btn-lay").change(function(e){ 
            e.preventDefault();
            var name = $("select[name=materiales]").val();
            var cant = $("input[name=cantidad]").val();
            var lay =  $("input[name=layout]").val();
            console.log(name);
            console.log(cant);
            console.log(lay);
            
            $.ajax({
               type:'POST',
               url:"{{ route('lprecios.post') }}",
               data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
               success: function (response) {
                    $('#f314-div').html(response);
                },
            });
    
        });
 /*Layout 2*/
    $(".btn-lay2").change(function(e){
    e.preventDefault();
    var name = $("#material_l2").val();
    var cant = $("#cantidad_l2").val();
    var lay =  $("#layout_l2").val();
    console.log(name);
    console.log(cant);
    console.log(lay);
    $.ajax({
       type:'POST',
       url:"{{ route('lprecios.post') }}",
       data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
       success: function (response) {
            $('#l2-div').html(response);
        },
    });
});
/*Layout 3 */
$(".btn-lay3").change(function(e){
    
    e.preventDefault();
    var name = $("#material_l3").val();
    var cant = $("#cantidad_l3").val();
    var lay =  $("#layout_l3").val();

    var material = $(this).val();
  
    const mater = @JSON($materiales);
    
    document.getElementById("descripcion3").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion3").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion3").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion3").innerHTML = mater[5].material_descripcion;
}
    $.ajax({
       type:'POST',
       url:"{{ route('lprecios.post') }}",
       data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
       success: function (response) {
            $('#l3-div').html(response);
        },
    });

});
/*Layout 4 */
$(".btn-lay4").change(function(e){
    
    e.preventDefault();
    var name = $("#material_l4").val();
    var cant = $("#cantidad_l4").val();
    var lay =  $("#layout_l4").val();
    var material = $(this).val();
    e.preventDefault();

    const mater = @JSON($materiales);
    
    document.getElementById("descripcion4").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion4").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion4").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion4").innerHTML = mater[5].material_descripcion;
}


 
    $.ajax({
       type:'POST',
       url:"{{ route('lprecios.post') }}",
       data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
       success: function (response) {
            $('#l4-div').html(response);
        },
    });

});
/*Layout 5 */
$(".btn-lay5").change(function(e){
    
    e.preventDefault();
    var name = $("#material_l5").val();
    var cant = $("#cantidad_l5").val();
    var lay =  $("#layout_l5").val();
    var material = $(this).val();
    const mater = @JSON($materiales);
    
    document.getElementById("descripcion5").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion5").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion5").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion5").innerHTML = mater[5].material_descripcion;
}

    $.ajax({
       type:'POST',
       url:"{{ route('lprecios.post') }}",
       data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
       success: function (response) {
            $('#l5-div').html(response);
        },
    });

});
/*Layout 6 */
$(".btn-lay6").change(function(e){
    
    e.preventDefault();
    var name = $("#material_l6").val();
    var cant = $("#cantidad_l6").val();
    var lay =  $("#layout_l6").val();
    var material = $(this).val();
    const mater = @JSON($materiales);
    
    document.getElementById("descripcion6").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion6").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion6").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion6").innerHTML = mater[5].material_descripcion;
}
    $.ajax({
       type:'POST',
       url:"{{ route('lprecios.post') }}",
       data:{ name:name, cant:cant, lay:lay, _token: '{{csrf_token()}}'},
       success: function (response) {
            $('#l6-div').html(response);
        },
    });

});



$("select[name=color]").change(function(){
           var colore = $('select[name=color]').val();
           console.log(colore);
           if (colore == 'Negro') {
              $('#item-img-output-t1').css('border', 'solid 25px #252727');
            }
           if (colore == 'Chocolate') {
              $('#item-img-output-t1').css('border', 'solid 25px #362424');
            }
           if (colore == 'Caoba') {
              $('#item-img-output-t1').css('border', 'solid 25px #844E43');
            }
           if (colore == 'Nogal') {
              $('#item-img-output-t1').css('border', 'solid 25px #AE6645');
            }
           
        });

</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Agregado al carrito!',
        '',
        'success'
        )

  
</script>


    
@endif
<script>
  $(".btn-mat1").change(function(e){
    
    e.preventDefault();

   
    var material = $("select[name=materiales]").val();
    const mater = @JSON($materiales);
    
    document.getElementById("descripcion1").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion1").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion1").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion1").innerHTML = mater[5].material_descripcion;
}
  

});
  $(".btn-mat2").change(function(e){
    
    e.preventDefault();
    
   
    var material = $("select[name=material]").val();

    const mater = @JSON($materiales);
    
    document.getElementById("descripcion2").innerHTML = "";


    if (material == 'Acrílico') {
      document.getElementById("descripcion2").innerHTML = mater[2].material_descripcion;
    }
  
    if (material == 'Trovicel') {
      document.getElementById("descripcion2").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Foamboard') {
  document.getElementById("descripcion2").innerHTML = mater[5].material_descripcion;
}

});

 
  
 

</script>

@stop