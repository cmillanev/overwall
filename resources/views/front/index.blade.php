@extends('layouts.app2')

@section('css')
 {{-- Aquí van los css específicos de esta página --}}
 <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
 
 <style>

  
   .diferentes{
     background-color: #123f6f;
     color: white;
     font-size: 35pt;
     height: auto;
     text-align: center;
     font-family: 'gotham-bold';
     padding-top: 65px; 
     padding-bottom: 60px; 
   }
   .sello_1{
    width: auto;
    height: 168px;
    
   }
   #sello1{
     text-align: center;
   }
   #sello2{
     text-align: center;
   }
   #sellos{
     margin-top:10%;
   color:  #123F6F;
   font-family: 'gotham-medium';
   font-size: 10pt;
   }
   #texexplora{
    color:#123F6F;
    font-size: 18pt;
    padding-left: 15px;
    padding-right: 15px;
  }
   #cuelga{
    margin-left: 45%;
   }
   .video{
     height: 505px;
      display: flex;
     justify-content: center;
     align-items: center;
   }
   #buscador{
     background-color: #E3E3E2;
     display: flex;
     justify-content: center;
     align-items: center;
   }

  #filtro{
     display: flex;
     justify-content: center;
     align-items: center;
  }
 

  #distintivos{
    margin-top: 35px;
    margin-bottom: 55px;
    text-align: center;
  
  }

  .opciones{
  
   width: 60%;
   height: auto;
   cursor:pointer;
  }
 
  #tex2{
    display: flex;
    justify-content: center;
    align-items: center;
   }
 
  .colec{
    display: flex;
     justify-content: center;
     text-align: center;
  }
  .coleccion{
    margin-left:10%;
    margin-right:10%;
  }
  .titulocolecciones{
    font-size: 34px;
    text-align: center;
    color: #126EB5;
    margin-top: 60px;
    font-family: 'gotham-bold';
   
  }
  .titulocoleccion{
    font-family: 'menu';
    font-size: 29pt;
    text-align: left;
    color: #123f6f;
    
  }


  hr.linea {
    width: 70%;
    border: 2px solid #123f6f;
    border-radius: 5px;
    

}
.vida{
  height: 314px;
  margin: 30px;
}

.vida h1{
    font-family: 'gotham-bold';
    font-size: 44pt;
    color: #126EB5;
    text-align:  center;
}
  .grande{
    
  }

  .mediano{
    font-family: 'menu';
    font-size: 30pt;
    color: #123F6F;
    text-align: center;
   
  }
  .input-icono input{
    background-image: url('front/lupa.png');
    background-repeat: no-repeat;
    background-position: right;
    background-size: 45px;
    display: flex;
    align-items: center;
    border-radius: 15px;
    border-color: grey;
    height: 70px; 
    padding-left: 20px;
    font-size: 24pt;  
    margin-top: 13%;
    
  }

  #buscar{
  margin-top: 18px;
    text-align: center;
    background-color: #E3E3E2;
  }

  #parrafo{
  font-family: 'gotham-medium';
  padding: 15px; 
  margin: 28px; 
  font-size: 12pt; 
  color: #123f6f; 
  }

input{
  height: 15px;
}

  #colecciones{
   text-align: center;
   margin: 50px;
  
  }
  .colecciones{
    height: 156px;
    width: 325px;
   
  }

  .swiper-container {
      width: 100%;
      padding-top: 50px;
      padding-bottom: 50px;
      background-color: #E3E3E2;
    }

    .swiper-slide {
      background-position: center;
      background-size: cover;
      width: 300px;
      height: 300px;
      border: 15px solid white;

    }

    #myVideo {
  display: none;
}
   
@media only screen and (max-width: 800px) {
  .vida{
  height: auto;
}

.vida h1{
    font-family: 'gotham-bold';
    font-size: 14pt;
    color: #126EB5;
    text-align:  center;
}
.mediano{
    margin-top:1%;
    font-family: 'menu';
    font-size: 12pt;
    color: #123F6F;
    text-align: center;

   
  }
  .diferentes{
     background-color: #123f6f;
     color: white;
     font-size: 15pt;
     height: auto;
     text-align: center;
     font-family: 'gotham-bold';
     padding-top: 5%; 
     padding-bottom: 2%; 
   }
   .cllos{
   color:  #123F6F;
   font-family: 'gotham-medium';
   font-size: 10pt;
   margin-left:10%;
  
   }
   .video{
     height: 80%;
      display: flex;
     justify-content: center;
     align-items: center;
   }
   #cuelga{
    margin-left: 40%;
   }
   .explorar{
    margin-top:10%;

   }

   .input-icono input{
    background-image: url('front/lupa.png');
    background-repeat: no-repeat;
    background-position: right;
    padding: 0%;
    display: flex;
    align-items: center;
    border-radius: 15px;
    border-color: grey;
    height: 70px; 
    padding-left: 20px;
    font-size: 24pt;  
    margin: 0%; 
  }
  .swiper-slide {
      background-position: center;
      background-size: cover;
      width: 200px;
      height: 200px;
      border: 10px solid white;

    }
    .coleccion{
    margin-left:5%;
    margin-right:5%;
  }
  #sellos{
     margin-top:1%;
   color:  #123F6F;
   font-family: 'gotham-medium';
   font-size: 10pt;
   }
   #parrafo{
  font-family: 'gotham-medium';
  padding: 15px; 
  margin: 0%; 
  font-size: 12pt; 
  color: #123f6f; 
  }

  #distintivos{
    margin-top: 10px;
    margin-bottom: 0px;
    text-align: center;
  
  }

  .opciones{
  
   width: 80%;
   height: auto;
   cursor:pointer;
  }
 
}
 
 </style>
@endsection

@section('content')
    

  <div class="container-fluid">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        @php
          $active = "active";
        @endphp
        @foreach ($sliders as $slider)
          <div class="carousel-item {{$active}}">
            <img class="d-block w-100" src="{{$slider->url}}" alt="First slide">
          </div>
        @php
          $active = "";
        @endphp
        @endforeach
     </div>
    </div>
  
  
<div class="vida">
 <div class="row ">
        <div class="col-md-12 col-sm-12" >

        <h1 class="grande"> DALE VIDA A TUS PAREDES, LIMPIA EL AIRE QUE RESPIRAS</h1>
        </div> 
 </div>



<div class="row">
  <div class="col-md-12 col-sm-12">
    <h4 class="mediano">Arte, fotografía, obras de artistas, ilustración, recuerdos y momentos personales.</h4>
 
  </div>
</div>
</div>

    <div class="row diferentes">
    <div class="col-md-12 col-sm-12" >
      
      <p>¿SABES QUÉ NOS HACE DIFERENTES?</p>
      
    </div>
    </div>

 <div class="row video" id="video1">
   
       
       <div class="col-sm-8 col-md-6 ">
         <br>
         <img id= "cuelga" src="{{ asset('front/cuelga marco.png') }} " width="80" height="58"alt="" class="img-fluid" >
          <div class="embed-responsive embed-responsive-16by9">
          
        
         <video width="320" height="240"  controls>
          <source src="{{asset('front/ANIMA OVERWALL_FINAL2.mp4')}}" type="video/mp4">
        
        Your browser does not support the video tag.
        </video>
         </div>
       </div>
     
   </div>

  
   
   


   <div class="row explorar">
       <div class="col-xs-3 col-md-4 " id="sello1">
        <a data-fancybox data-width="640" data-height="360" href="front/pureti.mp4" ><img src="{{ asset('front/sello_12.png') }}" alt="" class="sello_1" ></a>
       </div>
       <div class="col-xs-6 col-md-4 ">
         <br>
         <p class="cllos" id="sellos" >TODOS NUESTROS PRODUCTOS CUENTAN CON UN 
                         TRATAMIENTO PARA PURIFICAR EL EQUIVALENTE A 
                         10,000 km RECORRIDOS POR UN AUTO EN CADA m2 
                         DE TU OBRA OVERWALL FAVORITA</p>
       </div>
       <div class="col-xs-3 col-md-4" id="sello2" >   
           <img src="{{ asset('front/sello_2.png') }}" alt="" class="sello_1">
       </div>
     
    </div>
 <br>
  
  
    <div class="row" id="buscar">
      <div class="col-xs-4 col-md-4">
        <img src="{{ asset('front/logo_getty.png') }}" alt="" class="sello_1" >
      </div>
      <div class="col-xs-4 col-md-4" id="explorar">
        <form action="{{ route('resultado') }}" method="GET" >
          <div class="form-group input-icono">
            <input class="form-control" type="text" name="frase" placeholder="BUSCAR"  >
            <input type="hidden" name="page" value="1">
          </div>
          </form>
      </div>
      <div class="col-xs-4 col-md-4 ">
     <p id="parrafo">Busca entre miles de fotografías, tu imagen perfecta, directo del banco Getty Images.</p>
      </div>
    </div>
 
 

   
    <div class="row" id="distintivos" >
      <div class="col-4 ">
        <a href="resultado?frase=arte&page=1">
        <img src="{{ asset('front/categorias.png') }}" alt="" class="opciones">
        </a>
      </div><!-- /.col-lg-4 -->
      <div class="col-4">
        <a href="{{ route('talentos') }}">
        <img src="{{ asset('front/talentos.png') }}" alt="" class="opciones">
        </a>
      </div><!-- /.col-lg-4 -->
      <div class="col-4">
        <a href="{{ route('tuwall') }}">
        <img src="{{ asset('front/tu wall.png') }}" alt="" class="opciones">
        </a>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


  
    <div  class="titulocolecciones">
      <p>COLECCIONES</p>
      <hr class="linea">
    </div>
 


  <div class="row coleccion" >

    @foreach ($colecciones as $coleccion)
        
    <div class="col-xs-3 col-md-4 ">
      <a href="resultado?frase={{ $coleccion->coleccion_palabras}}&page=1">
      <img src=" {{ $coleccion->coleccion_url }} " alt="" class="img-fluid colecciones">
      <p class="titulocoleccion">{{ $coleccion->coleccion_nombre }}</p>
      </a>
    </div>
    @endforeach
      
      
  </div>


  

<div class="titulocolecciones">
  <p>FAVORITOS</p>
  
  <form action="{{ route('vistaprevia') }}" method="GET" >
 <!-- Swiper -->
 <div class="swiper-container">
  <div class="swiper-wrapper">
   
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1222094964';" style="background-image:url(https://media.gettyimages.com/photos/colourful-wave-peaking-into-a-flare-with-sunrise-storm-picture-id1222094964?b=1&k=6&m=1222094964&s=170667a&w=0&h=DnXivM317JVZaNmrrBR5ZlEoxwbn1dWPtHsWIWrbmxU=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1128687123';" style="background-image:url(https://media.gettyimages.com/photos/shopping-bag-full-of-fresh-vegetables-and-fruits-picture-id1128687123?b=1&k=6&m=1128687123&s=170667a&w=0&h=Dv6OSlJOH4kwvoas9mmZJlTcwL-KQW23a05ywdhETnA=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=479667835';" style="background-image:url(https://media.gettyimages.com/photos/background-elephant-picture-id479667835?b=1&k=6&m=479667835&s=170667a&w=0&h=2c8zV7eebBzU7jOnNomv0xdr7pqtHZ2X4xawHEaWN-w=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=511675552';" style="background-image:url(https://media.gettyimages.com/photos/candy-skies-picture-id511675552?b=1&k=6&m=511675552&s=170667a&w=0&h=OdPuy35ayQuEK0C8e6nKOREEz2xUK_qXt0ooLmyPPR8=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1160979608';" style="background-image:url(https://media.gettyimages.com/photos/view-over-old-man-of-storr-isle-of-skye-scotland-picture-id1160979608?b=1&k=6&m=1160979608&s=170667a&w=0&h=WLSlmxWn3EXnt-0BrrbrQeoZbwV2Uwok7gtyPSwOzMo=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=926689776';" style="background-image:url(https://media.gettyimages.com/photos/low-angle-view-of-the-skyscrapers-in-nyc-picture-id926689776?b=1&k=6&m=926689776&s=170667a&w=0&h=fPfibSNzoZkDJgMT-i_iUMBPAekae_Fsdrd9pyiWkBg=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=542197312';" style="background-image:url(https://media.gettyimages.com/photos/lion-in-black-and-white-picture-id542197312?b=1&k=6&m=542197312&s=170667a&w=0&h=3AjD1mQzwX2klrku2oxasWDw241uX0q9lSpk2wKyd-w=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=95157138';" style="background-image:url(https://media.gettyimages.com/photos/antarctic-babysitter-picture-id95157138?b=1&k=6&m=95157138&s=170667a&w=0&h=FgRPtmUwCXGaWv7pxIogInXFnbkHdjYGI4eqG2r6d-o=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=953823382';" style="background-image:url(https://media.gettyimages.com/photos/autumn-colored-leaves-glowing-in-sunlight-in-avenue-of-beech-trees-picture-id953823382?b=1&k=6&m=953823382&s=170667a&w=0&h=cUbqT5r0GvYRZmV38-LdRdkqsmiN6_pNRRD_kGgZUC0=)"></div>
    <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1164005273';" style="background-image:url(https://media.gettyimages.com/photos/the-city-of-london-just-after-sunset-united-kingdom-picture-id1164005273?b=1&k=6&m=1164005273&s=170667a&w=0&h=x-8_EkfpoShEzJzfzndvd8EhESw_dEGgcjFFnwFV1Xs=)"></div>


  </div>
  <!-- Add Pagination -->
  <div class="swiper-pagination"></div>
</div>
  </form>
</div>
</div>  

@endsection

@section('js')



    {{--Aqui van los js específicos de esta página--}}
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
      var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        loop: true,
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        },
        pagination: {
          el: '.swiper-pagination',
        },
      });
    </script>

 <script src="{{asset('js/js/detectmobilebrowser.js')}}"></script>
<script>
  
  if($.browser.mobile == false){
    //  $(".swiper-container1").hide();
     
        console.log('oculta imagenes');
  }

</script>
@endsection