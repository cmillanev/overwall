@extends('layouts.app2')

@section('css')
	<!-- Link Swiper's CSS -->
 {{-- Aquí van los css específicos de esta página --}}
 <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
 
<style type="text/css">
#distintivos{
    margin-top: 35px;
    margin-bottom: 55px;
    text-align: center;
  
  }
  .opciones{
  
   width:  270px;
   height: 326px;
  }
#es{
  width: 500px;
  height: 500px;
  padding: 20px;
}
#co{
  width: 700px;
  height: 350px;
}
#cu{
  width: 270px;
  height: 270px;
}

.swiper-container {
      width: 100%;
      padding-top: 50px;
      padding-bottom: 50px;
      background-color: #E3E3E2;
    }

    .swiper-slide {
      background-position: center;
      background-size: cover;
      width: 300px;
      height: 300px;
      border: 15px solid white;

    }

</style> 
@endsection

@section('content')

<div class="container-fluid">
    
  <div class="row">
      <div class="col-md-5">
        <img  class="img-fluid" id="es" src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-03.png') }}" alt="">
      </div>
      <div class="col-md-7">
          <img class="img-fluid" id="co" src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-02.png') }}" alt="">
      </div>
  </div>

<div>
  <form action="{{ route('vistaprevia') }}" method="GET" >
    <!-- Swiper -->
    <div class="swiper-container">
     <div class="swiper-wrapper">
      
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1222094964';" style="background-image:url(https://media.gettyimages.com/photos/colourful-wave-peaking-into-a-flare-with-sunrise-storm-picture-id1222094964?b=1&k=6&m=1222094964&s=170667a&w=0&h=DnXivM317JVZaNmrrBR5ZlEoxwbn1dWPtHsWIWrbmxU=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1128687123';" style="background-image:url(https://media.gettyimages.com/photos/shopping-bag-full-of-fresh-vegetables-and-fruits-picture-id1128687123?b=1&k=6&m=1128687123&s=170667a&w=0&h=Dv6OSlJOH4kwvoas9mmZJlTcwL-KQW23a05ywdhETnA=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=479667835';" style="background-image:url(https://media.gettyimages.com/photos/background-elephant-picture-id479667835?b=1&k=6&m=479667835&s=170667a&w=0&h=2c8zV7eebBzU7jOnNomv0xdr7pqtHZ2X4xawHEaWN-w=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=511675552';" style="background-image:url(https://media.gettyimages.com/photos/candy-skies-picture-id511675552?b=1&k=6&m=511675552&s=170667a&w=0&h=OdPuy35ayQuEK0C8e6nKOREEz2xUK_qXt0ooLmyPPR8=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1160979608';" style="background-image:url(https://media.gettyimages.com/photos/view-over-old-man-of-storr-isle-of-skye-scotland-picture-id1160979608?b=1&k=6&m=1160979608&s=170667a&w=0&h=WLSlmxWn3EXnt-0BrrbrQeoZbwV2Uwok7gtyPSwOzMo=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=926689776';" style="background-image:url(https://media.gettyimages.com/photos/low-angle-view-of-the-skyscrapers-in-nyc-picture-id926689776?b=1&k=6&m=926689776&s=170667a&w=0&h=fPfibSNzoZkDJgMT-i_iUMBPAekae_Fsdrd9pyiWkBg=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=542197312';" style="background-image:url(https://media.gettyimages.com/photos/lion-in-black-and-white-picture-id542197312?b=1&k=6&m=542197312&s=170667a&w=0&h=3AjD1mQzwX2klrku2oxasWDw241uX0q9lSpk2wKyd-w=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=95157138';" style="background-image:url(https://media.gettyimages.com/photos/antarctic-babysitter-picture-id95157138?b=1&k=6&m=95157138&s=170667a&w=0&h=FgRPtmUwCXGaWv7pxIogInXFnbkHdjYGI4eqG2r6d-o=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=953823382';" style="background-image:url(https://media.gettyimages.com/photos/autumn-colored-leaves-glowing-in-sunlight-in-avenue-of-beech-trees-picture-id953823382?b=1&k=6&m=953823382&s=170667a&w=0&h=cUbqT5r0GvYRZmV38-LdRdkqsmiN6_pNRRD_kGgZUC0=)"></div>
       <div class="swiper-slide" onclick="window.location='https://overwall.com.mx/vistaprevia?id=1164005273';" style="background-image:url(https://media.gettyimages.com/photos/the-city-of-london-just-after-sunset-united-kingdom-picture-id1164005273?b=1&k=6&m=1164005273&s=170667a&w=0&h=x-8_EkfpoShEzJzfzndvd8EhESw_dEGgcjFFnwFV1Xs=)"></div>
   
   
     </div>
     <!-- Add Pagination -->
     <div class="swiper-pagination"></div>
   </div>
     </form>
</div>
  
 <br>
    <div style="background-color: #123F6F; width: auto; height: 30px;"></div>
    <br>
   
<div class="row">
    <div class="col-md-4">
    <img  id="cu" class="es" src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-04.png') }}" alt="">
    </div>
    <div class="col-md-8">
     
      <h1 align="center" style="color: #123F6F; font-family:DIN-ALTERNATES-BOLD; font-size: 35px; margin-top: 30px;">DISFRUTA TU OBRA Y EL AIRE LIMPIO</h1>

  <p align="center" style="color: #123F6F; font-family:Gotham-Medium; font-size: 24px;">RECUERDA QUE TODOS NUESTROS PRODUCTOS CUENTAN CON UN TRATAMIENTO PARA PURIFICAR EL AIRE EQUIVALENTE A 10,000 KM RECORIDOS POR UN AUTO EN CADA METRO CUADRADO.</p>
    </div>
</div>
<br>
    <div style="background-color: #123F6F; width: auto; height: 30px;"></div>
    <br>
<div>
  <h1 align="center" style="color: #123F6F; font-family:Gotham-Bold; font-size: 44px;">¿LISTOS PARA SEGUIR EXPLORANDO?</h1>
</div>
<div class="row" id="distintivos" >
    <div class="col-lg-4 ">
      <a href="resultado?frase=arte&page=1">
      <img src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-05.png') }}" alt="" class="opciones">
      </a>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <a href="{{ route('talentos') }}">
      <img src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-06.png') }}" alt="" class="opciones">
      </a>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <a href="{{ route('tuwall') }}">
      <img src="{{ asset('front/pospago/OVERWALL_POST_COMPRA_PX-07.png') }}" alt="" class="opciones">
      </a>
    </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->



</div>

@endsection

@section('js')
 
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
      var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        loop: true,
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        },
        pagination: {
          el: '.swiper-pagination',
        },
      });
    </script>
 
@endsection