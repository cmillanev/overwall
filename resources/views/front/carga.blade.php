@extends('layouts.app2') @section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<link rel="stylesheet" href="{{asset('assets/css/layout.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/cropper/cropper.css')}}">
<style>
    #distintivos{
        background-color: #123f6f;
        height: 130px;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .opciones{
        height: 135px;
      }
      .opciones2{
        height: 135px;
        padding-left:40px;
      }
    
      .opciones1{
        text-align: center;
      }
    
      .texto{
        padding: 25px;
      }
      
      .mostrar1{
        margin: 40px;
      
      }
      .preview {
      border:1px solid #ddd;
      padding:5px;
      border-radius:2px;
      background:#fff;
      max-width:200px;
      
    }
    @media all and (orientation:portrait){
       .preview img {
       height: 430px;
        width: 270px;
        display:block;
        
        }
    }
    @media all and (orientation:landscape){
       .preview img {
       height: 270px;
        width: 430px;
        display:block;
        
        }
    }
      
    
      .titulo{
        color: #123f6f;
        display: flex;
        justify-content: center;
        align-items: center;
      }
    
      #tuwall2{
        background-color: #E3E3E2;
        height: 725px;
      }
    
      #marcos{
        width: 100%;
      }
      label, .lazul{
        color: #123f6f;
      }
        input[type]:focus{
          border-color: #123f6f;
        }
      
      #submit{
        background-color: #123f6f;
        color: white;
      }
      #letramarco{
        margin-top: 100px;
      }
      #card{  
        margin: 60px; 
      }
    
      .file-upload{
    width: 450px; 
    height:390px;
    
    margin:40px auto;
    border:15px solid #123f6f;
    background: white;
    overflow:hidden;
    position:relative;
    }
    .file-upload input{
    position:absolute;
    height:390px;
    width:450px;
    left:-200px;
    top:-200px;
    background:transparent;
    opacity:0;
    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    filter: alpha(opacity=0);
    
    }
    .file-upload img{
    height:390px;
    width:450px;
    
    }
    .hiddenFileInput > input{
    height: 0;
    width: 0;
    opacity: 0;
    cursor: pointer;
    }
    .hiddenFileInput{
    
     width: 0px;
    height: 0px;
    display: inline-block;
    overflow: hidden;
    cursor: pointer;
    }
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" /> @stop @section('content')

@section('content')
    
<!-- The Make Selection Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
      <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
              <h4 class="modal-title">Make a selection</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
              <div id="cropimage">
                  <img id="imageprev" src="assets/img/bg.png" />
              </div>

              <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
              </div>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
              <div class="btngroup">
                  <button type="button" class="btn upload1 float-left" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btnsmall" id="rotateL" title="Rotate Left"><i class="fa fa-undo"></i></button>
                  <button type="button" class="btn btnsmall" id="rotateR" title="Rotate Right"><i class="fa fa-repeat"></i></button>
                  <button type="button" class="btn btnsmall" id="scaleX" title="Flip Horizontal"><i class="fa fa-arrows-h"></i></button>
                  <button type="button" class="btn btnsmall" id="scaleY" title="Flip Vertical"><i class="fa fa-arrows-v"></i></button>
                  <button type="button" class="btn btnsmall" id="reset" title="Reset"><i class="fa fa-refresh"></i></button>
                  <button type="button" class="btn camera1 float-right" id="saveAvatar">Save</button>
              </div>
          </div>

      </div>
  </div>
</div>  
<section class="">

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 1.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 2.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 4.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 5.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 6.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 7.png'}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{'carruseles_tuwall/carrusel tu wall 8.png'}}" alt="First slide">
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</section>

<div class="container">
    <div class="row" id="tipo">
        <div class="col-md-6 opciones1">

            <img src="front/imglayout.png" width="215" height="258" alt="" class="img-fluid" id="mostrar1">

        </div>
        <div class="col-md-6 opciones1">

            <img src="front/marcos.png" width="215" height="258" alt="" class="img-fluid" id="mostrar2">

        </div>
    </div>
</div>
{{--Empieza el layout --}}
<div class="container target1">
    <div class="texto">
        <h2 class="titulo">SELECCIONA TU LAYOUT</h2>
    </div>

    <div class="row" id="distintivos">
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 1.png') }}" alt="" class="opciones" id="layout1"><br>

        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 2.png') }}" alt="" class="opciones" id="layout2">
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 3.png') }}" alt="" class="opciones" id="layout3">
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 4.png') }}" alt="" class="opciones" id="layout4">
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 5.png') }}" alt="" class="opciones" id="layout5">
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-2">
            <img src="{{ asset('front/layout 6.png') }}" alt="" class="opciones" id="layout6">
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->

    <div class="texto">
        <h2 class="titulo">CARGA TUS FOTOS</h2>
    </div>

</div>
{{-- Layout 1--}}
<div class="container mostrarlay1">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('clayout')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">
                    <!--Start a2z-area-->
                    <div class="imagearea col-lg-5 col-md-5 col-sm-12">
                      <div class="avatarimage" id="drop-area">
                          <img src="{{asset('assets/img/avatar.jpg')}}" alt="avatar" id="avatarimage" />
                          
                      </div>
                      <div class="buttonarea">
                          <!-- <button class="btn upload" data-toggle="modal" data-target="#myModal" onclick="cropImage()"><i class="fa fa-upload"></i> &nbsp; Upload</button> -->

                          <label class="btn upload"> <i class="fa fa-upload"></i> &nbsp; Upload<input type="file" class="sr-only" id="input" name="image" accept="image/*"></label>
                        
                      </div>
                      <div class="alert" role="alert"></div>
                  </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button>
            </form>
            @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>
{{-- Layouy FIN 1--}} {{-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++Layout 2++++++++++++++++++++++++++++++++++--}}
<div class="container mostrarlay2">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            <input class="form-control" type="text" name="precio3" value="" id=""><span id="spTotal"></span> @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button> @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

            </form>

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>

{{--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Iniciar layour 3--}}
<div class="container mostrarlay3">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                            3
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            <input class="form-control" type="text" name="precio3" value="" id=""><span id="spTotal"></span> @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button> @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

            </form>

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++FIN layour 3--}} {{--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Iniciar layour 4--}}
<div class="container mostrarlay4">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                            4
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            <input class="form-control" type="text" name="precio3" value="" id=""><span id="spTotal"></span> @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button> @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

            </form>

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++FIN layour 4--}} {{--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Iniciar layour 5--}}
<div class="container mostrarlay5">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                            5
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            <input class="form-control" type="text" name="precio3" value="" id=""><span id="spTotal"></span> @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button> @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

            </form>

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++FIN layour 5--}} {{--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Iniciar layour 6--}}
<div class="container mostrarlay6">
    <div class="row fotos">
        <div class="col-8">
            <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card" style="width: 550px; height:390px;" id="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                            6
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                </div>
                <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                </p>
        </div>

        <div class="col-md-3">

            <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
            </p>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
                <select name="materiales" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
            <option value="0">Seleccione</option>
         @foreach ($materiales as $material)
            <option value="{{ $material->material_precio}}">{{ $material->material_nombre }}</option>
         @endforeach
     </select>
            </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" value="1">
            </div>

            <input class="form-control" type="text" name="precio3" value="" id=""><span id="spTotal"></span> @if (Auth::check())

            <button class="form-control" type="submit" id="submit">Agregar al carrito</button> @else {!! print_r("Registrate o inicia sesión para continuar") !!}

            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
            </ul>
            @endif

            </form>

        </div>
        <div class="col-md-1">
        </div>

    </div>
</div>
{{--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++FIN layour 6--}} {{--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++INICIA TU WALL++++++++++++++++++++++--}}

<div class="container target2">

    <div class="texto">
        <h2 class="titulo"> TU WALL</h2>
    </div>

    <form action="{{ route('cmarco')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row" id="distintivos">
            <div class="col-lg-4">
                <img src="{{ asset('front/medida 1.png') }}" alt="" class="opciones2">
                <input type="radio" id="medida1" name="medida" value="15 X 20" class="btn-tuwall">
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img src="{{ asset('front/medida 2.png') }}" alt="" class="opciones2">
                <input type="radio" id="medida2" name="medida" value="20 X 25" class="btn-tuwall">
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img src="{{ asset('front/medida 3.png') }}" alt="" class="opciones2">
                <input type="radio" id="medida3" name="medida" value="28 X 35" class="btn-tuwall">
            </div>
            <!-- /.col-lg-4 -->

        </div>
        <!-- /.row -->

        <div class=" ">
            <img src="{{ asset('front/coloresmarcos.png') }}" alt="" id="marcos" class=" img-fluid">
        </div>

        <div class="container" id="tuwall2">
            <div class="row">
                <div class="col-8">
                    <div class="card" style="width: 550px; height:390px;" id="card">
                        <input class="form-control" id="file" name="file" type="file" />
                        <hr>
                        <div class="preview" id="preview"></div>

                    </div>
                    <p> IMPORTANTE: Cuida que tus fotos esten en alta resolución y <br> con buena calidad. Recomendamos un mínimo <br> de 600 x 600 px y 300 dpi.
                    </p>
                </div>

<div class="col-md-3">

                    <p style="color: #123f6f;" id="letramarco">MARCO OVERWALL CHICO <br> 14 x 21 cm
                    </p>

                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Acabado de Fotografía</label>
              <select name="acabado" id="txt_campo_1" onchange="sumar(this.value);" class="form-control lazul">
          <option value="0">Seleccione</option>
     
          <option value="Mate" selected>Mate</option>
      
         </select>
                    </div>
     <div class="form-group">
                        <label for="exampleFormControlSelect1">Color Marco</label>
        <select name="color" id="txt_campo_2" class="form-control lazul">
          <option value="0">Seleccione</option>
     
          <option value="Negro" selected>Negro</option>
          <option value="Chocolate">Chocolate</option>
          <option value="Caoba">Caoba</option>
          <option value="Nogal">Nogal</option>
      
        </select>
    </div>

            <div class="form-group">
                <label for="">Cantidad</label>
                <input type="number" class="form-control btn-tuwall" name="tcantidad" value="1">
            </div>

     MX $<span id="f315-div">0</span>

                    <br> @if (Auth::check())

                    <button class="form-control" type="submit" id="submit">Agregar al carrito</button> 
                    
                        @else {!! print_r("Registrate o inicia sesión para continuar") !!}

                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> @if (Route::has('register'))

                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> @endif
                    </ul>
                    @endif

    </form>
</div>
    <div class="col-md-1"></div>

    </div>
  







    <script src="{{asset('assets/js/jquery-1.12.4.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/js/dropzone.js')}}"></script>
    <!-- Script -->
    <script src="{{asset('webcamjs/webcam.min.js')}}"></script>
    <script src="{{asset('assets/cropper/cropper.js')}}"></script>

    <script language="JavaScript">
        // Configure a few settings and attach camera
        		function configure(){
        			Webcam.set({
        				width: 640,
        				height: 480,
        				image_format: 'jpeg',
        				jpeg_quality: 100
        			});
        			Webcam.attach('#my_camera');
        		}
        		// A button for taking snaps
        		
        		function take_snapshot() {
        			// take snapshot and get image data
        			Webcam.snap( function(data_uri) {
        				// display results in page
        				$("#cameraModal").modal('hide');
        				$("#myModal").modal({backdrop: "static"});
        				$("#cropimage").html('<img id="imageprev" src="'+data_uri+'"/>');
        				cropImage();
        				//document.getElementById('cropimage').innerHTML = ;
        			} );
        
        			Webcam.reset();
        		}
        
        		function saveSnap(){
        			// Get base64 value from <img id='imageprev'> source
        			var base64image =  document.getElementById("imageprev").src;
        
        			 Webcam.upload( base64image, 'upload.php', function(code, text) {
        				 console.log('Save successfully');
        				 //console.log(text);
                    });
        
        		}
        	
        $('#cameraModal').on('show.bs.modal', function () {
          configure();
        })
        
        $('#cameraModal').on('hide.bs.modal', function () {
          Webcam.reset();
          $("#cropimage").html("");
        })
        
        $('#myModal').on('hide.bs.modal', function () {
         $("#cropimage").html('<img id="imageprev" src="assets/img/bg.png"/>');
        })
        
        
        /* UPLOAD Image */	
        var input = document.getElementById('input');
        var $alert = $('.alert');
        
        
        /* DRAG and DROP File */
        $("#drop-area").on('dragenter', function (e){
        	e.preventDefault();
        });
        
        $("#drop-area").on('dragover', function (e){
        	e.preventDefault();
        });
        
        $("#drop-area").on('drop', function (e){
        	var image = document.querySelector('#imageprev');
        	var files = e.originalEvent.dataTransfer.files;
        	
        	var done = function (url) {
                  input.value = '';
                  image.src = url;
                  $alert.hide();
        		  $("#myModal").modal({backdrop: "static"});
        		  cropImage();
                };
        		
        	var reader;
                var file;
                var url;
        
                if (files && files.length > 0) {
                  file = files[0];
        
                  if (URL) {
                    done(URL.createObjectURL(file));
                  } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                      done(reader.result);
                    };
                    reader.readAsDataURL(file);
                  }
                }	
        	
        	e.preventDefault();	
        	
        });
        
        /* INPUT UPLOAD FILE */
        input.addEventListener('change', function (e) {
        		var image = document.querySelector('#imageprev');
                var files = e.target.files;
                var done = function (url) {
                  input.value = '';
                  image.src = url;
                  $alert.hide();
        		  $("#myModal").modal({backdrop: "static"});
        		  cropImage();
        		  
                };
                var reader;
                var file;
                var url;
        
                if (files && files.length > 0) {
                  file = files[0];
        
                  if (URL) {
                    done(URL.createObjectURL(file));
                  } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                      done(reader.result);
                    };
                    reader.readAsDataURL(file);
                  }
                }
              });
        /* CROP IMAGE AFTER UPLOAD */	
        function cropImage() {
              var image = document.querySelector('#imageprev');
              var minAspectRatio = 0.5;
              var maxAspectRatio = 1.5;
        	  
              var cropper = new Cropper(image, {
        		aspectRatio: 11 /12,  
        		minCropBoxWidth: 220,
        		minCropBoxHeight: 240,
        
                ready: function () {
                  var cropper = this.cropper;
                  var containerData = cropper.getContainerData();
                  var cropBoxData = cropper.getCropBoxData();
                  var aspectRatio = cropBoxData.width / cropBoxData.height;
                  //var aspectRatio = 4 / 3;
                  var newCropBoxWidth;
        		  cropper.setDragMode("move");
                  if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
                    newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);
        
                    cropper.setCropBoxData({
                      left: (containerData.width - newCropBoxWidth) / 2,
                      width: newCropBoxWidth
                    });
                  }
                },
        
                cropmove: function () {
                  var cropper = this.cropper;
                  var cropBoxData = cropper.getCropBoxData();
                  var aspectRatio = cropBoxData.width / cropBoxData.height;
        
                  if (aspectRatio < minAspectRatio) {
                    cropper.setCropBoxData({
                      width: cropBoxData.height * minAspectRatio
                    });
                  } else if (aspectRatio > maxAspectRatio) {
                    cropper.setCropBoxData({
                      width: cropBoxData.height * maxAspectRatio
                    });
                  }
                },
        		
        		
              });
        	  
        	  $("#scaleY").click(function(){ 
        		var Yscale = cropper.imageData.scaleY;
        		if(Yscale==1){ cropper.scaleY(-1); } else {cropper.scaleY(1);};
        	  });
        	  
        	  $("#scaleX").click( function(){ 
        		var Xscale = cropper.imageData.scaleX;
        		if(Xscale==1){ cropper.scaleX(-1); } else {cropper.scaleX(1);};
        	  });
        	  
        	  $("#rotateR").click(function(){ cropper.rotate(45); });
        	  $("#rotateL").click(function(){ cropper.rotate(-45); });
        	  $("#reset").click(function(){ cropper.reset(); });
        	  
        	  $("#saveAvatar").click(function(){
        		  var $progress = $('.progress');
        		  var $progressBar = $('.progress-bar');
        		  var avatar = document.getElementById('avatarimage');
        		  var $alert = $('.alert');
                  canvas = cropper.getCroppedCanvas({
                    width: 220,
                    height: 240,
                  });
                  
                  $progress.show();
                  $alert.removeClass('alert-success alert-warning');
                  canvas.toBlob(function (blob) {
                   
                    avatar.src = initialAvatarURL;
                   
        			    	avatar.src = canvas.toDataURL();
                   

                  });
                  initialAvatarURL = avatar.src;
                  
        	  });
        };
    </script>

    @endsection
    
    @section('js')

    <script>
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

            
           
              $(".btn-tuwall").change(function(e){
          
                  e.preventDefault();
                  
                  var med =  $('input:radio[name=medida]:checked').val();
                 
                 console.log(med);
                  var cant = $("input[name=tcantidad]").val();
                 
          
                  $.ajax({
                     type:'POST',
                     url:"{{ route('tprecios.post') }}",
                     data:{med:med, cant:cant, _token: '{{csrf_token()}}'},
                     success: function (response) {
                          $('#f315-div').html(response);
                      },
                  });
          
              });
    </script>

    <script>
        $(document).ready(function(){
          $('.target1').hide();
          $('.target2').hide();
          $('.mostrarlay1').hide();
          $('.mostrarlay2').hide();
          $('.mostrarlay3').hide();
          $('.mostrarlay4').hide();
          $('.mostrarlay5').hide();
          $('.mostrarlay6').hide();
        
        	$("#mostrar1").click(function(){
        		$('.target1').show();
        	  $('.target2').hide();
            $('.mostrarlay1').hide();
            $('.mostrarlay2').hide();
            $('.mostrarlay3').hide();
            $('.mostrarlay4').hide();
            $('.mostrarlay5').hide();
            $('.mostrarlay6').hide();
         	});
        	$("#mostrar2").click(function(){
        	  $('.target2').show();
        		$('.target1').hide();
            $('.mostrarlay1').hide();
            $('.mostrarlay2').hide();
            $('.mostrarlay3').hide();
            $('.mostrarlay4').hide();
            $('.mostrarlay5').hide();
            $('.mostrarlay6').hide();
        	
        	});
        
          $("#layout1").click(function(){
        		$('.mostrarlay1').show();
        	  $('.mostrarlay2').hide();
            $('.mostrarlay3').hide();
        	  $('.mostrarlay2').hide();
        	  $('.mostrarlay4').hide();
        	  $('.mostrarlay5').hide();
        	  $('.mostrarlay6').hide();
         	});
        
          $("#layout2").click(function(){
        		$('.mostrarlay2').show();
        	  $('.mostrarlay1').hide();
            $('.mostrarlay3').hide();
        	  $('.mostrarlay4').hide();
        	  $('.mostrarlay5').hide();
        	  $('.mostrarlay6').hide();
        	  
         	});
           $("#layout3").click(function(){
        		$('.mostrarlay3').show();
        	  $('.mostrarlay1').hide();
        	  $('.mostrarlay2').hide();
        	  $('.mostrarlay4').hide();
        	  $('.mostrarlay5').hide();
        	  $('.mostrarlay6').hide();
        	  
         	});
           $("#layout4").click(function(){
        		$('.mostrarlay4').show();
        	  $('.mostrarlay1').hide();
        	  $('.mostrarlay2').hide();
        	  $('.mostrarlay3').hide();
        	  $('.mostrarlay5').hide();
        	  $('.mostrarlay6').hide();
        	  
         	});
           $("#layout5").click(function(){
        		$('.mostrarlay5').show();
        	  $('.mostrarlay1').hide();
        	  $('.mostrarlay2').hide();
        	  $('.mostrarlay3').hide();
        	  $('.mostrarlay4').hide();
        	  $('.mostrarlay6').hide();
        	  
         	});
           $("#layout6").click(function(){
        		$('.mostrarlay6').show();
        	  $('.mostrarlay1').hide();
        	  $('.mostrarlay2').hide();
        	  $('.mostrarlay3').hide();
        	  $('.mostrarlay5').hide();
        	  $('.mostrarlay4').hide();
        	  
         	});
        });
    </script>

    <script src="{{ asset('js/js/cropper.js') }}"></script>
    <script src="{{ asset('js/js/dropzone.js') }}"></script>
    <script src="{{ asset('js/js/webcam.js') }}"></script>

    <script language="JavaScript">
        // Configure a few settings and attach camera
          function configure(){
            Webcam.set({
              width: 640,
              height: 480,
              image_format: 'jpeg',
              jpeg_quality: 100
            });
            Webcam.attach('#my_camera');
          }
          // A button for taking snaps
          
          function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap( function(data_uri) {
              // display results in page
              $("#cameraModal").modal('hide');
              $("#myModal").modal({backdrop: "static"});
              $("#cropimage").html('<img id="imageprev" src="'+data_uri+'"/>');
              cropImage();
              //document.getElementById('cropimage').innerHTML = ;
            } );
        
            Webcam.reset();
          }
        
          function saveSnap(){
            // Get base64 value from <img id='imageprev'> source
            var base64image =  document.getElementById("imageprev").src;
        
             Webcam.upload( base64image, 'upload.php', function(code, text) {
               console.log('Save successfully');
               //console.log(text);
                  });
        
          }
        
        $('#cameraModal').on('show.bs.modal', function () {
        configure();
        })
        
        $('#cameraModal').on('hide.bs.modal', function () {
        Webcam.reset();
        $("#cropimage").html("");
        })
        
        $('#myModal').on('hide.bs.modal', function () {
        $("#cropimage").html('<img id="imageprev" src="{{ 'asset("img/bg.png")'}}"/>');
        })
        
        
        /* UPLOAD Image */	
        var input = document.getElementById('input');
        var $alert = $('.alert');
        
        
        /* DRAG and DROP File */
        $("#drop-area").on('dragenter', function (e){
        e.preventDefault();
        });
        
        $("#drop-area").on('dragover', function (e){
        e.preventDefault();
        });
        
        $("#drop-area").on('drop', function (e){
        var image = document.querySelector('#imageprev');
        var files = e.originalEvent.dataTransfer.files;
        
        var done = function (url) {
                input.value = '';
                image.src = url;
                $alert.hide();
            $("#myModal").modal({backdrop: "static"});
            cropImage();
              };
          
        var reader;
              var file;
              var url;
        
              if (files && files.length > 0) {
                file = files[0];
        
                if (URL) {
                  done(URL.createObjectURL(file));
                } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function (e) {
                    done(reader.result);
                  };
                  reader.readAsDataURL(file);
                }
              }	
        
        e.preventDefault();	
        
        });
        
        /* INPUT UPLOAD FILE */
        input.addEventListener('change', function (e) {
          var image = document.querySelector('#imageprev');
              var files = e.target.files;
              var done = function (url) {
                input.value = '';
                image.src = url;
                $alert.hide();
            $("#myModal").modal({backdrop: "static"});
            cropImage();
            
              };
              var reader;
              var file;
              var url;
        
              if (files && files.length > 0) {
                file = files[0];
        
                if (URL) {
                  done(URL.createObjectURL(file));
                } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function (e) {
                    done(reader.result);
                  };
                  reader.readAsDataURL(file);
                }
              }
            });
        /* CROP IMAGE AFTER UPLOAD */	
        function cropImage() {
            var image = document.querySelector('#imageprev');
            var minAspectRatio = 0.5;
            var maxAspectRatio = 1.5;
          
            var cropper = new Cropper(image, {
          aspectRatio: 11 /12,  
          minCropBoxWidth: 220,
          minCropBoxHeight: 240,
        
              ready: function () {
                var cropper = this.cropper;
                var containerData = cropper.getContainerData();
                var cropBoxData = cropper.getCropBoxData();
                var aspectRatio = cropBoxData.width / cropBoxData.height;
                //var aspectRatio = 4 / 3;
                var newCropBoxWidth;
            cropper.setDragMode("move");
                if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
                  newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);
        
                  cropper.setCropBoxData({
                    left: (containerData.width - newCropBoxWidth) / 2,
                    width: newCropBoxWidth
                  });
                }
              },
        
              cropmove: function () {
                var cropper = this.cropper;
                var cropBoxData = cropper.getCropBoxData();
                var aspectRatio = cropBoxData.width / cropBoxData.height;
        
                if (aspectRatio < minAspectRatio) {
                  cropper.setCropBoxData({
                    width: cropBoxData.height * minAspectRatio
                  });
                } else if (aspectRatio > maxAspectRatio) {
                  cropper.setCropBoxData({
                    width: cropBoxData.height * maxAspectRatio
                  });
                }
              },
          
          
            });
          
          $("#scaleY").click(function(){ 
          var Yscale = cropper.imageData.scaleY;
          if(Yscale==1){ cropper.scaleY(-1); } else {cropper.scaleY(1);};
          });
          
          $("#scaleX").click( function(){ 
          var Xscale = cropper.imageData.scaleX;
          if(Xscale==1){ cropper.scaleX(-1); } else {cropper.scaleX(1);};
          });
          
          $("#rotateR").click(function(){ cropper.rotate(45); });
          $("#rotateL").click(function(){ cropper.rotate(-45); });
          $("#reset").click(function(){ cropper.reset(); });
          
          $("#saveAvatar").click(function(){
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var avatar = document.getElementById('avatarimage');
            var $alert = $('.alert');
                canvas = cropper.getCroppedCanvas({
                  width: 220,
                  height: 240,
                });
                
                $progress.show();
                $alert.removeClass('alert-success alert-warning');
                canvas.toBlob(function (blob) {
                  
                });
               
              avatar.src = canvas.toDataURL();
              $("#myModal").modal('hide');  
          });
        };
    </script>

    @stop