@extends('layouts.app2')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">
  .margen{ 
    
    padding: 5%;
    background-color: rgb(215, 214, 214);

  }



  #submit{
        background-color: #123f6f;
        color: white;
        width: 210px;
        height: 50px;
        border-radius:8px;
        font-family: 'gotham-bold';
        font-size: 10pt;
      }

  #regresar{
    padding-top: 30px;
    font-size: 14pt;
    color: #123f6f;
    font-family: 'din-pro-medium'; 
    text-decoration: underline #123f6f;
  }
  #spTotal{

    margin: 13px;
  }
  .descripcion{
    margin-top: 30%;
    margin-bottom: 60%;
  }
  #nombre{
    font-size: 14pt;
    text-align: center;
    margin: 30px;
  }

  .contenedor{
    margin-left: 30px;
    margin-right: 40px;
  }
  
  #txt_campo_1{ font-family: verdana; 
  
    
    border-color: #123f6f;
    width: 80%;
    }
  #txt_campo_2{ font-family: verdana; 

  
    border-color: #123f6f;
    width: 80%;
    }

  input[type="number"] {
   width: 80%;
   border-color: #123f6f;
  }
  .precio {
      color: #123f6f;
      font-size: 27px;
      font-family: 'din-pro-light';
    }  

      label, .lazul{
        color: #123f6f;
        font-size: 13pt;
        font-family: 'din-pro-light';
      }
        input[type]:focus{
          border-color: #123f6f;
          color:#123f6f;
          font-size: 18pt;
        font-family: 'din-pro-light';
        } 
 #descripcion{
  color:#123f6f;
  font-family: 'din-pro-light';
  font-size: 12pt;
 }         

</style> 
@endsection

@section('content')
    
{{--Personalizacion --}}
<div class="container">


<div class="row contenedor">
    <div class="col-md-6">
        @foreach ($data as $dat)
        @for ($i = 0; $i < 1; $i++)
            
    
          
                <div class="card margen " style="width: 100%; " id="card">
                    <img src="{{$dat['images'][$i]['display_sizes'][0]['uri']}}" alt="" class="img-fluid">
                  
                </div>
          
              @endfor 
        @endforeach
   </div>
<div class="col-md-3">
    @foreach ($data as $dat)
    @for ($i = 0; $i < 1; $i++)
    @if (Auth::check())
    <form action="{{ url('usuario/cgetty') }}" method="post" >
        @csrf
    @else 
    <form >
    @endif   
     <div class="form-group">
        <label for="exampleFormControlSelect1">Impreso sobre:</label>
          <select  name="materiales"  id="txt_campo_1"  class="form-control btn-cambio" required>
            <option value="">Seleccione</option>
         @foreach ($materiales as $material)
       
         @if($material->material_nombre  != 'Foamboard')
            <option value="{{ $material->material_nombre }}">{{ $material->material_nombre }}</option>
            @else 
            
            @endif
          @endforeach
     </select>  
    </div>   
     
    <div class="form-group">
        <label for="">Dimensiones</label>
        <select name="medidas"  id="txt_campo_2"  class="form-control btn-cambio" required>
             <option value="">Seleccione</option>
         @foreach ($medidas as $medida)
           
            <option value="{{ $medida->medidas_x . ' X ' . $medida->medidas_y }}">{{ $medida->medidas_x . ' X ' . $medida->medidas_y }} cm.</option>
          
         @endforeach
     </select>  
    </div>

    <div class="form-group ">
        <label for="">Cantidad:</label>
      <input type="number" class="form-control btn-cambio cantidad" name="cantidad" value="1">
     
    </div>
      
    <p class="precio">  MX $<span id="f315-div">0</span><p>

     <input type="hidden" name="url" value="{{$dat['images'][$i]['display_sizes'][0]['uri']}}" id="url">   

     <input type="hidden" name="imagen" value="{{ $dat['images'][$i]['id']}}"> 
     <br>
   
     @if (Auth::check())
  
     <button class="btn  btn-info  btn-agregar " type="submit" id="submit" >AGREGAR AL CARRITO </button> 
    </form>
         @else
            <button class="btn  btn-info  btn-agregar " id="agregar-carrito" >AGREGAR AL CARRITO</button> 
 @endif
      
     <br>
     <a id="regresar" type="button" onclick="history.back()">VOLVER A LA GALERÍA</a>   
     <br>
   
    
    </form>
    @endfor 
    @endforeach
</div>
<div class="col-md-3">
  <div class="descripcion">
      <p id="descripcion"></p>
  </div>
  <div>
    <img src="{{asset('front/iconos_pago.jpeg')}}" alt=""  class="img-fluid" id="imgpago">
 </div>
</div>
   </div>

@endsection

@section('js')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   
<script>
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  
      $(".btn-cambio").change(function(e){
  
          e.preventDefault();
  
          var name = $("select[name=materiales]").val();
          var mun = $("select[name=medidas]").val();
          var cant = $("input[name=cantidad]").val();
         
          console.log(name);
          console.log(mun);
          console.log(cant);
  
          $.ajax({
             type:'POST',
             url:"{{ route('gprecios.post') }}",
             data:{name:name, mun:mun, cant:cant, _token: '{{csrf_token()}}'},
             success: function (response) {
                  $('#f315-div').html(response);
              },
          });
         
  
      });

  </script>

  <script>

$(".btn-cambio").change(function(e){
    
    e.preventDefault();

    const mater = @JSON($materiales);
   
   
    var material = $("select[name=materiales]").val();

    document.getElementById("descripcion").innerHTML = "";

    if (material == 'Canvas') {
      document.getElementById("descripcion").innerHTML = mater[0].material_descripcion;
    }
    if (material == 'Aluminio') {
      document.getElementById("descripcion").innerHTML = mater[1].material_descripcion;
    }
    if (material == 'Acrílico') {
      document.getElementById("descripcion").innerHTML = mater[2].material_descripcion;
    }
    if (material == 'Trovicel') {
      document.getElementById("descripcion").innerHTML = mater[3].material_descripcion;
    }
    if (material == 'Poster') {
      document.getElementById("descripcion").innerHTML = mater[4].material_descripcion;
    }

});
  </script>

    <script>

var aMaterial = [];
    aMedidas = [];
    aCantidad = [];
    aPrecio = [];
    aTotal = [];
    aUrl = [];  

  if (localStorage.getItem('material_cuadros') != null) {
   console.log('entro');
    aMaterial = JSON.parse(localStorage.getItem('material_cuadros'));
    aMedidas =  JSON.parse(localStorage.getItem('medidas_cuadros'));
    aCantidad = JSON.parse(localStorage.getItem('cantidad_cuadros'));
    aPrecio = JSON.parse(localStorage.getItem('precio_cuadros'));
    aTotal = JSON.parse(localStorage.getItem('total_cuadros'));
    aUrl = JSON.parse(localStorage.getItem('url_cuadros')); 

    
  }  


 var elementoBotonRegistrar = document.querySelector('#agregar-carrito');

 elementoBotonRegistrar.addEventListener('click', registrarPedido);

 function registrarPedido(){

  var material = document.querySelector("#txt_campo_1").value;
      medidas =  document.querySelector("#txt_campo_2").value;
      cantidad = document.querySelector(".cantidad").value;
      precio = document.querySelector('#precio').value;
      total = document.querySelector('#total').value;
      url = document.querySelector('#url').value;

     aMaterial.push(material);
     aMedidas.push(medidas);
     aCantidad.push(cantidad);
     aPrecio.push(precio);
     aTotal.push(total)
     aUrl.push(url);

     localStorage.setItem('material_cuadros', JSON.stringify( aMaterial ));
     localStorage.setItem('medidas_cuadros', JSON.stringify( aMedidas ));   
     localStorage.setItem('cantidad_cuadros', JSON.stringify( aCantidad ));   
     localStorage.setItem('precio_cuadros', JSON.stringify( aPrecio ));   
     localStorage.setItem('total_cuadros', JSON.stringify( aTotal ));   
     localStorage.setItem('url_cuadros', JSON.stringify( aUrl ));    


     Swal.fire(
        '¡Agregado al carrito! ',
        '',
        'success'
        ) 
  

 }





 
    </script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Agregado al carrito! ',
        '',
        'success'
        )

  
</script>
    
@endif
 

@endsection