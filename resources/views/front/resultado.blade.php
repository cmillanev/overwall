@extends('layouts.app2') @section('css')
<link href="{{asset('css/css/pagination.css')}}" rel="stylesheet" type="text/css">
<style>
    #icono{
        width: 30px;
      } 
      #icono1{
        width: 35px;
      } 
      #cuadro{
        width: 100%;
       
      }
      .container-fluid{
        padding-left: 40px;
        padding-right: 30px;
        
      }
      #card{
        padding: 10px;
        margin-left: 2px;
        margin-bottom: 25px;
        background-color: #E3E3E2;
        
      }
    #lin{
       content: '';
      position: absolute;
      left: -30vw;
      right: -30vw;
      width: 40vw;
      height: 3px;
      top: 0.6em;
      background-color: #126EB5;
    }
    .bus{
      margin-top: 5px;
      margin-bottom: 5px;
      font-family: 'Gotham-Bold';
      font-size: 23px;
      color: #126EB5;
      padding: 5px;
    }
    #cuadro{
      padding: 1px;
     margin-top: 2px;
     margin-bottom: 2px;
    
      background-color: #E3E3E2;
    }
    
    #hr{
      border: 2px solid #123F6F;
     
    }
    .categoria{
      font-size: 24pt;
      color: #126EB5;
      text-transform: uppercase;
      font-family: 'gotham-bold';
    }
    
    .contenedor{
      background: radial-gradient( #E3E3E2, white);
    }
    .co{
      width: 500px;
      height: 900px;
    }
    
    .vista{
      font-family: 'din-pro-regular';
      font-size: 17px;
      width: auto;
      height: 28px;
      border-radius: 13px;
      background-color: white;
      color: #123F6F;
      border: 0;
    
      margin-bottom: 5px;
      margin-left: 15px;
      margin-top: 6px;
    }
    #fondo{
      width: 320px;
      height: 900px;
      direction: rtl;
      overflow: auto;
      font-size: 8pt;
    }
    #fondo::-webkit-scrollbar{
      
      background: #E3E3E2; 
    }
    #fondo::-webkit-scrollbar-thumb{
      background:gray;
      border-radius: 10px;
    
    }
    #fondo::-webkit-scrollbar:vertical {
      width:10px;
    
    }
    #fondo::-webkit-scrollbar-track {
        border-radius: 10px;  
    }    
    
    .list-group-item {
      background-color: rgb(244, 244, 244);
      
    }
    
    .ca{
      font-family: 'din-pro-regular';
      font-size: 16pt;
      color: #123F6F;
    }
    
    #detalla{
    
    text-align: center;
    padding-top: 8px;
    color: #123F6F;
    font-family: 'gotham-bold';
    }
    
    .paginador{
      text-align: center;
    }
    
    .input-icono input{
        background-image: url('front/lupa.png');
        background-repeat: no-repeat;
        background-position: right;
        background-size: 45px;
        display: flex;
        align-items: center;
        border-radius: 15px;
        border-color: grey;
        height: 70px; 
        padding-left: 20px;
        font-size: 24pt;  
       
        
      }


   
</style>
@endsection 
@section('content')

<div class="container-fluid" >
    <div class="row">
        <div class="col-md-3 ">
            <div id="fondo">

                @foreach ($categorias as $categoria)
                <ul class="list-group list-group-flush">
                    <li class="list-group-item "><a style="margin-left: 4vw" class="ca" href="resultado?frase={{ $categoria->categoria_palabras}}&page=1">  {{ $categoria->categoria_nombre }}<img src=" {{ $categoria->categoria_url }}" alt="" id="icono"></a></li>
                </ul>

                @endforeach
            </div>
            <br> @if ($etiquetas != null)
            <div style="background-color: #E3E3E2" class="">

                <h5 id="detalla">DETALLA TU BÚSQUEDA</h5>

                @foreach ($etiquetas as $tag)
                <a href="resultado?frase={{ $tag->tag_palabras}}&page=1"><button class="vista">{{$tag->tag_nombre}}</button></a> @endforeach

                <br>

            </div>
            @else 
            @endif
            <img src="{{ asset('front/cuadro azul.png') }}" alt="" id="cuadro">

        </div>

        <div class="col-md-9 ">

            <div class="row">
                <div class="col-md-4">
                    <form action="{{ route('resultado') }}" method="GET">
                        <div class="form-group input-icono">
                            <input class="form-control" type="text" name="frase" value="{{$phrase}}">
                            <input type="hidden" name="page" value="1">
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <hr id="hr">
                </div>
                <div class="col-md-2">
                    <img src=" {{ $categoria->categoria_url_fondo }}" alt="" id="icono1">
                </div>
            </div>

            <div class="row">
                @foreach ($data as $dat) @for ($i = 0; $i
                < $count; $i++) <div class="col">
                    <form action="{{ route('vistaprevia') }}" method="GET">
                        <div class="card" style="width: 16rem; height: 13rem;" id="card">

                            <input type="hidden" name="id" value="{{ $dat['images'][$i]['id']}}">
                            <button style="border: 0;" type="submit"><img  style="width: 14rem; height: 11rem; " src="{{$dat['images'][$i]['display_sizes'][0]['uri']}}" alt="" id="cuadro"></button>
                        </div>
                    </form>

            </div>

            @endfor 
            @endforeach

        </div>

    </div>


    <div class="col-md-4">

    </div>
    <div class="col-md-4">
        <div class="d-flex justify-content-center ">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    @if ($count < 30) <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=1">1</a></li>
                        @else

                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=1">1</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=2">2</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=3">3</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=4">4</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=5">5</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=6">6</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=7">7</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=8">8</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=9">9</a></li>
                        <li class="page-item"><a class="page-link" href="resultado?frase={{$phrase}}&page=10">10</a></li>
                        @endif
                </ul>
            </nav>
        </div>
    </div>

</div>


@endsection
 @section('js') 
 
 <script src="{{asset('js/js/detectmobilebrowser.js')}}"></script>
 <script>
   
   if($.browser.mobile == true){
    $("#pc").hide();
        $("#celular").show();
       
 
          console.log('oculta imagenes');
    }
 </script>
 @endsection