@extends('layouts.app2')
 @section('css') 
 <meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
  
    .columna{
        color: #123F6F;
       }
    
     
       #pagar{
         font-weight: bold;
         display:block; 
         position:right; 
         margin-top: 30px;
         margin-left: 90%;
         background: #123F6F;
         border: 2px solid #000000;
       
       }
       .total{
        font-weight: bold;
         color: #123F6F;
         font-size: 15pt;
         display:block; 
         float:right; 
    
       }
       .borde{
         border: 0;
       }
      .col-form-label-izquierda{
        margin-top: 5px;
        text-align: end;
        color: #123F6F;
      }
    
   
      .modalimg img{
      position:relative;
       top: 30px;
       left:250px;
      width: 300px;
    }
    .layouts img{
      padding: 3px;
      width: 30px;
      display: block;
      margin-left: auto;
      margin-right: auto;
    }

    .table td, .table th {
      
    padding: 3px;
    vertical-align: top;
    border-top: 1px  transparent;

}
.home table{
  margin-left:5px;
}
   
</style>
@endsection
 @section('content')
<div class="container">
    <img src="{{ asset('front/carrito con texto.png') }}" class="img-fluid ml-5" height="100px" width="258px">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div><br />
@endif
<div class="table-responsive">
    <table id="lista-carrito" class="table table-striped ">
        <thead>
            <tr>
                <th class="columna" scope="col">Imagen</th>
                <th class="columna" scope="col">Nombre</th>
                <th class="columna" scope="col">Precio</th>
                <th class="columna" scope="col">Cantidad</th>
                <th class="columna" scope="col">Total</th>
                <th class="columna" scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody id="lista-carrito">

        </tbody>
        <tbody>
            @auth @foreach ($cgetties as $cgetty)
            <tr>
                <td><img style="width: 100px;" src="{{ $cgetty->url }}" alt=""></td>
                <td>{{ $cgetty->imagen }}</td>
                <td>${{ number_format($cgetty->precio, 2) }}</td>
                <td>{{ $cgetty->cantidad }}</td>
                <td>${{ number_format($cgetty->total, 2) }}</td>
                <td>
                    <form id="formulario-borrar" action="{{ route('cgetty.destroy', $cgetty )}}" method="POST" style="display:inline-block;">
                        @method('DELETE') @csrf

                        <button type="submit" class="" style="border:none;"><i class="submit fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @php $sumaf += $cgetty->total; 
            @endphp 
            @endforeach 
            @foreach ($frames as $frame)
            <tr>
                <td><img style="width: 100px;" src="{{ $frame->url_recorte }}" alt=""></td>
                <td>Tu wall {{$frame->material}}, {{$frame->medidas}}</td>
                <td>${{ number_format($frame->precio, 2) }}</td>
                <td>{{ $frame->cantidad }}</td>
                <td>$ {{ number_format($frame->total, 2)}}</td>
                <td>
                    <form id="formulario-borrar" action="{{ route('tuwall.destroy', $frame )}}" method="POST" style="display:inline-block;">
                        @method('DELETE') @csrf
                        <button type="submit" class="" style="border:none;"><i class="submit fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @php $sumaf += $frame->total; 
            @endphp 
            @endforeach
            @foreach ($layouts as $layout)
            <tr>
                <td >
                  @if ($layout->layout == 'Room')
                  <div class="row layouts">
                    <div class="col-2">
                      <img style="width: 60px; height:auto;" src="{{ $layout->url_recorte }}" alt="">
                    </div>
                  </div>
                
              @endif
                  @if ($layout->layout == 'Office')
                      <div class="row layouts">
                        <div class="col-1">
                          <img  src="{{ $layout->url_recorte }}" alt="">
                        </div>
                        <div class="col-1">
                          <img  src="{{ $layout->url_recorte_2 }}" alt="">
                        </div>
                      </div>
                      <div class="row layouts">
                        <div class="col-1">
                          <img src="{{ $layout->url_recorte_3 }}" alt="">
                        </div>
                        <div class="col-1">
                          <img src="{{ $layout->url_recorte_4 }}" alt="">
                        </div>
                      </div>
                  @endif
                  @if ($layout->layout == 'Fun')
                  <div class="row layouts">
                    <div class="col-1">
                      <img  src="{{ $layout->url_recorte }}" alt="">
                    </div>
                    <div class="col-1">
                      <img  src="{{ $layout->url_recorte_2 }}" alt="">
                    </div>
                    <div class="col-1">
                      <img  src="{{ $layout->url_recorte_3 }}" alt="">
                    </div>
                  </div>
                  <div class="row layouts">
                    <div class="col-1">
                      <img src="{{ $layout->url_recorte_4 }}" alt="">
                    </div>
                    <div class="col-1">
                      <img src="{{ $layout->url_recorte_5 }}" alt="">
                    </div>
                    <div class="col-1">
                      <img src="{{ $layout->url_recorte_6 }}" alt="">
                    </div>
                  </div>
              @endif
              @if ($layout->layout == 'Home')
              <div class="row" class="home">
               <table>
                 <tr>
                   <td rowspan="2">  <img style="height: 60px; width: 30px; margin-left: 13px;" src="{{ $layout->url_recorte }}" alt=""></td>
                   <td>  <img style="width: 25px;" src="{{ $layout->url_recorte_2 }}" alt=""></td>
                   <td rowspan="2">  <img style="height: 60px; width: 30px;" src="{{ $layout->url_recorte_4 }}" alt=""></td>
                 </tr>
                 <tr>
                   <td>  <img style="width: 25px;" src="{{ $layout->url_recorte_3 }}" alt=""></td>
                 </tr>
               </table>
              </div>
           
          @endif
          @if ($layout->layout == 'Flag')
          <div class="row" class="home">
           <table>
             <tr>
               <td>  <img style="height: 60px; width: 30px; margin-left: 13px;" src="{{ $layout->url_recorte }}" alt=""></td>
               <td>  <img style="height: 60px; width: 30px;" src="{{ $layout->url_recorte_2 }}" alt=""></td>
               <td>  <img style="height: 60px; width: 30px;" src="{{ $layout->url_recorte_3 }}" alt=""></td>
             </tr>
             
           </table>
          </div>
       
      @endif
      @if ($layout->layout == 'Space')
      <div class="row" class="home">
       <table>
         <tr>
           <td colspan="2" rowspan="2">  <img style="height: 60px; width: 60px; margin-left: 13px;" src="{{ $layout->url_recorte }}" alt=""></td>
           <td>  <img style="width: 25px;" src="{{ $layout->url_recorte_2 }}" alt=""></td>
          
         <tr>
          <td>  <img style="width: 25px;" src="{{ $layout->url_recorte_3 }}" alt=""></td>
         </tr>
       </table>
      </div>
   
  @endif
                  </td>
                <td>{{$layout->layout}}, {{$layout->material}}</td>
                <td>${{ number_format($layout->precio, 2) }}</td>
                <td>{{ $layout->cantidad }}</td>
                <td>$ {{ number_format($layout->total, 2)}}</td>
                <td>
                    <form id="formulario-borrar" action="{{ route('tuwall.destroy', $layout )}}" method="POST" style="display:inline-block;">
                        @method('DELETE') @csrf
                        <input type="hidden" name="control" value="{{$layout->layout}}">
                        <button type="submit" class="" style="border:none;"><i class="submit fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @php $sumaf += $layout->total; 
            @endphp 
            @endforeach
             @endauth
        </tbody>
    </table>
  </div>
    @auth
    <form action="{{route('cupon.post')}}" method="POST">
      @csrf
    <input type="text" name="cupon" size="17" placeholder="CÓDIGO DE CUPÓN">
    <input type="submit" class="btn1 btn-outline-secondary" value="APLICAR CUPÓN">
  </form>
    <p class="total">TOTAL:

        <span>${{ number_format($sumaf, 2) }} MX</span>
       
    </p>
    @endauth @auth

    <form action="https://mexpago.com/app/pagoOnline" method="post"> <input type="hidden" name="monto" value="{{ $sumaf }} ">
        <input type="hidden" name="noTransaccion" value="{{$transaccion}}">
        <input type="hidden" name="llave" value="eyJsbGF2ZSI6Ijc5NzU2YmQ0LTdjMzctNDRjNC04ZDQ2LTJiNjU2NGQ2OTE1OSIsImlkT25saW5lIjoiNzY5ZjUxODgtNzg3Zi00YTRiLThmN2ItMjVkN2M1MGYzNzI2In0=">
        <input type="hidden" name="fecha" value="{{$hoy}} {{$hora}}">
        <input type="hidden" name="articulos" value='{{ $productos }}'>
        <input type="hidden" name="precargaDatos" value='{{ $nombre }}'>
        <input type="hidden" name="enviarCorreo" value="false"> <input type="hidden" name="infoComercio" value="true"> <input type="hidden" name="lenguaje" value="es">

        <button type="submit" id="pagar" class="bt btn btn-primary btn-sm">PAGAR</button>
    </form>

    @endauth
    @guest
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
  Inicia sesión
  </button>
   @endguest
    <!-- Modal -->
    <div class="container">
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              
             
             
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
       
        </button>
    
              <div class="modal-body">
                <img src="{{'front/registro_boton.png'}}" alt="">
                <div class="container b-modal">
                   <h5 class="menu">¿YA TIENES UNA CUENTA? <a href="{{route('login')}}">ENTRAR</a></h5>

                   <form action="{{ route('registro.store') }}" method="post">
                    @csrf
                   <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">USUARIO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="usuario" class="form-control" value="">
                    </div>
                 
                  
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">NOMBRE Y APELLIDO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="name" id="name" class="form-control" value="" required>
                    </div>
                 
                 
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">EMAIL DE CONTACTO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="email" class="form-control" value="">
                    </div>
                  
                  
                    <label for="inputPassword" class="col-sm-4 col-form-label-izquierda texcol">PASSWORD:</label>
                    <div class="col-sm-7">
                      <input type="password" name="password" class="form-control" id="inputPassword" >
                    </div>
                  </div>
                
                  
                  <!--direccion de contacto-->

          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DIRECCIÓN DE CONTACTO:</label>
            <label for="calle" class="col-sm-1 col-form-label texcol">Calle:</label>
            <div class="col-sm-6">
              <input type="text" name="c_calle"  class="form-control" value="" id="c_calle">
            </div>
           
             <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Colonia:</label>
            <div class="col-sm-7">
              <input type="text" name="c_colonia" id="c_colonia"  class="form-control" value="" >
            </div>
          <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Número Exterior:</label>
            <div class="col-sm-3">
              <input type="text" name="c_numExt" id="c_numExt"  class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >Interior:</label>
            <div class="col-sm-3">
              <input type="text" name="c_numInt" id="c_numInt"  class="form-control"  value="">
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Municipio/Delegación:</label>
            <div class="col-sm-7">
              <input type="text" name="c_mun" id="c_mun"  class="form-control" value="" >
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Ciudad:</label>
            <div class="col-sm-3">
              <input type="text" name="c_ciudad" id="c_ciudad" class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol">Estado:</label>
            <div class="col-sm-3">
               <select class="form-control" name="c_estado" id="c_estado" name="Estado"   >
                        <option value="CDMX">Ciudad de México</option>
                        <option value="Aguascalientes">Aguascalientes</option>
                        <option value="Baja California">Baja California</option>
                        <option value="Baja California Sur">Baja California Sur</option>
                        <option value="Campeche">Campeche</option>
                        <option value="Chiapas">Chiapas</option>
                        <option value="Chihuahua">Chihuahua</option>
                        <option value="Coahuila">Coahuila</option>
                        <option value="Colima">Colima</option>
                        <option value="Durango">Durango</option>
                        <option value="Estado de México">Estado de México</option>
                        <option value="Guanajuato">Guanajuato</option>
                        <option value="Guerrero">Guerrero</option>
                        <option value="Hidalgo">Hidalgo</option>
                        <option value="Jalisco">Jalisco</option>
                        <option value="Michoacán">Michoacán</option>
                        <option value="Morelos">Morelos</option>
                        <option value="Nayarit">Nayarit</option>
                        <option value="Nuevo León">Nuevo León</option>
                        <option value="Oaxaca">Oaxaca</option>
                        <option value="Puebla">Puebla</option>
                        <option value="Querétaro">Querétaro</option>
                        <option value="Quintana Roo">Quintana Roo</option>
                        <option value="San Luis Potosí">San Luis Potosí</option>
                        <option value="Sinaloa">Sinaloa</option>
                        <option value="Sonora">Sonora</option>
                        <option value="Tabasco">Tabasco</option>
                        <option value="Tamaulipas">Tamaulipas</option>
                        <option value="Tlaxcala">Tlaxcala</option>
                        <option value="Veracruz">Veracruz</option>
                        <option value="Yucatán">Yucatán</option>
                        <option value="Zacatecas">Zacatecas</option>
                       </select>
                      </div>
              <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">País:</label>
            <div class="col-sm-3">
              <input type="text" name="c_pais" id="c_pais"  class="form-control" value="México"  >
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >CP:</label>
            <div class="col-sm-3">
              <input type="text" name="c_cp" id="c_cp"  class="form-control"  value="">
            </div>
            </div>

            <!--direccion de envio-->
       <br>
      <div class="form-group row ">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DIRECCIÓN DE ENVÍO:</label>
           
            <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="seleccion" id="seleccion1"  onclick="myFunction()">
                    <label class="form-check-label texcol" for="seleccion1">Usar datos de contacto</label>
                </div>
                <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="seleccion" id="seleccion2" onclick="myFunctionLimpia()">
                    <label class="form-check-label texcol" for="selecion2">Otra información</label>
                </div>
              

            <label for="calle" class="col-sm-4 col-form-label-izquierda texcol">Calle:</label>
            <div class="col-sm-7">
              <input type="text" name="e_calle" id="e_calle"  class="form-control" value="" >
            </div>
           
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Colonia:</label>
            <div class="col-sm-7">
              <input type="text" name="e_colonia" id="e_colonia"  class="form-control" value="" >
            </div>
          <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Númmero Exterior:</label>
            <div class="col-sm-3">
              <input type="text" name="e_numExt" id="e_numExt"  class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >Interior:</label>
            <div class="col-sm-3">
              <input type="text" name="e_numInt" id="e_numInt"  class="form-control"  value="">
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Municipio/Delegación:</label>
            <div class="col-sm-7">
              <input type="text" name="e_mun" id="e_mun" class="form-control" value="" >
            </div>

            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Ciudad:</label>
            <div class="col-sm-3">
              <input type="text" name="e_ciudad" id="e_ciudad" class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol">Estado:</label>
            <div class="col-sm-3">
               <select class="form-control" name="e_estado"   id="e_estado">
                       
                        <option value="Aguascalientes">Aguascalientes</option>
                        <option value="Baja California">Baja California</option>
                        <option value="Baja California Sur">Baja California Sur</option>
                        <option value="Campeche">Campeche</option>
                        <option value="Chiapas">Chiapas</option>
                        <option value="Chihuahua">Chihuahua</option>
                        <option value="CDMX">Ciudad de México</option>
                        <option value="Coahuila">Coahuila</option>
                        <option value="Colima">Colima</option>
                        <option value="Durango">Durango</option>
                        <option value="Estado de México">Estado de México</option>
                        <option value="Guanajuato">Guanajuato</option>
                        <option value="Guerrero">Guerrero</option>
                        <option value="Hidalgo">Hidalgo</option>
                        <option value="Jalisco">Jalisco</option>
                        <option value="Michoacán">Michoacán</option>
                        <option value="Morelos">Morelos</option>
                        <option value="Nayarit">Nayarit</option>
                        <option value="Nuevo León">Nuevo León</option>
                        <option value="Oaxaca">Oaxaca</option>
                        <option value="Puebla">Puebla</option>
                        <option value="Querétaro">Querétaro</option>
                        <option value="Quintana Roo">Quintana Roo</option>
                        <option value="San Luis Potosí">San Luis Potosí</option>
                        <option value="Sinaloa">Sinaloa</option>
                        <option value="Sonora">Sonora</option>
                        <option value="Tabasco">Tabasco</option>
                        <option value="Tamaulipas">Tamaulipas</option>
                        <option value="Tlaxcala">Tlaxcala</option>
                        <option value="Veracruz">Veracruz</option>
                        <option value="Yucatán">Yucatán</option>
                        <option value="Zacatecas">Zacatecas</option>
                       </select>
                      </div>
              <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">País:</label>
            <div class="col-sm-3">
              <input type="text" name="e_pais"  class="form-control" value="México"  readonly="readonly" >
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >CP:</label>
            <div class="col-sm-3">
              <input type="text" name="e_cp" id="e_cp"  class="form-control"  value="">
            </div>
            <label for="calle" class="col-sm-4 col-form-label-izquierda texcol">IMPORTANTE:</label>
            <div class="col-sm-7">
              <label  class=" col-form-label texcol">Si tu dirección de envío es fuera de México, ponte en contacto con nosotros antes de tu compra.</label>
            </div>
          </div>
         
<!--destinatario-->
       <br>
      <div class="form-group row col-md-12">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DESTINATARIO:</label>
           
            <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input texcol" type="radio" name="option" id="option1"  onclick="myFunction2()">
                    <label class="form-check-label texcol" for="option1">Usar datos de contacto</label>
                </div>
           
            
                <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="option" id="option2" onclick="myFunctionLimpia2()">
                    <label class="form-check-label texcol" for="option2">Otra información</label>
                </div>
              

            <label for="nombre" class="col-sm-4 col-form-label-izquierda texcol">Nombre:</label>
            <div class="col-sm-8">
              <input type="text" name="d_nombre" id="d_nombre" class="form-control" value="" >
              <input type="hidden" name="material_cuadros" id="material_cuadros">
              <input type="hidden" name="medidas_cuadros" id="medidas_cuadros">
              <input type="hidden" name="cantidad_cuadros" id="cantidad_cuadros">
              <input type="hidden" name="precio_cuadros" id="precio_cuadros">
              <input type="hidden" name="total_cuadros" id="total_cuadros">
              <input type="hidden" name="url_cuadros" id="url_cuadros">
            </div>
            
          </div>
          <div class="col-md-12" >
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" id="sub" class="btn btn-primary">Guardar</button>
        </div>
        </form> 
              </div>
               
                
           
            </div>
         

   <!-- FIN Modal -->
</div>

@endsection 
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/js/all.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> 
<script>

  

  
  
  $( document ).ready(function() {
    document.getElementById('material_cuadros').value = JSON.parse(localStorage.getItem('material_cuadros'));
    document.getElementById('medidas_cuadros').value = JSON.parse(localStorage.getItem('medidas_cuadros'));
    document.getElementById('cantidad_cuadros').value = JSON.parse(localStorage.getItem('cantidad_cuadros'));
    document.getElementById('precio_cuadros').value = JSON.parse(localStorage.getItem('precio_cuadros'));
    document.getElementById('total_cuadros').value = JSON.parse(localStorage.getItem('total_cuadros'));
    document.getElementById('url_cuadros').value = JSON.parse(localStorage.getItem('url_cuadros'));
  });
  
   
  </script>
<script>
    llenartabla();
    
    function llenartabla(){
       var tbody = document.querySelector('#lista-carrito tbody');
    
    //LIMPIA EL TBODY
       tbody.innerHTML = '';
    
       var aMaterial = JSON.parse(localStorage.getItem('material_cuadros'));
           aMedidas = JSON.parse(localStorage.getItem('medidas_cuadros'));
           aCantidad = JSON.parse(localStorage.getItem('cantidad_cuadros'));
           aPrecio = JSON.parse(localStorage.getItem('precio_cuadros'));
           aTotal = JSON.parse(localStorage.getItem('total_cuadros'));
           aUrl = JSON.parse(localStorage.getItem('url_cuadros'));
    
          var nCantidadCauadros = aMaterial.length;
    
          for (let i = 0; i < nCantidadCauadros; i++) {
           
           var fila = document.createElement('tr');
    
           var celdaMaterial = document.createElement('td'),
               celdaMedidas = document.createElement('td'),
               celdaCantidad = document.createElement('td'),
               celdaPrecio = document.createElement('td'),
               celdaTotal = document.createElement('td'),
               celdaUrl = document.createElement('td');
               celdaBtn = document.createElement('td');
    
    
    
               
              
              //Formato a las monedas
               const formatterDolar = new Intl.NumberFormat('en-US', {
                      style: 'currency',
                      currency: 'USD'
                    })
                  
               
    
           var nodoTextoMaterial = document.createTextNode(aMaterial[i]);  
               nodoTextoMedidas = document.createTextNode(aMedidas[i]);  
               nodoTextoCantidad = document.createTextNode(aCantidad[i]);  
               nodoTextoPrecio = document.createTextNode(formatterDolar.format(aPrecio[i]));  
               nodoTextoTotal = document.createTextNode(formatterDolar.format(aTotal[i]));  
               nodoTextoUrl = document.createTextNode(aUrl[i]);  
    
    
           var x = document.createElement('img');
              x.setAttribute("src", aUrl[i]);
              x.setAttribute("width", "152");
              x.setAttribute("height", "114");
              x.setAttribute("alt", "Imagen");
           var btn = document.createElement('INPUT');
                    btn.setAttribute("type", "button");
                    btn.setAttribute("value", "Eliminar");
                    btn.setAttribute("class", "limpiar");

           var getty = "Imagen Getty";   
             
           
            
            celdaMaterial.appendChild(nodoTextoMaterial);
            celdaMedidas.appendChild(nodoTextoMedidas);
            celdaPrecio.appendChild(nodoTextoPrecio);
            celdaCantidad.appendChild(nodoTextoCantidad);
            celdaTotal.appendChild(nodoTextoTotal);
            celdaUrl.appendChild(x);
            celdaBtn.appendChild(btn);
           
            
            fila.appendChild(celdaUrl);
            fila.appendChild(celdaMaterial);
            fila.appendChild(celdaPrecio);
            fila.appendChild(celdaCantidad);
            fila.appendChild(celdaTotal);
            fila.appendChild(celdaBtn);
            
            
    
            tbody.appendChild(fila);
    
    
          }
       
    }
</script>


<script>
  function myFunction() {
    var e_calle = document.getElementById("c_calle").value;
    var e_colonia = document.getElementById("c_colonia").value;
    var e_numExt = document.getElementById("c_numExt").value;
    var e_numInt = document.getElementById("c_numInt").value;
    var e_mun = document.getElementById("c_mun").value;
    var e_ciudad = document.getElementById("c_ciudad").value;
    var e_estado = document.getElementById("c_estado").value;
    var e_pais = document.getElementById("c_pais").value;
    
    $("#e_calle").val(e_calle);
    $("#e_colonia").val(e_colonia);
    $("#e_numExt").val(e_numExt);
    $("#e_numInt").val(e_numInt);
    $("#e_mun").val(e_mun);
    $("#e_ciudad").val(e_ciudad);
    $("#e_estado").val(e_estado);
    $("#e_pais").val(e_pais);
    
  }

  function myFunctionLimpia() {
    
    $("#e_calle").val("");
    $("#e_colonia").val("");
    $("#e_numExt").val("");
    $("#e_numInt").val("");
    $("#e_mun").val("");
    $("#e_ciudad").val("");
    $("#e_estado").val("");
    $("#e_pais").val("");
    
  }

  function myFunction2(){
    var d_nombre = document.getElementById('name').value;
    $("#d_nombre").val(d_nombre);
  }
  function myFunctionLimpia2() {
    
    $("#d_nombre").val("");
   
  }






  </script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if (session('actualizar') == 'ok')

<script>
  
  

  Swal.fire({
    position: 'center',
  icon: 'success',
  title: '¡Registro exitoso!',
  showConfirmButton: false,
  timer: 1500
}).then((result) => {
  // Reload the Page
  localStorage.clear();  
  location.reload();
 
});

  
 // location.reload(); 
</script>
    
@endif
@if (session('cupon') == 'ok')

<script>
  
  

  Swal.fire({
    position: 'center',
  icon: 'success',
  title: '¡Cupón aplicado!',
  showConfirmButton: false,
  timer: 2500
}).then((result) => {
  // Reload the Page
  localStorage.clear();  
  location.reload();
 
});

  
 // location.reload(); 
</script>
    
@endif
@if (session('cupon') == 'no')

<script>
  
  

  Swal.fire({
    position: 'center',
  icon: 'danger',
  title: '¡Cupón no valido!',
  showConfirmButton: false,
  timer: 2500
}).then((result) => {
  // Reload the Page
  localStorage.clear();  
  location.reload();
 
});

  
 // location.reload(); 
</script>
    
@endif
@if (session('login') == 'ok')

<script>
  
  

  Swal.fire({
    position: 'center',
  icon: 'success',
  title: '¡Bienvenido!',
  showConfirmButton: false,
  timer: 1500
}).then((result) => {
  // Reload the Page
  localStorage.clear();  
  location.reload();
 
});

  
 // location.reload(); 
</script>
    
@endif

<script>
     $(".limpiar").click(function(e){ 
            e.preventDefault();
            localStorage.clear(); 
            location.reload();
        });
</script>

@endsection