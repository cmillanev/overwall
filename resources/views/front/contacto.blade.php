@extends('layouts.app2')

@section('css')

<style>

 .asunto{
  text-align: center;
  background-color: #126EB5;
  margin: 15px;
  padding: 30px;
  height: 90%;

 }

 .asunto img{
   height: 106px;
   width: 88px;
   
   margin: 15px
 }

 .asunto p{
   color: white;
   font-family: 'din-pro-medium';
   font-size: 24pt;
 }

 .form{
   background-color: #E3E3E2;
   margin: 15px;
   padding: 30px;
   height: 90%;
   width: 100%;
 }
 h1{
    font-family: 'menu';
    text-align: center;
    color: #123f6f;
    font-size: 48pt;
    margin: 15px;
 }
 P{
   color: #123f6f;
   font-family: 'din-pro-medium';
   margin: 15px;
 }

 label{
  color: #123f6f;
  font-family: 'menu';
  font-size: 22pt;
 }



</style>
 
@endsection

@section('plugins.Sweetalert2', true)

@section('title', 'Contacto')

@section('content')


<div class="container">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-3">
      <h1>¡HOLA!</h1>
    </div>
    <div class="col-md-8">
      <p class="parrafo">No dudes en ponerte en contacto con nosotros. <br>
        Recuerda que en nuestra sección de preguntas frecuentes, algunas dudas  podrían quedar <br>
        resueltas, consulta las políticas de envío, devoluciones y aviso de privacidad. ¡Escríbenos!
      </p>
    </div>
  </div>
  <form role="form" action="{{ route('contacto.store') }}" method="POST" enctype="multipart/form-data" id="contacto">
    @csrf
  <div class="row">
    
      <div class="col-md-1"></div>
      <div class="col-md-3">
        <div class="row asunto">
          <div class="col-lg-12">
           <p>Asunto:</p>
          </div>
          <div class="col-lg-12">
            <input type="radio" class="form-check-input" id="" name="contacto_motivo" value="General">
            <img src="{{ asset('front/general.png') }}" class="img-fluid asunt">
          </div>
          <div class="col-lg-12">
            <input type="radio" class="form-check-input" id="" name="contacto_motivo" value="Talento">
            <img src="{{ asset('front/talento.png') }}" class="img-fluid asunt">
          </div>
          <div class="col-lg-12">
            <input type="radio" class="form-check-input" id="" name="contacto_motivo" value="Pedido">
            <img  src="{{ asset('front/consultar pedido.png') }}" class="img-fluid asunt">
          </div>
          
        </div>
      </div>
      <div class="col-md-8">
        <div class="row form">
          <div class="col-md-12">
           
              <div class="form-group row">
                <label for="nombre" class="col-sm-2 col-form-label">Nombre:</label>
                <div class="col-sm-10">
                  <input type="text"  class="form-control" name="contacto_nombre" id="nombre" value="" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="apellido" class="col-sm-2 col-form-label">Apellido:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="contacto_apellido" id="apellido" value="">
                </div>
              </div>
              <div class="form-group row">
                <label for="correo" class="col-sm-4 col-form-label">Correo Electrónico:</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" name="contacto_correo" id="correo" value="" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="orden" class="col-sm-4 col-form-label">Número de orden:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="contacto_orden" id="orden" value="">
                </div>
              </div>
              <div class="form-group row">
                <label for="exampleFormControlTextarea1" class="col-sm-4 col-form-label">Tu mensaje:</label>
                <div class="col-sm-8">
                  <textarea class="form-control" id="exampleFormControlTextarea1"  rows="5"  name="contacto_mensaje" required></textarea>
                </div>
              </div>
            
            <div class="row">
              <div class="iconos col-sm-8">
                <p style="font-size:12px;"class="red">CONSÚLTANOS Y SÍGUENOS EN:</p>
              <img src="{{ asset('front/facebook.png') }}"  width="40" height="40" >
              <img src="{{ asset('front/instagram.png') }}" width="40" height="40">
              <img src="{{ asset('front/twitter.png') }}" width="40" height="40">
              <img src="{{ asset('front/youtube.png') }}" width="40" height="40">
              </div>
              <div class="col-sm-4">
                <button type="submit" class=" form-control btn btn-secondary">ENVIAR</button> 
              </div>
              
            </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>


@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if (session('actualizar') == 'ok')

<script>
   Swal.fire(
        '¡Tu mensaje se ha enviado!',
        '',
        'success'
        )
</script>
    
@endif


@endsection