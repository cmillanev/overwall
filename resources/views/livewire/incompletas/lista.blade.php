<table id="example1" class="table table-striped table-bordered" style="width:100%">
    <thead class="thead-success">
        <th>Fecha Pedido</th>
        <th>Estatus</th>
        <th>Folio Pago</th>
        <th>Transacción</th>
        <th>Pago</th>
        <th>Monto</th>
       
        
    </thead>
    <tbody>
    @foreach ($orden as $orde)
    <tr>
       <td>{{$orde->created_at}}
       
       </td>
        <td>Preparando</td>
        <td>{{$orde->folioMexPago}}</td>
        <td>{{$orde->numeroTransaccion}}</td>
        <td>{{$orde->pago}}</td>
        <td>
        ${{number_format($orde->monto), 2}}.00
        </td>
        
    </tr>
    @endforeach
    </tbody>
</table>