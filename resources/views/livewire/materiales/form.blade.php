<div class="card card-body">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="nombre">Nombre</label>
             <input class="form-control"  type="text" wire:model.defer="nombre">
        @error('nombre') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group col-md-9">
            <label for="descripcion">Descripción del material</label>
             <input class="form-control"  type="text" wire:model.defer="descripcion">
        @error('descripcion') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

</div>


