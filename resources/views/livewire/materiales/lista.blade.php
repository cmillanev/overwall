<table  class="table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Acciones</th>
    </thead>
    <tbody>
    @foreach ($materiales as $material)
    <tr>
        <td>{{$material->id}}</td>
        <td>{{$material->material_nombre }}</td>
        <td>{{$material->material_descripcion}}</td>
        <td>
            <button wire:click="edit({{$material->id}})" class="btn btn-primary"><i class="fas fa-edit"></i></button>
            <button wire:click="destroy({{$material->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></button>   
        </td>
    </tr>
    @endforeach
    </tbody>
</table>