<div>
    <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
         Agregar Material
        </a>
      </p>
      <div class="collapse" id="collapseExample">
       @include('livewire.materiales.form')
       <button class="btn btn-primary" wire:click="store">
        Guardar
      </button>
      <button class="btn btn-primary" wire:click="default">
        Cancelar
      </button>
      </div>
      
</div>
    