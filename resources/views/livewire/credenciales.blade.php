<div class="card">
    <div class="card-header">
      Datos de la cuenta
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        {{ $successMsg }}
    </div>
 @endif
    <div class="card-body">
    <form wire:submit.prevent="submitForm">
      @csrf
      @method('PUT')
  <div class="row">
    <div class="form-group col-md-4">
      <label for="">Nombre</label>
      <input type="text" wire:model.defer='name'required class="form-control">
    </div>
 
 
    <div class="form-group col-md-4">
      <label for="">Email</label>
      <input type="text" wire:model.defer="email" required class="form-control">
    </div>
  

    <div class="form-group col-md-4">
      <label for="password">Contraseña</label>
      <input type="password" wire:model.defer='password' name="password" required class="form-control">
    </div>
    </div>

    <div class="card-footer">
        <input type="hidden" name="control" value="personales">
        <input class="btn btn-success" type="submit" value="Actualizar">
  
    </div>
  </form>
  </div>
</div>