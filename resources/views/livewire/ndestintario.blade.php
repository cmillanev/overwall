<div class="card">
    <form wire:submit.prevent="submitForm">
        @csrf
        @method('PUT')
    <div class="card-header">
      Nombre del destinatario
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        {{ $successMsg }}
    </div>
 @endif
    <div class="card-body">
  <div class="row">
    <div class="form-group col-md-4">
      <label for="nombre">Nombre</label>
      <input type="text" wire:model.defer="nombre" required class="form-control" >
    </div>  
  </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Actualizar</button>
  </div>
    </form>
</div>