<table  class="table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th>Email</th>
        <th>Asunto</th>
        <th>Acciones</th>
    </thead>
    <tbody>
    @foreach ($contactos as $contacto)
    <tr>
        <td>{{ $contacto->id}}</td>
        <td>{{ $contacto->contacto_nombre }} {{ $contacto->contacto_apellido }}</td>
        <td>{{ $contacto->contacto_correo }}</td>
        <td>{{ $contacto->contacto_mensaje }}</td>
        <td>
            <button wire:click="destroy({{$contacto->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></button>   
        </td>
    </tr>
    @endforeach
    </tbody>
</table>