<table class="table">
    <thead>
        <tr>
     
            <th>Nombre</th>
            <th>Email</th>
            <th>Fecha Reg</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($clientes as $cliente)
            <tr>
               
                <td>{{ $cliente->name}}</td>
                <td>{{ $cliente->email}}</td>
                <td>{{ $cliente->created_at}}</td>
                <td>
                    <button wire:click="show({{$cliente->id}})" class="btn btn-primary"><i class="fas fa-eye"></i></button>
                    
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{{$clientes->links('pagination::bootstrap-4')}}