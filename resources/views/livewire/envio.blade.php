<div class="card">
    <div class="card-header">
      Datos de envío
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        {{ $successMsg }}
    </div>
 @endif
    <div class="card-body">
    <form wire:submit.prevent="submitForm">
      @csrf
      @method('PUT')
  <div class="row">
    <div class="form-group col-md-4">
      <label for="e_calle">Calle</label>
      <input type="text" wire:model.defer="e_calle" required class="form-control">
    </div>
    <div class="form-group col-md-1">
      <label for="e_numExt">Num Ext</label>
      <input type="text" wire:model.defer="e_numExt" required class="form-control">
    </div>
    <div class="form-group col-md-1">
      <label for="e_numInt">Num Int</label>
      <input type="text" wire:model.defer="e_numInt" required class="form-control">
    </div>
    <div class="form-group col-md-3">
      <label for="e_colonia">Colonia</label>
      <input type="text" wire:model.defer="e_colonia" required class="form-control">
    </div>
    <div class="form-group col-md-3">
      <label for="c_numExt">Municipio / Delegación</label>
      <input type="text" wire:model.defer="e_mun" required class="form-control">
    </div>
    
  </div>
  <div class="row">
      <div class="form-group col-md-3">
          <label for="c_numExt">Ciudad</label>
          <input type="text" wire:model.defer="e_ciudad" required class="form-control" >
        </div>
    <div class="form-group col-md-3">
      <label for="c_numExt">Estado</label>
      <input type="text" wire:model.defer="e_estado" required class="form-control">
    </div>
    <div class="form-group col-md-3">
      <label for="c_numExt">Pais</label>
      <input type="text" wire:model.defer="e_pais" required class="form-control">
    </div>
    <div class="form-group col-md-3">
      <label for="c_numExt">Cp</label>
      <input type="text" wire:model.defer="e_cp" required class="form-control">
    </div>
  </div>
    </div>
    <div class="card-footer">
     
        <input class="btn btn-success" type="submit" placeholder="Actualizar">
  
    </div>
  </form>
  </div>