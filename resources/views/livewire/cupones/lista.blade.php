<table  class="table">
    <thead>
        <th>Id</th>
        <th>Cupón</th>
        <th>% descuento</th>
        <th>Estatus</th>
        <th>Vigencia</th>
        <th>Acciones</th>
    </thead>
    <tbody>
        @foreach ($cupones as $cupon)
            <tr>
                <td>{{$cupon->id}}</td>
                <td>{{$cupon->cupon}}</td>
                <td>{{$cupon->porcentaje}}</td>
                <td>{{$cupon->estatus}}</td>
                <td>{{$cupon->fecha}}</td>
                <td>
                    <button wire:click="edit({{$cupon->id}})" class="btn btn-primary"><i class="fas fa-edit"></i></button>
                    <button wire:click="destroy({{$cupon->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></button>   
                </td>
            </tr>
        @endforeach
    </tbody>
</table>