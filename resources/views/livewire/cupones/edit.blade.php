<div>
    <p>
        <a class="btn btn-primary" data-toggle="collapse" role="button" >
         Editar cupón
        </a>
      </p>
      <div class="collapse show" id="collapseExample">
       @include('livewire.cupones.form')
       <button class="btn btn-primary" wire:click="update">
        Actualizar
      </button>
       <button class="btn btn-primary" wire:click="default">
        Cancelar
      </button>
      </div>
      
</div>