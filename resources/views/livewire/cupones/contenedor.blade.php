<div>
    <div class="row">
        <div class="col-md-12">
            @include("livewire.cupones.$view")
        </div>
        @error('nombre')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('descripcion')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('estatus')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @if(Session::has('message'))
          <div class="alert alert-success">
              {{ $successMsg }}
          </div>
        @endif
     </div>
   <div class="row">
       <div class="col-md-12">
           @include('livewire.cupones.lista')
       </div>
   </div>
</div>