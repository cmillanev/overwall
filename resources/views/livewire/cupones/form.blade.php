<div class="card card-body">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="cupon">Cupón</label>
             <input class="form-control"  type="text" wire:model.defer="cupon">
        @error('cupon') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group col-md-2">
            <label for="porcentaje">% de descuento</label>
             <input class="form-control"  type="number" wire:model.defer="porcentaje">
        @error('porcentaje') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group col-md-3">
            <label for="estatus">Estatus</label>
             <input class="form-control"  type="checkbox" wire:model.defer="estatus">
        @error('estatus') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group col-md-3">
            <label for="porcentaje">Fecha de vigencia</label>
             <input class="form-control"  type="date" wire:model.defer="fecha">
        @error('fecha') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

</div>