<div>
    <div class="row">
        <div class="col-md-12">
            @include("livewire.precios.$view")
        </div>
        @error('modalidad')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('material')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('medidas')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('precio')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @if(Session::has('message'))
          <div class="alert alert-success">
              {{ $successMsg }}
          </div>
        @endif
     </div>
    <div class="row">
        <div class="col-md-12">
            @include('livewire.precios.lista')
        </div>
     </div>
</div>
