
  
    <div class="card card-body">
        <div class="row">
            <div class="form-group col-md-3">
                <label for="modalidad">Modalidad</label>
                 <input class="form-control"  type="text" wire:model.defer="modalidad">
            @error('modalidad') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group col-md-3">
                <label for="material">Material</label>
                 <input class="form-control"  type="text" wire:model.defer="material">
            @error('material') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group col-md-3">
                <label for="medidas">Medidas</label>
                <input class="form-control" type="text" wire:model.defer="medidas">
             @error('medidas')<span class="error">{{ $message }}</span> @enderror  
            </div>
            <div class="form-group col-md-3">
                <label for="precio">Precio</label>
                <input class="form-control" type="number"  min="0.00" max="10000.00" step="0.01" wire:model.defer="precio">
             @error('precio') <span class="error">{{ $message }}</span> @enderror  
            </div>
        </div>

</div>








