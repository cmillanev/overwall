<div>
    <p>
        <a class="btn btn-primary" data-toggle="collapse" role="button" >
         Editar usuario
        </a>
      </p>
      <div class="collapse show" id="collapseExample">
       @include('livewire.precios.form')
       <button class="btn btn-primary" wire:click="update">
        Actualizar
      </button>
       <button class="btn btn-primary" wire:click="default">
        Cancelar
      </button>
      </div>
      
</div>