
    <table  class="table table-striped ">
        <thead>
            <tr>
                <th>Id</th>
                <th>Modalidad</th>
                <th>Material</th>
                <th>Medidas</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
           
        </thead>
        <tbody>
            @foreach ($precios as $precio)
            <tr>
                <td>{{$precio->id}}</td>
                <td>{{$precio->modalidad}}</td> 
                <td>{{$precio->material}}</td> 
                <td>{{$precio->medidas}}</td>  
                <td>${{ number_format($precio->precio, 2) }}</td>
                <td>
                    <button wire:click="edit({{$precio->id}})" class="btn btn-primary"><i class="fas fa-edit"></i></button>
                    <button wire:click="destroy({{$precio->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                </td>
            </tr>  
            @endforeach
        </tbody>
    </table>
    {{$precios->links('pagination::bootstrap-4')}}

