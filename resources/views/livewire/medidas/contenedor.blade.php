<div>
    <div class="row">
        <div class="col-md-12">
            @include("livewire.medidas.$view")
        </div>
        @error('medidas_x')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
          @error('medidas_y')<div class="alert alert-danger" role="alert">
            {{ $message }}
          </div> @enderror  
       
          @if(Session::has('message'))
          <div class="alert alert-success">
              {{ $successMsg }}
          </div>
        @endif
     </div>
   <div class="row">
       <div class="col-md-12">
           @include('livewire.medidas.lista')
       </div>
   </div>
</div>
