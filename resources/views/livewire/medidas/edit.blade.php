<div>
    <p>
        <a class="btn btn-primary" data-toggle="collapse" role="button" >
         Editar material
        </a>
      </p>
      <div class="collapse show" id="collapseExample">
       @include('livewire.medidas.form')
       <button class="btn btn-primary" wire:click="update">
        Actualizar
      </button>
       <button class="btn btn-primary" wire:click="default">
        Cancelar
      </button>
      </div>
      
</div>