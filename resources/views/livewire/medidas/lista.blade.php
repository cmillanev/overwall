<table  class="table">
    <thead>
        <th>Id</th>
        <th>Medidas X</th>
        <th>Medidas Y</th>
        <th>Acciones</th>
    </thead>
    <tbody>
    @foreach ($medidas as $medida)
    <tr>
        <td>{{$medida->id}}</td>
        <td>{{$medida->medidas_x }} cm.</td>
        <td>{{$medida->medidas_y}} cm.</td>
        <td>
            <button wire:click="edit({{$medida->id}})" class="btn btn-primary"><i class="fas fa-edit"></i></button>
            <button wire:click="destroy({{$medida->id}})" class="btn btn-danger"><i class="fas fa-trash"></i></button>   
        </td>
    </tr>
    @endforeach
    </tbody>
</table>