<div class="card card-body">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="medidas_x">Medidas X</label>
             <input class="form-control"  type="text" wire:model.defer="medidas_x">
        @error('medidas_x') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group col-md-3">
            <label for="medidas_y">Medidas Y</label>
             <input class="form-control"  type="text" wire:model.defer="medidas_y">
        @error('medidas_y') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

</div>


