<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="shortcut icon" href="{{ asset('front/favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nav2.css') }}" rel="stylesheet">

  <style>

@font-face{
  font-family: 'menu';
  src: url('fonts/DIN Alternate Bold.otf');
}
@font-face{
  font-family: 'gotham-bold';
  src: url('fonts/Gotham-Bold.ttf');
}

@font-face{ 
  font-family: 'gotham-medium';
  src: url('fonts/Gotham-Medium.otf');
}
@font-face{
  font-family: 'din-pro-medium';
  src: url('fonts/DINPro Medium.otf');
}
@font-face{
  font-family: 'din-pro-regular';
  src: url('fonts/DINPro Regular.otf');
}
@font-face{
  font-family: 'din-pro-light';
  src: url('tipografias_overwall/DINPro-Light.otf');
}

.container a{
  color: #123f6f;
}

  #navbarDropdown{
    color:aliceblue;
  }

  .navbar-toggler{
    background-color: white;
    float: right;
  }

  #container{
    height: 104px;
  }
 .navg{
   color: #123f6f;
  
  
 }
 .nav-link{
   margin-right: 30px; 
   margin-left: 10px; 
 }

 a .nav-link:hover {color:#7c7ce9}

 .hrfooter{
  border: 2px solid #123f6f;
  margin-top: 27%;
  border-radius: 5px;
}

#logofooter{

  width: 80%;
  margin-left: 11%; 
 
  
}

.nav-link{
  font-family: 'menu';
}



.modal-body input{
       margin-top: 5px;
       background-color: #ededed;
       color: #123F6F;
       height: 20px;
     }
     .modal-body label{
       font-size: 8pt;
     }
     .modal-body select{
       height: 27px;
       font-size: 8pt;
     }

     .modal-body h5{
       margin-top: 30px;
     }
     .titulo{
     color: white;
      font-weight: bold;
         display:block; 
         position:right; 
         margin-top: 5px;
         margin-left: 2%;
         margin-right: 3%;
         text-align: center;
         background: #123F6F;
         border: 2px solid #000000;
         border-radius: 8px;
     }
    .close{
      font-weight: bold;
         display:block; 
         position:right; 
         margin-left: 90%;
    }
    .pregunta{
      color: #1F73B9;
      font-weight: bold;
    }
    
    .texcol{
      color: #1F73B9;
      font-weight: bold;
    }
    hr {
      height: 1px;
      background-color: #1F73B9;
    }
    .hregistro{
      border: 2px solid #123f6f;
      margin-top: 27%;
      border-radius: 5px;
    }
    .b-modal{
      border: 5px solid #123f6f;
    }
    .modal img{
      position:relative;
       top: 30px;
       left:250px;
      width: 300px;
    }
    .modal-body h5{
      text-align: center;
      color: #123f6f;
      margin-bottom: 25px;
    }
    .footer2{
      display: none;
    }

    @media only screen and (max-width: 800px) {

  #logofooter{
    width: 80%;
  
  
  }
 .footer2{
   padding-left: 3%; 
   padding-right: 3%; 
   background-color:#E3E3E2;
   color: gray;
 }
 .footer2 h5{
   font-size: 10pt;
 }
 .footer{
   font-size: 6pt;
 }
}
/* Footer*/
/* Fin Footer*/
  </style>
  
    @yield('css')

</head>
<body>
    <div id="app">
     
        <header id="registro" style="background-color:#123f6f ">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm">
            <div class="container">
              <a class="navbar-brand" href="{{ url('/') }}">
              
            </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link"  style="color: white;" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                  <a class="nav-link" style="color: white;" href="" data-toggle="modal" data-target="#exampleModalLong">
                                  Registrate
                                  </a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                    @role('cliente')
                                    <a class="dropdown-item" href="{{ url('admin') }}">
                                        Mi cuenta
                                    </a>
                                    @else
                                    <a class="dropdown-item" href="{{ url('admin') }}">
                                        Dashboard
                                    </a>
                                    
                                    @endrole
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <header>
        <div class="">
          
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="" id="logo1">
              <a href="{{ url('') }}">
             <img src="{{ asset('front/logo_menu.png') }}" alt="" style="width: 300px;">   
              </a>
             
          </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse bar" id="navbarNav">
      <ul class="navg navbar-nav w-100 justify-content-left ">
        <li class="nav-item " id="">
          <a class="nav-link" style="color: #123f6f; hover{ color: #7c7ce9}"  href="{{ url('/#explorar') }}">BUSCAR </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " style="color: #123f6f; hover{ color: #7c7ce9}"  href="{{ url('resultado?frase=Paisaje&page=1') }}">CATEGORÍAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  style="color: #123f6f; hover{ color: #7c7ce9}" href="{{ route('talentos') }}">TALENTOS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link"  style="color: #123f6f; hover{ color: #7c7ce9}" href="{{ route('tuwall') }}">TU WALL</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" style="color: #123f6f; hover{ color: #7c7ce9}" href="{{ url('contacto') }}">CONTACTO</a>
        </li>
        <li>
          <a href="{{ url('carrito') }}">
          <img src="{{ asset('front/carrito solo.png') }}" alt=""  class="img-fluid car">  
          </a>
          
        </li>
      </ul>
     
    </div>
  </nav>
        </div>
      </header>
      
        <main class="py-4">
            @yield('content')
        </main>
        
    </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        
    @yield('js')


 <!-- Footer -->
<footer class="" id="footer">
   <div class="container">
     <div class="row">
       <div   class="col-md-4">
         <hr class="hrfooter">
       </div>
       <div  class="col-md-4">
        <img src="{{ asset('front/logo_getty.png') }}" alt=""  class="img-fluid" id="logofooter"> 
       </div>
       <div  class="col-md-4">
         <hr class="hrfooter">
       </div>
     </div>
   </div>
     
      <!-- Footer Links -->
      <div class="container text-center text-md-left">
    
        <!-- Grid row -->
        <div class="row">
    
          <!-- Grid column -->
          <div class="col-md-3 mx-auto " >
    
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let">NUESTRA PAGINA</h5>
    
            <ul class="list-unstyled let">
              <li>
                <a id="footer" class="footer"  href="{{ url('/#explorar') }}">EXPLORAR</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('resultado?frase=Paisaje&page=1') }}">CATEGORIAS</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('artistas') }}">ARTISTAS</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('tuwall') }}">TU WALL</a>
              </li>
            </ul>
    
          </div>
          <!-- Grid column -->
    
          <hr class="clearfix w-100 d-md-none">
    
          <!-- Grid column -->
          <div class="col-md-3 mx-auto">
    
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let">SOPORTE</h5>
    
            <ul class="list-unstyled let">
              <li>
                <a id="footer" class="footer" href="{{ url('carrito') }}">CARRITO DE COMPRA</a>
              </li>
              <li>
                <a id="footer" class="footer" href="#!">ESTADO DE PEDIDO</a>
              </li>
              <li>
                <a id="footer" class="footer" href="docs/preguntas_frecuentes.pdf">PREGUNTAS FRECUENTES</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('contacto') }}">CONTACTO</a>
              </li>
            </ul>
    
          </div>
          <!-- Grid column -->
    
          <hr class="clearfix w-100 d-md-none">
    
          <!-- Grid column -->
          <div class="col-md-3 mx-auto">
    
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let">POLITICAS</h5>
    
            <ul class="list-unstyled let" >
              <li>
                <a id="footer" class="footer" href="docs/acuerdodeconfidencialidad.pdf">ACUERDO DE CONFIDENCIALIDAD</a>
              </li>
            
              <li>
                <a id="footer" class="footer" href="docs/avisodeprivacidad.pdf">AVISO DE PRIVACIDAD</a>
              </li>
            </ul>
    
          </div>
          <!-- Grid column -->
    
          <hr class="clearfix w-100 d-md-none">
    
          <!-- Grid column -->
          <div class="col-md-3 mx-auto">
    
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let" id="footer">INSPIRACIÓN</h5>
    
            <ul class="list-unstyled let">
              <li>
                <img src="{{ asset('front/facebook.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://www.facebook.com/overwallmx">FACEBOOK</a>

              </li>
              <li>
                <img src="{{ asset('front/instagram.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://www.instagram.com/overwallmx/">INSTAGRAM</a>
              </li>
              <li>
                <img src="{{ asset('front/youtube.png') }}" alt=""  class="img-fluid social">
                <a  id="footer" href="https://www.youtube.com/channel/UCGEo_Uqb-UAF6N28ypyrFuA">YOUTUBE</a>
              </li>
              <li>
                <img src="{{ asset('front/twitter.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://twitter.com/OverwallM">TWITTER</a>
              </li>
            </ul>
    
          </div>
          <!-- Grid column -->
    
        </div>
        <!-- Grid row -->
    <div class="footer-copyright text-center let">© 2021 Copyright:
        <a> OVERWALL</a>
      </div>
      </div>
      <!-- Footer Links -->
    
      <!-- Copyright -->
      
      <!-- Copyright -->
    
    </footer>

    <footer class="footer2" id="footer2">
        <div class="row">
          <div class="col">
            <hr>
          </div>
          <div class="col">
            <img src="{{ asset('front/logo_getty.png') }}" alt=""  class="img-fluid" id="logofooter"> 
          </div>
          <div class="col">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let">NUESTRA PAGINA</h5>
                
            <ul class="list-unstyled let">
              <li>
                <a id="footer" class="footer"  href="{{ url('/#explorar') }}">EXPLORAR</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('resultado?frase=Paisaje&page=1') }}">CATEGORIAS</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('artistas') }}">ARTISTAS</a>
              </li>
              <li>
                <a id="footer" class="footer" href="{{ url('tuwall') }}">TU WALL</a>
              </li>
            </ul>
          </div>
          <div class="col">
              <!-- Links -->
              <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let">POLITICAS</h5>
                
              <ul class="list-unstyled let" >
                <li>
                  <a id="footer" class="footer" href="docs/acuerdodeconfidencialidad.pdf">ACUERDO DE CONFIDENCIALIDAD</a>
                </li>
              
                <li>
                  <a id="footer" class="footer" href="docs/avisodeprivacidad.pdf">AVISO DE PRIVACIDAD</a>
                </li>
              </ul>
          </div>
          <div class="col">
            <!-- Links -->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4 let" id="footer">INSPIRACIÓN</h5>
              
            <ul class="list-unstyled let">
              <li>
                <img src="{{ asset('front/facebook.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://www.facebook.com/overwallmx">FACEBOOK</a>

              </li>
              <li>
                <img src="{{ asset('front/instagram.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://www.instagram.com/overwallmx/">INSTAGRAM</a>
              </li>
              <li>
                <img src="{{ asset('front/youtube.png') }}" alt=""  class="img-fluid social">
                <a  id="footer" href="https://www.youtube.com/channel/UCGEo_Uqb-UAF6N28ypyrFuA">YOUTUBE</a>
              </li>
              <li>
                <img src="{{ asset('front/twitter.png') }}" alt=""  class="img-fluid social">
                <a id="footer" class="footer" href="https://twitter.com/OverwallM">TWITTER</a>
              </li>
            </ul>
          </div>
        </div>
    </footer>
    <!-- Footer -->

    @include('layouts.modal')
    <!-- Cloudflare Web Analytics -->
<script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{"token": "497953f06b004035942b2b3e6bc29d18"}'></script>
<!-- End Cloudflare Web Analytics -->
</body>
<script src="{{asset('js/js/detectmobilebrowser.js')}}"></script>
<script>
    $( document ).ready(function() {
    document.getElementById('material_cuadros').value = JSON.parse(localStorage.getItem('material_cuadros'));
    document.getElementById('medidas_cuadros').value = JSON.parse(localStorage.getItem('medidas_cuadros'));
    document.getElementById('cantidad_cuadros').value = JSON.parse(localStorage.getItem('cantidad_cuadros'));
    document.getElementById('precio_cuadros').value = JSON.parse(localStorage.getItem('precio_cuadros'));
    document.getElementById('total_cuadros').value = JSON.parse(localStorage.getItem('total_cuadros'));
    document.getElementById('url_cuadros').value = JSON.parse(localStorage.getItem('url_cuadros'));
  });
</script>
<script>
   if($.browser.mobile){
        $("#footer").hide();
        $("#footer2").show();
          console.log('oculta imagenes');
    }
</script>
</html>
