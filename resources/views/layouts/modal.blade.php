    
    
    <!-- Modal -->
    <div class="container">
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              
             
             
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
       
        </button>
    
              <div class="modal-body modalimg">
                <img src="{{'front/registro_boton.png'}}" alt="">
                <div class="container b-modal">
                   <h5 class="menu">¿YA TIENES UNA CUENTA? <a href="{{route('login')}}">ENTRAR</a></h5>

                   <form action="{{ route('registro.store') }}" method="post">
                    @csrf
                   <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">USUARIO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="usuario" class="form-control" value="">
                    </div>
                 
                  
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">NOMBRE Y APELLIDO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="name" id="name" class="form-control" value="" required>
                    </div>
                 
                 
                    <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">EMAIL DE CONTACTO:</label>
                    <div class="col-sm-7">
                      <input type="text" name="email" class="form-control" value="">
                    </div>
                  
                  
                    <label for="inputPassword" class="col-sm-4 col-form-label-izquierda texcol">PASSWORD:</label>
                    <div class="col-sm-7">
                      <input type="password" name="password" class="form-control" id="inputPassword" placeholder="">
                    </div>
                  </div>
                
                  
                  <!--direccion de contacto-->

          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DIRECCIÓN DE CONTACTO:</label>
            <label for="calle" class="col-sm-1 col-form-label texcol">Calle:</label>
            <div class="col-sm-6">
              <input type="text" name="c_calle"  class="form-control" value="" id="c_calle">
            </div>
           
             <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Colonia:</label>
            <div class="col-sm-7">
              <input type="text" name="c_colonia" id="c_colonia"  class="form-control" value="" >
            </div>
          <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Número Exterior:</label>
            <div class="col-sm-3">
              <input type="text" name="c_numExt" id="c_numExt"  class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >Interior:</label>
            <div class="col-sm-3">
              <input type="text" name="c_numInt" id="c_numInt"  class="form-control"  value="">
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Municipio/Delegación:</label>
            <div class="col-sm-7">
              <input type="text" name="c_mun" id="c_mun"  class="form-control" value="" >
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Ciudad:</label>
            <div class="col-sm-3">
              <input type="text" name="c_ciudad" id="c_ciudad" class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol">Estado:</label>
            <div class="col-sm-3">
               <select class="form-control" name="c_estado" id="c_estado" name="Estado"   >
                        <option value="CDMX">Ciudad de México</option>
                        <option value="Aguascalientes">Aguascalientes</option>
                        <option value="Baja California">Baja California</option>
                        <option value="Baja California Sur">Baja California Sur</option>
                        <option value="Campeche">Campeche</option>
                        <option value="Chiapas">Chiapas</option>
                        <option value="Chihuahua">Chihuahua</option>
                        <option value="Coahuila">Coahuila</option>
                        <option value="Colima">Colima</option>
                        <option value="Durango">Durango</option>
                        <option value="Estado de México">Estado de México</option>
                        <option value="Guanajuato">Guanajuato</option>
                        <option value="Guerrero">Guerrero</option>
                        <option value="Hidalgo">Hidalgo</option>
                        <option value="Jalisco">Jalisco</option>
                        <option value="Michoacán">Michoacán</option>
                        <option value="Morelos">Morelos</option>
                        <option value="Nayarit">Nayarit</option>
                        <option value="Nuevo León">Nuevo León</option>
                        <option value="Oaxaca">Oaxaca</option>
                        <option value="Puebla">Puebla</option>
                        <option value="Querétaro">Querétaro</option>
                        <option value="Quintana Roo">Quintana Roo</option>
                        <option value="San Luis Potosí">San Luis Potosí</option>
                        <option value="Sinaloa">Sinaloa</option>
                        <option value="Sonora">Sonora</option>
                        <option value="Tabasco">Tabasco</option>
                        <option value="Tamaulipas">Tamaulipas</option>
                        <option value="Tlaxcala">Tlaxcala</option>
                        <option value="Veracruz">Veracruz</option>
                        <option value="Yucatán">Yucatán</option>
                        <option value="Zacatecas">Zacatecas</option>
                       </select>
                      </div>
              <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">País:</label>
            <div class="col-sm-3">
              <input type="text" name="c_pais" id="c_pais"  class="form-control" value="México"  readonly="readonly" >
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >CP:</label>
            <div class="col-sm-3">
              <input type="text" name="c_cp" id="c_cp"  class="form-control"  value="">
            </div>
            </div>

            <!--direccion de envio-->
       <br>
      <div class="form-group row ">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DIRECCIÓN DE ENVÍO:</label>
           
            <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="seleccion" id="seleccion1"  onclick="myFunction()">
                    <label class="form-check-label texcol" for="seleccion1">Usar datos de contacto</label>
                </div>
                <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="seleccion" id="selecion2" onclick="myFunctionLimpia()">
                    <label class="form-check-label texcol" for="selecion2">Otra información</label>
                </div>
              

            <label for="calle" class="col-sm-4 col-form-label-izquierda texcol">Calle:</label>
            <div class="col-sm-7">
              <input type="text" name="e_calle" id="e_calle"  class="form-control" value="" >
            </div>
           
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Colonia:</label>
            <div class="col-sm-7">
              <input type="text" name="e_colonia" id="e_colonia"  class="form-control" value="" >
            </div>
          <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Númmero Exterior:</label>
            <div class="col-sm-3">
              <input type="text" name="e_numExt" id="e_numExt"  class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >Interior:</label>
            <div class="col-sm-3">
              <input type="text" name="e_numInt" id="e_numInt"  class="form-control"  value="">
            </div>
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Municipio/Delegación:</label>
            <div class="col-sm-7">
              <input type="text" name="e_mun" id="e_mun" class="form-control" value="" >
            </div>

            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">Ciudad:</label>
            <div class="col-sm-3">
              <input type="text" name="e_ciudad" id="e_ciudad" class="form-control" value="">
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol">Estado:</label>
            <div class="col-sm-3">
               <select class="form-control" name="e_estado"   id="e_estado">
                       
                        <option value="Aguascalientes">Aguascalientes</option>
                        <option value="Baja California">Baja California</option>
                        <option value="Baja California Sur">Baja California Sur</option>
                        <option value="Campeche">Campeche</option>
                        <option value="Chiapas">Chiapas</option>
                        <option value="Chihuahua">Chihuahua</option>
                        <option value="CDMX">Ciudad de México</option>
                        <option value="Coahuila">Coahuila</option>
                        <option value="Colima">Colima</option>
                        <option value="Durango">Durango</option>
                        <option value="Estado de México">Estado de México</option>
                        <option value="Guanajuato">Guanajuato</option>
                        <option value="Guerrero">Guerrero</option>
                        <option value="Hidalgo">Hidalgo</option>
                        <option value="Jalisco">Jalisco</option>
                        <option value="Michoacán">Michoacán</option>
                        <option value="Morelos">Morelos</option>
                        <option value="Nayarit">Nayarit</option>
                        <option value="Nuevo León">Nuevo León</option>
                        <option value="Oaxaca">Oaxaca</option>
                        <option value="Puebla">Puebla</option>
                        <option value="Querétaro">Querétaro</option>
                        <option value="Quintana Roo">Quintana Roo</option>
                        <option value="San Luis Potosí">San Luis Potosí</option>
                        <option value="Sinaloa">Sinaloa</option>
                        <option value="Sonora">Sonora</option>
                        <option value="Tabasco">Tabasco</option>
                        <option value="Tamaulipas">Tamaulipas</option>
                        <option value="Tlaxcala">Tlaxcala</option>
                        <option value="Veracruz">Veracruz</option>
                        <option value="Yucatán">Yucatán</option>
                        <option value="Zacatecas">Zacatecas</option>
                       </select>
                      </div>
              <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">País:</label>
            <div class="col-sm-3">
              <input type="text" name="e_pais" id="e_pais" class="form-control" value="México"  readonly="readonly" >
            </div>
            <label for="staticEmail" class="col-sm-1 col-form-label texcol" >CP:</label>
            <div class="col-sm-3">
              <input type="text" name="e_cp" id="e_cp"  class="form-control"  value="">
            </div>
            <label for="" class="col-sm-4 col-form-label-izquierda texcol">IMPORTANTE:</label>
            <div class="col-sm-7">
              <label  class=" col-form-label texcol">Si tu dirección de envío es fuera de México, ponte en contacto con nosotros antes de tu compra.</label>
            </div>
          </div>

<!--destinatario-->
       <br>
      <div class="form-group row col-md-12">
            <label for="staticEmail" class="col-sm-4 col-form-label-izquierda texcol">DESTINATARIO:</label>
           
            <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input texcol" type="radio" name="option" id="option1"  onclick="myFunction2()">
                    <label class="form-check-label texcol" for="option1">Usar datos de contacto</label>
                </div>
           
            
                <div class="form-check form-check-inline col-md-3">
                    <input class="form-check-input" type="radio" name="option" id="option2" onclick="myFunctionLimpia2()">
                    <label class="form-check-label texcol" for="option2">Otra información</label>
                </div>
              

            <label for="nombre" class="col-sm-4 col-form-label-izquierda texcol">Nombre:</label>
            <div class="col-sm-8">
              <input type="text" name="d_nombre" id="d_nombre" class="form-control" value="" >
              <input type="hidden" name="material_cuadros" id="material_cuadros">
              <input type="hidden" name="medidas_cuadros" id="medidas_cuadros">
              <input type="hidden" name="cantidad_cuadros" id="cantidad_cuadros">
              <input type="hidden" name="precio_cuadros" id="precio_cuadros">
              <input type="hidden" name="total_cuadros" id="total_cuadros">
              <input type="hidden" name="url_cuadros" id="url_cuadros">
            </div>
            
          </div>
          <div class="col-md-12" >
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
        </form> 
              </div>
               
                
           
            </div>
         

   <!-- FIN Modal -->


  <!-- Modal -->
<div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
         

   <!-- FIN Modal -->