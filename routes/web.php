<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('resultado', [App\Http\Controllers\HomeController::class, 'busqueda'])->name('resultado');
Route::get('vistaprevia', [App\Http\Controllers\HomeController::class, 'vistaprevia'])->name('vistaprevia');
Route::post('gprecios', [App\Http\Controllers\PreciosController::class, 'gprecios'])->name('gprecios.post');
Route::post('tprecios', [App\Http\Controllers\PreciosController::class, 'tprecios'])->name('tprecios.post');
Route::post('lprecios', [App\Http\Controllers\PreciosController::class, 'lprecios'])->name('lprecios.post');
Route::post('clayout', [App\Http\Controllers\TuwallController::class, 'clayout'])->name('clayout.post');
Route::get('carrito', [App\Http\Controllers\HomeController::class, 'carrito'])->name('carrito');
Route::get('confirmacion', [App\Http\Controllers\HomeController::class, 'confirmacion'])->name('confirmacion');
//Route::get('carrito', ['middleware' => 'auth', 'uses' => 'App\Http\Controllers\HomeController@carrito'], [App\Http\Controllers\HomeController::class, 'carrito'])->name('carrito');

Route::get('tuwall', [App\Http\Controllers\TuwallController::class, 'tuwall'])->name('tuwall');
Route::post('cmarco', [App\Http\Controllers\TuwallController::class, 'cmarco'])->name('cmarco');
Route::post('clayout', [App\Http\Controllers\TuwallController::class, 'clayout'])->name('clayout');
Route::get('talentos', [App\Http\Controllers\TalentosController::class, 'talentos'])->name('talentos');
Route::resource('contacto', App\Http\Controllers\ContactoController::class);
Route::get('carga', [App\Http\Controllers\HomeController::class, 'carga'])->name('carga');
Route::resource('registro', App\Http\Controllers\RegistroController::class);
Route::post('registra', [App\Http\Controllers\RegistraController::class, 'registra'])->name('registra.post');


Route::get('/clear-cache', function () {
    echo Artisan::call('config:clear');
    echo Artisan::call('config:cache');
    echo Artisan::call('cache:clear');
    echo Artisan::call('route:clear');
    echo Artisan::call('optimize:clear');
 });


Auth::routes();

Route::group(['prefix' => 'usuario', 'middleware' => ['role:admin|operativo|cliente']], function(){
    Route::get('/', [App\Http\Controllers\Usuario\HomeController::class, 'index']);
    Route::get('confirmacion', [App\Http\Controllers\Usuario\HomeController::class, 'confirmacion']);
    Route::resource('carrito', App\Http\Controllers\Usuario\CarritoController::class);
    Route::resource('cgetty',  App\Http\Controllers\Usuario\CgettyController::class);
    Route::resource('tuwall',  App\Http\Controllers\Usuario\TuwallController::class);
    Route::get('pago', [App\Http\Controllers\Usuario\PagoController::class, 'pago'])->name('pago');
});

Route::get('api', [App\Http\Controllers\Api\GettyController::class, 'getty'])->name('getty');

Route::group(['prefix' => 'admin','middleware' => ['role:admin|operativo|cliente']], function(){
    Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home');
    Route::resource('sliders', App\Http\Controllers\Admin\SlidersController::class);
    Route::resource('usuarios', App\Http\Controllers\Admin\UsersController::class);
    Route::get('reportes', [App\Http\Controllers\ReportesController::class, 'index'])->name('reportes');
    Route::resource('atributos', App\Http\Controllers\AtributosController::class);
    Route::get('getty', [App\Http\Controllers\Admin\GettyController::class, 'getty'])->name('api');
    Route::post('search',[App\Http\Controllers\Admin\GettyController::class, 'search'])->name('search.post');
    Route::resource('materiales', App\Http\Controllers\Admin\MaterialesController::class);
    Route::resource('medidas', App\Http\Controllers\Admin\MedidasController::class);
    Route::resource('categorias', App\Http\Controllers\Admin\CategoriasController::class);
    Route::resource('colecciones', App\Http\Controllers\Admin\ColeccionesController::class);
    Route::resource('clientes', App\Http\Controllers\Admin\ClientesController::class);
    Route::resource('tags', App\Http\Controllers\Admin\TagsController::class);
    Route::resource('contact', App\Http\Controllers\Admin\ContactoController::class);
    Route::resource('ordenes', App\Http\Controllers\Admin\OrdenesController::class);
    Route::resource('perfil', App\Http\Controllers\Admin\PerfilController::class);
    Route::get('precios', [App\Http\Controllers\Admin\PreciosController::class, 'index'])->name('precios');
    Route::post('cupon',[App\Http\Controllers\Admin\CuponesController::class, 'cupon'])->name('cupon.post');
    Route::get('cuponera',[App\Http\Controllers\Admin\CuponesController::class, 'index']);
    Route::get('incompletas',[App\Http\Controllers\Admin\OrdenIncompletaController::class, 'index'])->name('incompletas');
    
    
});